package b4j.example;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class main_subs_0 {


public static RemoteObject  _appstart(RemoteObject _args) throws Exception{
try {
		Debug.PushSubsStack("AppStart (main) ","main",0,main.ba,main.mostCurrent,11);
if (RapidSub.canDelegate("appstart")) { return b4j.example.main.remoteMe.runUserSub(false, "main","appstart", _args);}
Debug.locals.put("Args", _args);
 BA.debugLineNum = 11;BA.debugLine="Sub AppStart (Args() As String)";
Debug.ShouldStop(1024);
 BA.debugLineNum = 12;BA.debugLine="SendMessage(\"general\", \"title\", \"body\")";
Debug.ShouldStop(2048);
_sendmessage(BA.ObjectToString("general"),BA.ObjectToString("title"),RemoteObject.createImmutable("body"));
 BA.debugLineNum = 13;BA.debugLine="StartMessageLoop";
Debug.ShouldStop(4096);
main.__c.runVoidMethod ("StartMessageLoop",main.ba);
 BA.debugLineNum = 14;BA.debugLine="End Sub";
Debug.ShouldStop(8192);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _jobdone(RemoteObject _job) throws Exception{
try {
		Debug.PushSubsStack("JobDone (main) ","main",0,main.ba,main.mostCurrent,35);
if (RapidSub.canDelegate("jobdone")) { return b4j.example.main.remoteMe.runUserSub(false, "main","jobdone", _job);}
Debug.locals.put("job", _job);
 BA.debugLineNum = 35;BA.debugLine="Sub JobDone(job As HttpJob)";
Debug.ShouldStop(4);
 BA.debugLineNum = 36;BA.debugLine="Log(job)";
Debug.ShouldStop(8);
main.__c.runVoidMethod ("Log",(Object)(BA.ObjectToString(_job)));
 BA.debugLineNum = 37;BA.debugLine="If job.Success Then";
Debug.ShouldStop(16);
if (_job.getField(true,"_success" /*RemoteObject*/ ).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 38;BA.debugLine="Log(job.GetString)";
Debug.ShouldStop(32);
main.__c.runVoidMethod ("Log",(Object)(_job.runClassMethod (b4j.example.httpjob.class, "_getstring" /*RemoteObject*/ )));
 };
 BA.debugLineNum = 40;BA.debugLine="job.Release";
Debug.ShouldStop(128);
_job.runClassMethod (b4j.example.httpjob.class, "_release" /*RemoteObject*/ );
 BA.debugLineNum = 41;BA.debugLine="ExitApplication '!";
Debug.ShouldStop(256);
main.__c.runVoidMethod ("ExitApplication");
 BA.debugLineNum = 42;BA.debugLine="End Sub";
Debug.ShouldStop(512);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}

private static boolean processGlobalsRun;
public static void initializeProcessGlobals() {
    
    if (main.processGlobalsRun == false) {
	    main.processGlobalsRun = true;
		try {
		        main_subs_0._process_globals();
httputils2service_subs_0._process_globals();
main.myClass = BA.getDeviceClass ("b4j.example.main");
httputils2service.myClass = BA.getDeviceClass ("b4j.example.httputils2service");
httpjob.myClass = BA.getDeviceClass ("b4j.example.httpjob");
		
        } catch (Exception e) {
			throw new RuntimeException(e);
		}
    }
}public static RemoteObject  _process_globals() throws Exception{
 //BA.debugLineNum = 7;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 8;BA.debugLine="Private const API_KEY As String = \"AAAAlatA4jY:AP";
main._api_key = BA.ObjectToString("AAAAlatA4jY:APA91bEC4X80Pl2Y6dV-dZu2dgw3XV1_GX3RleHpevyYo2ELDL1gQeEj8lGdIFKLRfueR-kYp1YDMTAY46-BaJBOw-CDA9ZO7zKXtpMA4FW-CB02zwrUyfeXdq3YCOyFD_u_M-kPXK8q");
 //BA.debugLineNum = 9;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _sendmessage(RemoteObject _topic,RemoteObject _title,RemoteObject _body) throws Exception{
try {
		Debug.PushSubsStack("SendMessage (main) ","main",0,main.ba,main.mostCurrent,16);
if (RapidSub.canDelegate("sendmessage")) { return b4j.example.main.remoteMe.runUserSub(false, "main","sendmessage", _topic, _title, _body);}
RemoteObject _job = RemoteObject.declareNull("b4j.example.httpjob");
RemoteObject _m = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
RemoteObject _data = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
RemoteObject _iosalert = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
RemoteObject _jg = RemoteObject.declareNull("anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator");
Debug.locals.put("Topic", _topic);
Debug.locals.put("Title", _title);
Debug.locals.put("Body", _body);
 BA.debugLineNum = 16;BA.debugLine="Private Sub SendMessage(Topic As String, Title As";
Debug.ShouldStop(32768);
 BA.debugLineNum = 17;BA.debugLine="Dim Job As HttpJob";
Debug.ShouldStop(65536);
_job = RemoteObject.createNew ("b4j.example.httpjob");Debug.locals.put("Job", _job);
 BA.debugLineNum = 18;BA.debugLine="Job.Initialize(\"fcm\", Me)";
Debug.ShouldStop(131072);
_job.runClassMethod (b4j.example.httpjob.class, "_initialize" /*RemoteObject*/ ,main.ba,(Object)(BA.ObjectToString("fcm")),(Object)(main.getObject()));
 BA.debugLineNum = 19;BA.debugLine="Dim m As Map = CreateMap(\"to\": $\"/topics/${Topic}";
Debug.ShouldStop(262144);
_m = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");
_m = main.__c.runMethod(false, "createMap", (Object)(new RemoteObject[] {RemoteObject.createImmutable(("to")),((RemoteObject.concat(RemoteObject.createImmutable("/topics/"),main.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_topic))),RemoteObject.createImmutable(""))))}));Debug.locals.put("m", _m);Debug.locals.put("m", _m);
 BA.debugLineNum = 20;BA.debugLine="Dim data As Map = CreateMap(\"title\": Title, \"body";
Debug.ShouldStop(524288);
_data = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");
_data = main.__c.runMethod(false, "createMap", (Object)(new RemoteObject[] {RemoteObject.createImmutable(("title")),(_title),RemoteObject.createImmutable(("body")),(_body)}));Debug.locals.put("data", _data);Debug.locals.put("data", _data);
 BA.debugLineNum = 21;BA.debugLine="If Topic.StartsWith(\"ios_\") Then";
Debug.ShouldStop(1048576);
if (_topic.runMethod(true,"startsWith",(Object)(RemoteObject.createImmutable("ios_"))).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 22;BA.debugLine="Dim iosalert As Map =  CreateMap(\"title\": Title,";
Debug.ShouldStop(2097152);
_iosalert = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");
_iosalert = main.__c.runMethod(false, "createMap", (Object)(new RemoteObject[] {RemoteObject.createImmutable(("title")),(_title),RemoteObject.createImmutable(("body")),(_body),RemoteObject.createImmutable(("sound")),(RemoteObject.createImmutable("default"))}));Debug.locals.put("iosalert", _iosalert);Debug.locals.put("iosalert", _iosalert);
 BA.debugLineNum = 23;BA.debugLine="m.Put(\"notification\", iosalert)";
Debug.ShouldStop(4194304);
_m.runVoidMethod ("Put",(Object)(RemoteObject.createImmutable(("notification"))),(Object)((_iosalert.getObject())));
 BA.debugLineNum = 24;BA.debugLine="m.Put(\"priority\", 10)";
Debug.ShouldStop(8388608);
_m.runVoidMethod ("Put",(Object)(RemoteObject.createImmutable(("priority"))),(Object)(RemoteObject.createImmutable((10))));
 };
 BA.debugLineNum = 26;BA.debugLine="m.Put(\"data\", data)";
Debug.ShouldStop(33554432);
_m.runVoidMethod ("Put",(Object)(RemoteObject.createImmutable(("data"))),(Object)((_data.getObject())));
 BA.debugLineNum = 27;BA.debugLine="Dim jg As JSONGenerator";
Debug.ShouldStop(67108864);
_jg = RemoteObject.createNew ("anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator");Debug.locals.put("jg", _jg);
 BA.debugLineNum = 28;BA.debugLine="jg.Initialize(m)";
Debug.ShouldStop(134217728);
_jg.runVoidMethod ("Initialize",(Object)(_m));
 BA.debugLineNum = 29;BA.debugLine="Job.PostString(\"https://fcm.googleapis.com/fcm/se";
Debug.ShouldStop(268435456);
_job.runClassMethod (b4j.example.httpjob.class, "_poststring" /*RemoteObject*/ ,(Object)(BA.ObjectToString("https://fcm.googleapis.com/fcm/send")),(Object)(_jg.runMethod(true,"ToString")));
 BA.debugLineNum = 30;BA.debugLine="Job.GetRequest.SetContentType(\"application/json;c";
Debug.ShouldStop(536870912);
_job.runClassMethod (b4j.example.httpjob.class, "_getrequest" /*RemoteObject*/ ).runVoidMethod ("SetContentType",(Object)(RemoteObject.createImmutable("application/json;charset=UTF-8")));
 BA.debugLineNum = 31;BA.debugLine="Job.GetRequest.SetHeader(\"Authorization\", \"key=\"";
Debug.ShouldStop(1073741824);
_job.runClassMethod (b4j.example.httpjob.class, "_getrequest" /*RemoteObject*/ ).runVoidMethod ("SetHeader",(Object)(BA.ObjectToString("Authorization")),(Object)(RemoteObject.concat(RemoteObject.createImmutable("key="),main._api_key)));
 BA.debugLineNum = 32;BA.debugLine="End Sub";
Debug.ShouldStop(-2147483648);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}