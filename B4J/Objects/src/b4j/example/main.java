package b4j.example;

import anywheresoftware.b4a.debug.*;

import anywheresoftware.b4a.BA;

public class main extends Object{
public static main mostCurrent = new main();

public static BA ba;
static {
		ba = new  anywheresoftware.b4a.shell.ShellBA("b4j.example", "b4j.example.main", null);
		ba.loadHtSubs(main.class);
        if (ba.getClass().getName().endsWith("ShellBA")) {
			anywheresoftware.b4a.shell.ShellBA.delegateBA = new anywheresoftware.b4a.StandardBA("b4j.example", null, null);
			ba.raiseEvent2(null, true, "SHELL", false);
			ba.raiseEvent2(null, true, "CREATE", true, "b4j.example.main", ba);
		}
	}
    public static Class<?> getObject() {
		return main.class;
	}

 
    public static void main(String[] args) throws Exception{
        try {
            anywheresoftware.b4a.keywords.Common.LogDebug("Program started.");
            initializeProcessGlobals();
            ba.raiseEvent(null, "appstart", (Object)args);
        } catch (Throwable t) {
			BA.printException(t, true);
		
        } finally {
            anywheresoftware.b4a.keywords.Common.LogDebug("Program terminated (StartMessageLoop was not called).");
        }
    }


private static boolean processGlobalsRun;
public static void initializeProcessGlobals() {
    
    if (main.processGlobalsRun == false) {
	    main.processGlobalsRun = true;
		try {
		        		
        } catch (Exception e) {
			throw new RuntimeException(e);
		}
    }
}public static anywheresoftware.b4a.keywords.Common __c = null;
public static String _api_key = "";
public static b4j.example.httputils2service _httputils2service = null;
public static String  _appstart(String[] _args) throws Exception{
RDebugUtils.currentModule="main";
if (Debug.shouldDelegate(ba, "appstart", false))
	 {return ((String) Debug.delegate(ba, "appstart", new Object[] {_args}));}
RDebugUtils.currentLine=65536;
 //BA.debugLineNum = 65536;BA.debugLine="Sub AppStart (Args() As String)";
RDebugUtils.currentLine=65537;
 //BA.debugLineNum = 65537;BA.debugLine="SendMessage(\"general\", \"title\", \"body\")";
_sendmessage("general","title","body");
RDebugUtils.currentLine=65538;
 //BA.debugLineNum = 65538;BA.debugLine="StartMessageLoop";
anywheresoftware.b4a.keywords.Common.StartMessageLoop(ba);
RDebugUtils.currentLine=65539;
 //BA.debugLineNum = 65539;BA.debugLine="End Sub";
return "";
}
public static String  _sendmessage(String _topic,String _title,String _body) throws Exception{
RDebugUtils.currentModule="main";
if (Debug.shouldDelegate(ba, "sendmessage", false))
	 {return ((String) Debug.delegate(ba, "sendmessage", new Object[] {_topic,_title,_body}));}
b4j.example.httpjob _job = null;
anywheresoftware.b4a.objects.collections.Map _m = null;
anywheresoftware.b4a.objects.collections.Map _data = null;
anywheresoftware.b4a.objects.collections.Map _iosalert = null;
anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator _jg = null;
RDebugUtils.currentLine=131072;
 //BA.debugLineNum = 131072;BA.debugLine="Private Sub SendMessage(Topic As String, Title As";
RDebugUtils.currentLine=131073;
 //BA.debugLineNum = 131073;BA.debugLine="Dim Job As HttpJob";
_job = new b4j.example.httpjob();
RDebugUtils.currentLine=131074;
 //BA.debugLineNum = 131074;BA.debugLine="Job.Initialize(\"fcm\", Me)";
_job._initialize /*String*/ (null,ba,"fcm",main.getObject());
RDebugUtils.currentLine=131075;
 //BA.debugLineNum = 131075;BA.debugLine="Dim m As Map = CreateMap(\"to\": $\"/topics/${Topic}";
_m = new anywheresoftware.b4a.objects.collections.Map();
_m = anywheresoftware.b4a.keywords.Common.createMap(new Object[] {(Object)("to"),(Object)(("/topics/"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_topic))+""))});
RDebugUtils.currentLine=131076;
 //BA.debugLineNum = 131076;BA.debugLine="Dim data As Map = CreateMap(\"title\": Title, \"body";
_data = new anywheresoftware.b4a.objects.collections.Map();
_data = anywheresoftware.b4a.keywords.Common.createMap(new Object[] {(Object)("title"),(Object)(_title),(Object)("body"),(Object)(_body)});
RDebugUtils.currentLine=131077;
 //BA.debugLineNum = 131077;BA.debugLine="If Topic.StartsWith(\"ios_\") Then";
if (_topic.startsWith("ios_")) { 
RDebugUtils.currentLine=131078;
 //BA.debugLineNum = 131078;BA.debugLine="Dim iosalert As Map =  CreateMap(\"title\": Title,";
_iosalert = new anywheresoftware.b4a.objects.collections.Map();
_iosalert = anywheresoftware.b4a.keywords.Common.createMap(new Object[] {(Object)("title"),(Object)(_title),(Object)("body"),(Object)(_body),(Object)("sound"),(Object)("default")});
RDebugUtils.currentLine=131079;
 //BA.debugLineNum = 131079;BA.debugLine="m.Put(\"notification\", iosalert)";
_m.Put((Object)("notification"),(Object)(_iosalert.getObject()));
RDebugUtils.currentLine=131080;
 //BA.debugLineNum = 131080;BA.debugLine="m.Put(\"priority\", 10)";
_m.Put((Object)("priority"),(Object)(10));
 };
RDebugUtils.currentLine=131082;
 //BA.debugLineNum = 131082;BA.debugLine="m.Put(\"data\", data)";
_m.Put((Object)("data"),(Object)(_data.getObject()));
RDebugUtils.currentLine=131083;
 //BA.debugLineNum = 131083;BA.debugLine="Dim jg As JSONGenerator";
_jg = new anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator();
RDebugUtils.currentLine=131084;
 //BA.debugLineNum = 131084;BA.debugLine="jg.Initialize(m)";
_jg.Initialize(_m);
RDebugUtils.currentLine=131085;
 //BA.debugLineNum = 131085;BA.debugLine="Job.PostString(\"https://fcm.googleapis.com/fcm/se";
_job._poststring /*String*/ (null,"https://fcm.googleapis.com/fcm/send",_jg.ToString());
RDebugUtils.currentLine=131086;
 //BA.debugLineNum = 131086;BA.debugLine="Job.GetRequest.SetContentType(\"application/json;c";
_job._getrequest /*anywheresoftware.b4h.okhttp.OkHttpClientWrapper.OkHttpRequest*/ (null).SetContentType("application/json;charset=UTF-8");
RDebugUtils.currentLine=131087;
 //BA.debugLineNum = 131087;BA.debugLine="Job.GetRequest.SetHeader(\"Authorization\", \"key=\"";
_job._getrequest /*anywheresoftware.b4h.okhttp.OkHttpClientWrapper.OkHttpRequest*/ (null).SetHeader("Authorization","key="+_api_key);
RDebugUtils.currentLine=131088;
 //BA.debugLineNum = 131088;BA.debugLine="End Sub";
return "";
}
public static String  _jobdone(b4j.example.httpjob _job) throws Exception{
RDebugUtils.currentModule="main";
if (Debug.shouldDelegate(ba, "jobdone", false))
	 {return ((String) Debug.delegate(ba, "jobdone", new Object[] {_job}));}
RDebugUtils.currentLine=196608;
 //BA.debugLineNum = 196608;BA.debugLine="Sub JobDone(job As HttpJob)";
RDebugUtils.currentLine=196609;
 //BA.debugLineNum = 196609;BA.debugLine="Log(job)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(_job));
RDebugUtils.currentLine=196610;
 //BA.debugLineNum = 196610;BA.debugLine="If job.Success Then";
if (_job._success /*boolean*/ ) { 
RDebugUtils.currentLine=196611;
 //BA.debugLineNum = 196611;BA.debugLine="Log(job.GetString)";
anywheresoftware.b4a.keywords.Common.Log(_job._getstring /*String*/ (null));
 };
RDebugUtils.currentLine=196613;
 //BA.debugLineNum = 196613;BA.debugLine="job.Release";
_job._release /*String*/ (null);
RDebugUtils.currentLine=196614;
 //BA.debugLineNum = 196614;BA.debugLine="ExitApplication '!";
anywheresoftware.b4a.keywords.Common.ExitApplication();
RDebugUtils.currentLine=196615;
 //BA.debugLineNum = 196615;BA.debugLine="End Sub";
return "";
}
}