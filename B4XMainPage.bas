﻿B4A=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=9.85
@EndOfDesignText@
#Region Shared Files
#CustomBuildAction: folders ready, %WINDIR%\System32\Robocopy.exe,"..\..\Shared Files" "..\Files"
'Ctrl + click to sync files: ide://run?file=%WINDIR%\System32\Robocopy.exe&args=..\..\Shared+Files&args=..\Files&FilesSync=True
#End Region

'Ctrl + click to export as zip: ide://run?File=%B4X%\Zipper.jar&Args=Project.zip

Sub Class_Globals
	Private Root As B4XView
	Private xui As XUI
	Dim Plcover,Plwhite As Panel
	Private BSingUp As Button
	Private EditText1 As EditText
End Sub

Public Sub Initialize
	
End Sub

'This event will be called once, before the page becomes visible.
Private Sub B4XPage_Created (Root1 As B4XView)
	Root = Root1
	Root.LoadLayout("MainPage")
	
	Plcover.Initialize("Plcover")
	Dim cd As ColorDrawable
	cd.Initialize(Colors.ARGB(150,0,0,0),0)
	Plcover.Background = cd
	Root.AddView(Plcover,0,0,Root.Width,Root.Height)
	Plcover.SetVisibleAnimated(0,False)
	Plcover.SetElevationAnimated(0,2dip)
	
	Plwhite.Initialize("Plwhite")
	cd.Initialize(Colors.White,30dip)
	Plwhite.Background = cd
	Root.AddView(Plwhite,0,Root.Height,Root.Width,Root.Height * .5)
	Plwhite.LoadLayout("account")
	Plwhite.SetElevationAnimated(0,4dip)
	
	SetBackgroundTintList(EditText1, Colors.Red, 0xFF0020FF)

	
End Sub

'You can see the list of page related events in the B4XPagesManager object. The event name is B4XPage.


Sub BSingUp_Click
	Plcover.SetVisibleAnimated(130,True)
	Sleep(200)
	Plwhite.SetLayoutAnimated(0,0,Root.Height * .5,Root.Width,Root.Height* .8)
End Sub

Sub Plcover_Click
	Plcover.SetVisibleAnimated(130,False)
	Sleep(200)
	Plwhite.SetLayoutAnimated(0,0,Root.Height,Root.Width,Root.Height* .8)
End Sub

Sub SetBackgroundTintList(View As View,Active As Int, Enabled As Int)
	Dim States(2,1) As Int
	States(0,0) = 16842908     'Active
	States(1,0) = 16842910    'Enabled
	Dim Color(2) As Int = Array As Int(Active,Enabled)
	Dim CSL As JavaObject
	CSL.InitializeNewInstance("android.content.res.ColorStateList",Array As Object(States,Color))
	Dim jo As JavaObject
	jo.InitializeStatic("android.support.v4.view.ViewCompat")
	jo.RunMethod("setBackgroundTintList", Array(View, CSL))
End Sub