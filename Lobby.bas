﻿B4A=true
Group=Default Group
ModulesStructureVersion=1
Type=Activity
Version=10.2
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: true
	#IncludeTitle: False
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
	Public RefUser As DatabaseReference
	
	Dim Chosser As ContentChooser
	
	Public ChatState As Boolean = False
End Sub

Sub Globals
	'These global variables will be redeclared each time the activity is created.
	'These variables can only be accessed from this module.
	Dim Pnlprofile As Panel
	Dim xui As XUI
	Dim AllUsers As List
	'Dim CurrentUser As User
	Dim listvieExraitam As Int
	Public chat1 As Chat
	Private ime As IME
	Public Storage As FirebasStorage
	Dim PanChat As Panel
	Dim RedCircle As Bitmap
	Dim BlckCircle As Bitmap
	Dim ProfilePic As Bitmap
	Dim PeronProfilePic As Bitmap
	Dim AllPersonPic As List
	Dim AllFreindspic As List
	
	Private TabStrip1 As TabStrip
	Private LabUserName As Label
	Private LabOptions As Label
	Private ImavUProfile As B4XView
	Private LabProfClose As Label
	Private CustomLisAllUsers As CustomListView
	Private ImageUserProfile2 As ImageView
	'Private ImageVRofilePicture As ImageView
	Private Labeldeletelistite As Label
	Private LabePersonstatus As Label
	Private LabePersonName As Label
	Private LabelAddPerson As Label
	Private PanelAddPErson As Panel
	Private CustomLisAllContacts As CustomListView
	Private PanelFriendPerson As Panel
	
	Private LabeRemoveFriend As Label
	Private Labeldeletelistite2 As Label
	Private LabeChatFriend As Label
	Private CustomLisAllMessages As CustomListView
	Private ImageVMessageProfil As ImageView
	Private LabelMessageName As Label
	Private LabRefresh As Label
	Private LabMessageNumbers As Label
	Private PaneMessage As Panel
	Private ImageViOnlineStatue As ImageView
	Private ImavCoverr As ImageView
	
	Dim rp As RuntimePermissions
	Dim Rs As RSPopupMenu
	Private dialog As B4XDialog
	'images 
	Dim ChatBack As Bitmap
	Dim PanlBack As Bitmap
	Dim ProfileBack As Bitmap
	'
	Private PaneProfilPerson As Panel
	
	Private ImageVRofilePicture As B4XView
	Private LabUserStatue As Label
End Sub

Sub Activity_Create(FirstTime As Boolean)
	Activity.LoadLayout("Lobby")
	
	AllPersonPic.Initialize
	AllFreindspic.Initialize
	
	Storage.Initialize
	DownloadImagesApp
	
	AllUsers.Initialize
	listvieExraitam = -1
	StartService(UseService)
	CallSubDelayed(UseService,"Inti")
	'
	'
	Sleep(2000)
	StartService(RomeService)
	CallSubDelayed(RomeService,"Inti")
	
	StartService(MessageService)
	MessageService.AllMessages.Initialize
	
	RefUser.Initialize("RefUser2",Main.realtime.getReferencefromUrl($"https://first-project-5317d.firebaseio.com/Users"$),"User")
	RefUser.addChildEventListener
	'RefUser.addListenerForSingleValueEvent
	'RefUser.addValueEventListener
	
	'CurrentUser = Main.CurrentUser
	
	TabStrip1.LoadLayout("Page1", "All Users")
	TabStrip1.LoadLayout("Page2", "My Contact")
	TabStrip1.LoadLayout("page3", "Chat Messages")
	
	
	Pnlprofile.Initialize("Pnlprofile")
	Dim cd As ColorDrawable
	cd.Initialize(Colors.White,0)
	Pnlprofile.Background = cd
	Activity.AddView(Pnlprofile,50dip,Activity.Height/2,70%x,70%y)
	'Activity.AddView(Pnlprofile,ImavUProfile.Left,ImavUProfile.Height,0,0)
	Pnlprofile.LoadLayout("UserProfile")
	Pnlprofile.SetLayoutAnimated(0,Activity.Width/2,ImavUProfile.Height,0,0)
	Pnlprofile.SetElevationAnimated(0,4dip)
	
	
	'chat	
	PanChat.Initialize("PanChat")
	cd.Initialize(Colors.White,30dip)
	PanChat.Background = cd
	Activity.AddView(PanChat,0,Activity.Height,Activity.Width,Activity.Height)
	chat1.Initialize(PanChat)
	PanChat.SetElevationAnimated(0,4dip)
	
	Activity.Title = "Chat Example"
	ime.Initialize("ime")
	ime.AddHeightChangedEvent
	
'	Sleep(300)
'	For i = 0 To AllUsers.Size +7
'		AddScrollviewItem(i)
'	Next
	RedCircle = LoadBitmap(File.DirAssets,"Green1.png")
	BlckCircle = LoadBitmap(File.DirAssets,"black1.png")
	
	
	Chosser.Initialize("chooser")
	
	'User info
	LabUserName.Text = Main.UserId
	LabUserStatue.Text = UseService.Status
	If Not(UseService.ProfilPhotoUrl == "") Then
		Storage.DownloadProfilImage(UseService.ProfilPhotoUrl,ProfilePic,UseService.ProfilPhotoUrl.SubString(6),False,0)
	Else
		ProfilePic = LoadBitmap(File.DirAssets,"unknownimage.png")
		Dim ph As B4XBitmap = ProfilePic
		ImavUProfile.SetBitmap(CreateRoundBitmap(ph,ImavUProfile.Width))
	End If
	
	'Info Menu
	Rs.Initialize("PopupMenu",LabOptions )
	Rs.AddMenuItem(0, 0, "Show My Information")
	Rs.AddMenuItem(1, 1, "Edit State")
	Rs.AddMenuItem(2, 2, "Details")
	dialog.Initialize(Activity)
	dialog.Title = "Edit State"
	
	Sleep(6000)
	DowloadPersonsProfPic

End Sub

'Sub mnu1_Click
'	TabStrip1.ScrollTo(0, True)
'End Sub
'
'Sub mnu2_Click
'	TabStrip1.ScrollTo(1, True)
'End Sub
'
'Sub mnu3_Click
'	TabStrip1.ScrollTo(20, True)
'End Sub

Sub TabStrip1_PageSelected (Position As Int)
	listvieExraitam = -1
	If(Position == 1) Then ' Upload all contact list	
		UpdateTabeFriend
	End If
End Sub

Sub Activity_Resume

End Sub

Sub Activity_Pause (UserClosed As Boolean)
	If(UserClosed) Then
		RefUser.Child(UseService.childd).updateChildren(CreateMap("IsOnline":False))
		UseService.IsOnline = False
		
		For i =0 To MessageService.AllMessages.Size-1
			Dim Sn As DataSnapshot
			Sn.Initialize(MessageService.AllMessages.Get(i))
			MessageService.refMessage.Child(Sn.Key).removeValue
		Next
	

		MessageService.AllMessages.Clear
		'MessageService.refMessage.keepSynced(False)
	End If
End Sub

Sub AddScrollviewItem(Value As String,Name As String,Status As String,Profpic As String)
	Dim pn As B4XView = xui.CreatePanel("")
	pn.SetLayoutAnimated(0,0,0,CustomLisAllUsers.AsView.Width,60dip)
	pn.LoadLayout("cellitem")
	LabePersonName.Text = Name
	LabePersonstatus.Text = Status
   ' Storage.DownloadProfilImage(Profpic,PeronProfilePic,"Photo.jpg",True)
	AllPersonPic.Add(Profpic)
    ImageVRofilePicture.SetBitmap(CreateRoundBitmap(LoadBitmap(File.DirAssets,"unknownimage.png"),ImageVRofilePicture.Width))
	'ImageVRofilePicture.Bitmap = CreateRoundBitmap(LoadBitmap(File.DirAssets,"unknownimage.png"),ImageVRofilePicture.Width)
	'PaneProfilPerson.Tag = Value
	ImageVRofilePicture.Tag = Profpic
	CustomLisAllUsers.Add(pn,Value)
End Sub

Sub AddScrollviewItem2(Value As String,Name As String,Status As String,Isonline As Boolean,profpic As String)
	Dim pn As B4XView = xui.CreatePanel("")
	pn.SetLayoutAnimated(0,0,0,CustomLisAllUsers.AsView.Width,60dip)
	pn.LoadLayout("cellitem")
	LabePersonName.Text = Name
	LabePersonstatus.Text = Status
	ImageVRofilePicture.SetBitmap(CreateRoundBitmap(LoadBitmap(File.DirAssets,"unknownimage.png"),ImageVRofilePicture.Width))
	If(Isonline) Then
		ImageViOnlineStatue.Bitmap = RedCircle
	Else
		ImageViOnlineStatue.Bitmap = BlckCircle
	End If
	ImageVRofilePicture.Tag = profpic
	CustomLisAllContacts.Add(pn,Value)
End Sub

public Sub AddScrollviewItem3(Value As String,Name As String)
	Dim pn As B4XView = xui.CreatePanel("")
	pn.SetLayoutAnimated(0,0,0,CustomLisAllUsers.AsView.Width,60dip)
	pn.LoadLayout("CellItemMessages")
	LabelMessageName.Text = Name
	'ImageVMessageProfil.Bitmap = CreateRoundBitmap(LoadBitmap(File.DirAssets,"unknownimage.png"),ImageVMessageProfil.Width)
	CustomLisAllMessages.Add(pn,Value)
End Sub

Sub RefUser2_onChildAdded(Snapshot As Object, child As String, tag As Object)
	Dim sn As DataSnapshot
	sn.Initialize(Snapshot)
	If(UseService.UserName <> sn.getChild("UserName").Value) Then
	AllUsers.Add(Snapshot)'Add one User to the list
	
	Dim PersonName As String = sn.getChild("UserName").Value
	Dim Status As String = sn.getChild("Status").Value
	Dim ProfPic As String = sn.getChild("ProfilPhotoURL").Value
	AddScrollviewItem(PersonName,PersonName,Status,ProfPic)
	End If
End Sub

Sub RefUser2_onChildRemoved(Snapshot As Object, tag As Object)
	Dim sn As DataSnapshot
	sn.Initialize(Snapshot)
	
	Dim ItemIndex As Int
	For i=0 To AllUsers.Size-1
		Dim snlist As DataSnapshot
		snlist.Initialize(AllUsers.Get(i))
		If(snlist.getChild("UserName").Value == sn.getChild("UserName").Value) Then
			ItemIndex = i
		End If
	Next
	
	AllUsers.RemoveAt(ItemIndex)
	CustomLisAllUsers.RemoveAt(ItemIndex)
End Sub

Sub RefUser2_onChildChanged(Snapshot As Object, child As String, tag As Object)
	Dim Sn As DataSnapshot
	Sn.Initialize(Snapshot)
	
'	If(Sn.ChildrenCount > 3) Then
'		If(Sn.getChild("Contact").getChild("UserName").Value == UseService.UserName) Then
'			Log("Child Changed")
'			UseService.refContact.push.setValue(CreateMap("UserName":Sn.getChild("UserName").Value),"Contact","")
''		For i = 0 To AllUsers.Size -1
''				Dim Snus As DataSnapshot
''				Snus.Initialize(AllUsers.Get(i))
''				If(Snus.getChild("UserName").Value == Sn.getChild("UserName").Value) Then
''					AddScrollviewItem2(Snus.getChild("UserName").Value,Snus.getChild("UserName").Value,Snus.getChild("Status").Value)
''				End If
''		Next		
'		End If
'	End If
For i =0 To AllUsers.Size-1
		Dim Snuser As DataSnapshot
		Snuser.Initialize(AllUsers.Get(i))
		If(Sn.Key == Snuser.Key) Then
			AllUsers.Set(i,Snapshot)
			'change inonline in all contact friend
			If(Snuser.getChild("UserName").Value <> UseService.UserName) Then
				Dim ison As Boolean = Sn.getChild("IsOnline").Value
				For k =0 To CustomLisAllContacts.Size-1
					If(CustomLisAllContacts.GetValue(k) == Snuser.getChild("UserName").Value) Then
						Dim img As ImageView = CustomLisAllContacts.GetPanel(k).GetView(0).GetView(3)
						If(ison) Then
							img.Bitmap = RedCircle
						Else
							img.Bitmap = BlckCircle
						End If
					End If
				Next
				
'				Dim Photo As String = Sn.getChild("ProfilPhotoURL").Value
'				For k =0 To CustomLisAllContacts.Size-1
'					If(CustomLisAllContacts.GetValue(k) == Snuser.getChild("UserName").Value) Then
'						'AllFreindspic.Set(CustomLisAllContacts.,Photo)
'						DowloadFriendsProfPic
'					End If
'				Next
			End If
			UpdateTabeFriend
		End If
Next


End Sub

Sub CreateRoundBitmap (Input As B4XBitmap, Size As Int) As B4XBitmap
	If Input.Width <> Input.Height Then
		'if the image is not square then we crop it to be a square.
		Dim l As Int = Min(Input.Width, Input.Height)
		Input = Input.Crop(Input.Width / 2 - l / 2, Input.Height / 2 - l / 2, l, l)
	End If
	Dim c As B4XCanvas
	Dim xview As B4XView = xui.CreatePanel("")
	xview.SetLayoutAnimated(0, 0, 0, Size, Size)
	c.Initialize(xview)
	Dim path As B4XPath
	path.InitializeOval(c.TargetRect)
	c.ClipPath(path)
	c.DrawBitmap(Input.Resize(Size, Size, False), c.TargetRect)
	c.RemoveClip
	c.DrawCircle(c.TargetRect.CenterX, c.TargetRect.CenterY, c.TargetRect.Width / 2 - 2dip, xui.Color_White, False, 5dip) 'comment this line to remove the border
	c.Invalidate
	Dim res As B4XBitmap = c.CreateBitmap
	c.Release
	Return res
End Sub

Sub ImavUProfile_Click
	Pnlprofile.SetLayoutAnimated(300,50dip,50dip,70%x,70%y)
	ProfilePic.Resize(ImageUserProfile2.Width,ImageUserProfile2.Height,True)
	'Storage.DownloadImage(UseService.ProfilPhotoUrl,ProfilePic,UseService.ProfilPhotoUrl.SubString(6))
	ImageUserProfile2.Bitmap = ProfilePic
	'Dim imgN As String = UseService.ProfilPhotoUrl
	LabUserName.Text = Main.UserId
	LabUserStatue.Text = UseService.Status
End Sub

Sub LabProfClose_Click
	Pnlprofile.SetLayoutAnimated(300,ImavUProfile.Left,ImavUProfile.Height,0,0)
	'ProfilePic = LoadBitmap(File.DirAssets,"unknownimage.png")
End Sub

Sub CustomLisAllUsers_ItemClick (Index As Int, Value As Object)
	If(Index <> listvieExraitam Or listvieExraitam == -1) Then
		If(listvieExraitam == -1) Then
			listvieExraitam = 0
		End If
	Dim pn As B4XView = xui.CreatePanel("")
	pn.SetLayoutAnimated(300,0,0,CustomLisAllUsers.AsView.Width,40dip)
	pn.LoadLayout("CellitemClicked")
	PanelAddPErson.Tag = Value
	If(listvieExraitam <> 0) Then
		CustomLisAllUsers.RemoveAt(listvieExraitam)
	'ImageVRofilePicture.Bitmap = CreateRoundBitmap(LoadBitmap(File.DirAssets,"unknownimage.png"),ImageVRofilePicture.Width)
	End If	
	
		If(Index == CustomLisAllUsers.Size) Then
		CustomLisAllUsers.Add(pn,Value)
			listvieExraitam = Index
		Else
			CustomLisAllUsers.InsertAt(Index+1,pn,Value)
			listvieExraitam = Index + 1
		End If
	End If
End Sub

Sub CustomLisAllContacts_ItemClick (Index As Int, Value As Object)
	If(Index <> listvieExraitam Or listvieExraitam == -1) Then
		If(listvieExraitam == -1) Then
			listvieExraitam = 0
		End If
		Dim pn As B4XView = xui.CreatePanel("")
		pn.SetLayoutAnimated(300,0,0,CustomLisAllContacts.AsView.Width,40dip)
		pn.LoadLayout("CellItemClickPage2")
		PanelFriendPerson.Tag = Value
		If(listvieExraitam <> 0) Then
			CustomLisAllContacts.RemoveAt(listvieExraitam)
			'ImageVRofilePicture.Bitmap = CreateRoundBitmap(LoadBitmap(File.DirAssets,"unknownimage.png"),ImageVRofilePicture.Width)
		End If
	
		If(Index == CustomLisAllContacts.Size) Then
			CustomLisAllContacts.Add(pn,Value)
			listvieExraitam = Index
		Else
			CustomLisAllContacts.InsertAt(Index+1,pn,Value)
			listvieExraitam = Index + 1
		End If
	End If
End Sub

Sub Labeldeletelistite_Click
	CustomLisAllUsers.RemoveAt(listvieExraitam)
	listvieExraitam = -1
End Sub

Sub Labeldeletelistite2_Click
	CustomLisAllContacts.RemoveAt(listvieExraitam)
	listvieExraitam = -1
End Sub

Sub LabelAddPerson_Click
	Dim PersonName As String = PanelAddPErson.Tag
	Dim map As Map
	map.Initialize
	map.Put("UserName",PersonName)
	'see if have friend in contact
	Dim Ishave As Boolean = False
	For i=0 To UseService.Contact.Size-1
	Dim snlist As DataSnapshot
	snlist.Initialize(UseService.Contact.Get(i))
	If(snlist.getChild("UserName").Value == PersonName) Then
		Ishave = True
	End If
	Next
	
	If(Ishave) Then
	ToastMessageShow("You have "&PersonName&" in friend list",True)
	Else
	UseService.refContact.push.setValue(map,"Contact","")'Add to contact list
	
	For i = 0 To AllUsers.Size-1 ' Add me to person contact
			Dim snlist As DataSnapshot
			snlist.Initialize(AllUsers.Get(i))
			If(snlist.getChild("UserName").Value == PersonName) Then
				Dim refAdd As DatabaseReference
				refAdd.Initialize("RefAdd",Main.realtime.getReferencefromUrl("https://first-project-5317d.firebaseio.com/Users/"&snlist.Key&"/Contact"),"User")
				refAdd.push.setValue(CreateMap("UserName":UseService.UserName),"Contact","")
				'RefUser.addChildEventListener
				'snlist.getChild("Contact").DBRef.Child("Contact").push.setValue(CreateMap("UserName":UseService.UserName),"Contact","")
			End If
	Next
	Log(RomeService.AllRooms.Size)
	RomeService.refRoom.push.setValue(CreateMap("Name":".","Messages":"","Members":CreateMap("Member1":UseService.UserName,"Member2":PersonName)),"Rooms","Roo")
	End If
End Sub

Sub LabeRemoveFriend_Click

	Dim PersonName As String = PanelFriendPerson.Tag
	Dim ItemIndex As Int
	For i=0 To UseService.Contact.Size-1
		Dim snlist As DataSnapshot
		snlist.Initialize(UseService.Contact.Get(i))
		If(snlist.getChild("UserName").Value == PersonName) Then
			ItemIndex = i
			UseService.refContact.Child(snlist.Key).removeValue
			
			For k = 0 To AllUsers.Size-1 ' Remove me to person contact
				Dim sn As DataSnapshot
				sn.Initialize(AllUsers.Get(k))
				If(sn.getChild("UserName").Value == PersonName) Then
					Dim refRemove As DatabaseReference
					refRemove.Initialize("RefRemove",Main.realtime.getReferencefromUrl("https://first-project-5317d.firebaseio.com/Users/"&sn.Key&"/Contact"),"personCont")					
					Dim lisChild As List
					lisChild = sn.getChild("Contact").Children
					Log(lisChild.Size)
					For j =0 To lisChild.Size-1
						Dim schild As DataSnapshot
						schild= lisChild.Get(j)
						If(schild.getChild("UserName").Value == UseService.UserName) Then
							refRemove.Child(schild.Key).removeValue
						End If
					Next
					'snlist.getChild("Contact").DBRef.Child("Contact").push.setValue(CreateMap("UserName":UseService.UserName),"Contact","")
				End If
			Next
			
			'
			
		End If
	Next
	
	'Remove Room between tow friend
	Dim person1 As String = PersonName
	Dim person2 As String = UseService.UserName
	For k = 0 To RomeService.AllRooms.Size-1
		Dim sn As DataSnapshot
		sn.Initialize(RomeService.AllRooms.Get(k))
		If(sn.getChild("Members").getChild("Member1").Value == person1 Or sn.getChild("Members").getChild("Member2").Value == person1) Then
			If(sn.getChild("Members").getChild("Member1").Value == person2 Or sn.getChild("Members").getChild("Member2").Value == person2) Then
				RomeService.refRoom.Child(sn.Key).removeValue
				RomeService.AllRooms.RemoveAt(k)
			End If
		End If
	Next
	
	UseService.Contact.RemoveAt(ItemIndex)
	CustomLisAllContacts.RemoveAt(ItemIndex+1)
	CustomLisAllContacts.RemoveAt(ItemIndex)
	
	CustomLisAllMessages.RemoveAt(ItemIndex)
	AllPersonPic.RemoveAt(ItemIndex)
End Sub


Sub LabeChatFriend_Click
	MessageService.AllMessages.Clear
	For i =0 To RomeService.AllRooms.Size-1
		Dim Ds As DataSnapshot
		Ds.Initialize(RomeService.AllRooms.Get(i))
		Dim m1,m2 As String
		m1 = Ds.getChild("Members").getChild("Member1").Value
		m2 = Ds.getChild("Members").getChild("Member2").Value
		If(UseService.UserName == m1 Or UseService.UserName == m2) Then
			If(PanelFriendPerson.Tag == m1 Or PanelFriendPerson.Tag == m2) Then
				MessageService.Objest = Ds.Key
				If (Not(MessageService.ReceiverMember == PanelFriendPerson.Tag) Or (Not(MessageService.refMessage.IsInitialized))) Then
				CallSubDelayed(MessageService,"Inti")
				Log("find")
				End If
				
				MessageService.SenderMember = UseService.UserName
				MessageService.ReceiverMember = PanelFriendPerson.Tag
			End If
		End If
	Next
	
	PanChat.SetLayoutAnimated(300,0,0,Activity.Width,Activity.Height)
	
	Sleep(3000)
	CallSub(MessageService,"prossesMessage")
	
	ChatState = True
End Sub

'Public Sub ReciveMessage(M As String,isr As Boolean)
'	chat1.Recieve(M,isr)
'End Sub
'
'Public Sub SentMessage(M As String,isr As Boolean)
'	chat1.Sent(M,isr)
'End Sub

Public Sub ReciveMessageCustoview(M As String,isr As Boolean)
	Dim isMessage As Boolean
	If(M.CharAt(0) == "/") Then
		isMessage = False
		Else
			isMessage = True
	End If
	If(isMessage) Then
	    chat1.AddItem(M,isr,MessageService.ReceiverMember)
	Else
		Dim IsPhoto As Boolean = True
		If(M.CharAt(M.Length-1) == "v") Then
			IsPhoto = False
		End If
		
		If(IsPhoto) Then
		Storage.DownloadImageCover(M,M,isr,chat1.chooserBitmap)
		Else
			Storage.DownloadVoiceCover(M,M,isr)
		End If
		'Sleep(4000)
		'chat1.AddImage(M,isr,chat1.chooserBitmap)
	End If
End Sub

Public Sub SentMessageCustomview(M As String,isr As Boolean)
	Dim isMessage As Boolean
	If(M.CharAt(0) == "/") Then
		isMessage = False
	Else
		isMessage = True
	End If
	
	If (isMessage) Then
	    chat1.AddItem(M,isr,MessageService.SenderMember)
	Else
		Dim IsPhoto As Boolean = True
		If(M.CharAt(M.Length-1) == "v") Then
			IsPhoto = False
		End If
		
		If(IsPhoto) Then
			Storage.DownloadImageCover(M,M,isr,chat1.chooserBitmap)
		Else
			Storage.DownloadVoiceCover(M,M,isr)
		End If
		'chat1.AddImage(M,isr,chat1.chooserBitmap)
	End If
End Sub

Public Sub AddImageToChat(M As String,isr As Boolean)
	chat1.AddImage(M,isr,chat1.chooserBitmap)
End Sub

Public Sub AddVoiceToChat(M As String,isr As Boolean)
	chat1.AddIAudio(chat1.Dirvoice,chat1.FileNameVoice,isr)
End Sub

Public Sub insializChatvoice(Dir As String,FilName As String)
	chat1.Dirvoice = Dir
	chat1.FileNameVoice = FilName
End Sub

Public Sub SentImageToDataBase(FilName As String)
	chat1.Sent("/message/"&FilName)
End Sub

Public Sub SentvoiceToDataBase
	chat1.Sent("/message/"&"myRecording"&chat1.AudioNumber&".wav")
End Sub

Public Sub CloseChat
	chat1.ClearMessage
	PanChat.SetLayoutAnimated(300,0,Activity.Height,Activity.Width,Activity.Height)
	ChatState = False
'	For i =0 To MessageService.AllMessages.Size-1
'		Dim Sn As DataSnapshot
'		Sn.Initialize(MessageService.AllMessages.Get(i))
'		MessageService.refMessage.Child(Sn.Key).removeValue
'	Next
'	
'
'	MessageService.AllMessages.Clear
'	MessageService.refMessage.keepSynced(False)
End Sub


Sub LabRefresh_Click
	
End Sub


Sub CustomLisAllMessages_ItemClick (Index As Int, Value As Object)
	MessageService.AllMessages.Clear
	For i =0 To RomeService.AllRooms.Size-1
		Dim Ds As DataSnapshot
		Ds.Initialize(RomeService.AllRooms.Get(i))
		Dim m1,m2 As String
		m1 = Ds.getChild("Members").getChild("Member1").Value
		m2 = Ds.getChild("Members").getChild("Member2").Value
		If(UseService.UserName == m1 Or UseService.UserName == m2) Then
			If(Value == m1 Or Value== m2) Then
				If (Not(MessageService.ReceiverMember == Value) Or (Not(MessageService.refMessage.IsInitialized))) Then
				MessageService.Objest = Ds.Key
				CallSubDelayed(MessageService,"Inti")
				Log("find")
				End If
				
				MessageService.SenderMember = UseService.UserName
				MessageService.ReceiverMember = Value
			End If
		End If
	Next
	PanChat.SetLayoutAnimated(400,0,0,Activity.Width,Activity.Height)
	ChatState = True
End Sub

Public Sub AddMessageToLable(RecUser As String)
	'If Not(MessageService.refMessage.IsInitialized) Then
		For i =0 To CustomLisAllMessages.Size-1
			If(CustomLisAllMessages.GetValue(i) == RecUser) Then
			Dim pnluser As B4XView
			pnluser = CustomLisAllMessages.GetPanel(i)
			Dim lb As Label
			lb = pnluser.GetView(0).GetView(3)
			Dim valuse As Int = lb.Text
			valuse = valuse+1
			lb.Text = valuse
			End If
		Next
	'End If
End Sub

Sub UpdateTabeFriend
	CustomLisAllContacts.Clear
	AllFreindspic.Clear
	For j = 0 To UseService.Contact.Size -1
		Dim sn As DataSnapshot
		sn.Initialize(UseService.Contact.Get(j))
		For i = 0 To AllUsers.Size -1
			Dim snn As DataSnapshot
			snn.Initialize(AllUsers.Get(i))
			If(sn.getChild("UserName").Value == snn.getChild("UserName").Value) Then
				Dim ison As Boolean = snn.getChild("IsOnline").Value
				Dim photoUrl As String = snn.getChild("ProfilPhotoURL").Value
				AllFreindspic.Add(photoUrl)
				AddScrollviewItem2(sn.getChild("UserName").Value,sn.getChild("UserName").Value,snn.getChild("Status").Value,ison,photoUrl)
			End If
		Next
	Next
	DowloadFriendsProfPic
End Sub

'imaes Backround
Sub DownloadImagesApp
	'Download App images
'	Storage.DownloadImage("/public/chatbackground.jpg",ChatBack,"ChatBack.jpg")
'	Sleep(3000)
'	chat1.SetBackChatImag(ChatBack)
	Storage.DownloadImage("/public/2835be38b5274a4b20155999a7613542.jpg",PanlBack,"PanlBack.jpg")
	Sleep(3000)
	PanlBack.Resize(ImavCoverr.Width,ImavCoverr.Height,True)
	ImavCoverr.Bitmap = PanlBack
	Storage.DownloadImage("/public/unknownimage.png",ProfileBack,"ProfilBack.jpg")
	Sleep(3000)
	ProfileBack.Resize(ImageUserProfile2.Width,ImageUserProfile2.Height,True)
	ImageUserProfile2.Bitmap = ProfileBack
	
End Sub

Sub ButChangeProfPic_Click
	Chosser.Show("image/*", "Choose image")
	End Sub

Sub chooser_Result (Success As Boolean, Dir As String, FileName As String)
	If Success Then
        Storage.UploadProfilImage("/user/"&FileName.SubString(61)&".jpg",Dir,FileName)
		UseService.ProfilPhotoUrl = "/user/"&FileName.SubString(61)&".jpg"
		
		Dim imag As Bitmap
		imag.Initialize(Dir,FileName)
		ImageUserProfile2.Bitmap = imag
	Else
		ToastMessageShow("No image selected", True)
	End If
	
	'CallSub(MessageService,"prossesMessage")
End Sub

Public Sub sentimage(Url As String)
'	For i =0 To AllUsers.Size-1
	'	Dim sn As DataSnapshot
	'	sn.Initialize(AllUsers.Get(i))
		
	'	If(sn.Key == UseService.childd) Then
		'	Log("find wadi")
			RefUser.Child(UseService.childd).updateChildren(CreateMap("ProfilPhotoURL":Url))
			UseService.ProfilPhotoUrl = Url
	        Storage.DownloadProfilImage(UseService.ProfilPhotoUrl,ProfilePic,UseService.ProfilPhotoUrl.SubString(6),False,0)
		'End If
	'Next
End Sub

Public Sub UserProfilePicSet
	Dim Pic As B4XBitmap = ProfilePic
	Pic = CreateRoundBitmap(Pic,ImavUProfile.Width)
	ImavUProfile.SetBitmap(Pic)
End Sub

Public Sub PersonProfilePicSet(picc As Bitmap,i As Int)
	Dim Pic As B4XBitmap = picc
	'customPerson list
	Dim pnluser As B4XView
	pnluser = CustomLisAllUsers.GetPanel(i)
	Dim imv As B4XView = pnluser.GetView(0).GetView(1)
	'imv.Bitmap = pic
	imv.SetBitmap(CreateRoundBitmap(Pic,imv.Width))
	
'	'custonlist friends
'	If(CustomLisAllContacts.Size > 0) Then
'	Dim pnluser2 As B4XView
'	pnluser2 = CustomLisAllContacts.GetPanel(i)
'	Dim imv2 As B4XView = pnluser2.GetView(0).GetView(1)
'	'imv.Bitmap = pic
'	imv2.SetBitmap(CreateRoundBitmap(Pic,imv2.Width))
'	End If
End Sub

Public Sub FriendProfilePicSet(picc As Bitmap,i As Int)
	Dim Pic As B4XBitmap = picc
	'customPerson list
	Dim pnluser As B4XView
	pnluser = CustomLisAllContacts.GetPanel(i)
	Dim imv As B4XView = pnluser.GetView(0).GetView(1)
	'imv.Bitmap = pic
	imv.SetBitmap(CreateRoundBitmap(Pic,imv.Width))
End Sub

Sub DowloadPersonsProfPic
    Storage.DownloadProfilPersonImage(AllPersonPic)
End Sub

Sub DowloadFriendsProfPic
	Storage.DownloadProfilFriendsImage(AllFreindspic)
End Sub

Sub ImageVRofilePicture_Click
	Dim imageProfil As ImageView = Sender
	Dim pic As Bitmap
	Dim picurl As String = imageProfil.Tag
	If(picurl <> "") Then
	Pnlprofile.SetLayoutAnimated(0,imageProfil.Left,imageProfil.Height,0,0)
	Pnlprofile.SetLayoutAnimated(300,50dip,50dip,70%x,70%y)
	Storage.DownloadImage(imageProfil.Tag,pic,picurl.SubString(6))
	ImageUserProfile2.Bitmap = pic
	End If
	
	Dim pn As Panel = imageProfil.Parent
	Dim labname As Label = pn.GetView(0)
	Dim labState As Label = pn.GetView(2)
	LabUserName.Text = labname.Text
	LabUserStatue.Text = labState.Text
End Sub

'Menu info

Sub PopupMenu_Dismiss
	'ToastMessageShow("PopupMenu dismissed", False)
End Sub

Sub PopupMenu_MenuItemClick (ItemId As Int) As Boolean
	'ToastMessageShow("Item " & ItemId & " clicked.", False)
	If(ItemId == 0) Then
		Pnlprofile.SetLayoutAnimated(300,50dip,50dip,70%x,70%y)
		ProfilePic.Resize(ImageUserProfile2.Width,ImageUserProfile2.Height,True)
		'Storage.DownloadImage(UseService.ProfilPhotoUrl,ProfilePic,UseService.ProfilPhotoUrl.SubString(6))
		ImageUserProfile2.Bitmap = ProfilePic
	Else if (ItemId == 1) Then
    ChangeState
	End If
	Return False
End Sub

Sub ChangeState
	Dim input As B4XInputTemplate
	input.Initialize
	input.lblTitle.Text = "Enter Text:"
	'input.ConfigureForNumbers(True, True)
	Wait For (dialog.ShowTemplate(input, "OK", "", "CANCEL")) Complete (Result As Int)
	If Result = xui.DialogResponse_Positive Then
		Dim res As String = input.TextField1.Text 'no need to check with IsNumber
		UseService.Status = res
		RefUser.Child(UseService.childd).updateChildren(CreateMap("Status":res))
	End If
End Sub

Sub LabOptions_Click
	Rs.Show
End Sub