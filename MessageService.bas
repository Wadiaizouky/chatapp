﻿B4A=true
Group=Default Group
ModulesStructureVersion=1
Type=Service
Version=10.2
@EndOfDesignText@
#Region  Service Attributes 
	#StartAtBoot: False
	
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
	Public refMessage As DatabaseReference
	Public AllMessages As List
	
	Public Objest As Object
	
	Public SenderMember As String
	Public ReceiverMember As String
	Public LastSender As String
	
	Public Firsttime As Boolean = True
End Sub

Sub Service_Create

End Sub

Sub Service_Start (StartingIntent As Intent)
	Service.StopAutomaticForeground 'Call this when the background task completes (if there is one)
End Sub

Public Sub Inti()
	'Service.StopAutomaticForeground 'Call this when the background task completes (if there is one)
	
	'AllMessages.Initialize
	refMessage.Initialize("RefMessages",Main.realtime.getReferencefromUrl("https://first-project-5317d.firebaseio.com/Rooms/"&Objest&"/Messages"),"Message")
	refMessage.addChildEventListener
	'refRoom.addListenerForSingleValueEvent
	'refRoom.addValueEventListener

End Sub

Sub RefMessages_onChildAdded(Snapshot As Object, child As String, tag As Object)
	AllMessages.Add(Snapshot)
	Log("Message added ..")
	
	If Not(Firsttime) Then
	Dim Sn As DataSnapshot
	Sn.Initialize(Snapshot)
	
	LastSender = Sn.getChild("Sender").Value
	If(Sn.getChild("Sender").Value <> UseService.UserName) Then
		CallSubDelayed3(Lobby,"ReciveMessageCustoview",Sn.getChild("Message").Value,True)
	Else
		CallSubDelayed3(Lobby,"SentMessageCustomview",Sn.getChild("Message").Value,False)
	End If
	End If
End Sub

Sub RefMessages_onChildChanged(Snapshot As Object, child As String, tag As Object)

End Sub

Sub Service_Destroy

End Sub

public Sub prossesMessage
	For i = 0 To AllMessages.Size-1
		Dim Sn As DataSnapshot
		Sn.Initialize(AllMessages.Get(i))
		
		Dim ms As String = Sn.getChild("Message").Value
		If(ms.CharAt(0) == "/" ) Then
			Sleep(4000)
		End If
	
		If(Sn.getChild("Sender").Value <> UseService.UserName) Then
			CallSubDelayed3(Lobby,"ReciveMessageCustoview",Sn.getChild("Message").Value,True)
		Else
			CallSubDelayed3(Lobby,"SentMessageCustomview",Sn.getChild("Message").Value,False)
		End If
	Next
	
	AllMessages.Clear
	Firsttime = False
End Sub
