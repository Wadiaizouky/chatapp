﻿B4A=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=7.01
@EndOfDesignText@
'
' Class for FireBase Database REST operations
'           with access token auth
' (works only if FireBase setup conditions are met)
'
' fredo JULY 2016
' unfinished version 08
'
Sub Class_Globals
	Private strFiBaDbRoot As String = ""
	Private strCallBack1 As String = ""	
	Private objCaller1 As Object
End Sub

#Region ---- RULES Info to copy paste to FiBaDb RULES console while developing ---

'		// These rules require authentication
'		{
'		  "rules": {
'		    ".read": "auth != null",
'		    ".write": "auth != null"
'		  }
'		}


'		// These rules give anyone, even people who are Not users of your app,
'		// read And write access To your database
'		{
'		  "rules": {
'		    ".read": True,
'		    ".write": True
'		  }
'		}


'		// These rules grant access To a node matching the authenticated
'		// user's ID from the Firebase auth token
'		{
'		  "rules": {
'		    "users": {
'		      "$uid": {
'		        ".read": "$uid === auth.uid",
'		        ".write": "$uid === auth.uid"
'		      }
'		    }
'		  }
'		}


'		// These rules don't allow anyone read or write access to your database
'		{
'		  "rules": {
'		    ".read": False,
'		    ".write": False
'		  }
'		}




' -->http://stackoverflow.com/questions/38249664/firebase-custom-token-vs-service-account/38622640#38622640

'	With the service account, you are hard coding access To your database To all installed applications, in other words, everyone that installs your app, will see the exact same thing.
'	You would normally use the token approach so you tailor access To different nodes on the database To different users. This Is normally achieved by setting a few simple rules For your nodes, For example :
'
'	    // Sample firebase rules
'	    {
'	      "rules": {
'	        "messages": {
'	          // Only admin servers with service accounts should r/w here
'	          ".read": "auth.uid == 'server-with-svc-acct'",
'	          ".write": "auth.uid == 'server-with-svc-acct'",
'	          "$user_id": {
'	             // Users can only read their own nodes
'	             ".read": "auth.uid == $user_id",
'	             ".write": "auth.uid ==  $user_id",
'
'	          }
'	        }
'	      }
'	    }

#End Region


' Sets the basis parameter for communications with firebase
Public Sub Initialize(objCaller As Object, strFirebaseDbRoot As String, strCallBackSub As String)
	objCaller1 = objCaller
	strCallBack1 = strCallBackSub

	If strFirebaseDbRoot.EndsWith("/") Then 
		strFiBaDbRoot = strFirebaseDbRoot
	Else
		strFiBaDbRoot = strFirebaseDbRoot & "/"
	End If
End Sub

' Push mapData to strPath
' A new node will be genereated under a new generated key (e.g. "-KNp...")
Public Sub POST(strPath As String, mapData As Map, strJobTag As String) 
	Dim jsonGen As JSONGenerator
	jsonGen.Initialize(mapData)
	Dim strJsonToSend As String = jsonGen.ToString

	Dim job1 As HttpJob
	job1.Initialize("FiBaDb_write", Me)
	job1.Tag = strJobTag
	job1.PostString(strFiBaDbRoot & strPath & ".json", strJsonToSend)
	job1.GetRequest.SetContentType("application/json")
End Sub

' Write mapData to FiBaDb
'  If path exists, then the value will be modified
'  If path don't exists, then the path and the value will be added
Public Sub PUT(strPath As String, mapData As Map, strJobTag As String) 
	Dim jsonGen As JSONGenerator
	jsonGen.Initialize(mapData)
	Dim strJsonToSend As String = jsonGen.ToString

	Dim job1 As HttpJob
	job1.Initialize("FiBaDb_write", Me)
	job1.Tag = strJobTag
	job1.PutBytes(strFiBaDbRoot & strPath & ".json" , strJsonToSend.GetBytes("UTF8"))
End Sub



Public Sub PATCH(strPath As String, mapData As Map, strJobTag As String) ' not startet
	' 
	' ... not finished yet, I'm working on it
	Dim jsonGen As JSONGenerator
	jsonGen.Initialize(mapData)
	Dim strJsonToSend As String = jsonGen.ToString

	Dim job1 As HttpJob
	job1.Initialize("FiBaDb_write", Me)
	job1.Tag = strJobTag
	job1.PatchBytes(strFiBaDbRoot & strPath & ".json" , strJsonToSend.GetBytes("UTF8"))
	'
End Sub



' Delete data from path
'   The path and all of its data below will be removed 
'   (be careful --> you may loose all your data!!)
Public Sub DELETE(strPath As String, strJobTag As String) 
	Dim job1 As HttpJob
	job1.Initialize("FiBaDb_delete", Me)
	job1.Tag = strJobTag
	job1.Delete(strFiBaDbRoot & strPath & ".json"  & "?auth=" & Starter.strFirebaseSignedInUserToken)
End Sub

' Standard GET
'   The data of the path and all data below will be returned
public Sub GET(strPath As String, strJobTag As String)  
	Dim job1 As HttpJob
	job1.Initialize("FiBaDb_read", Me)
	job1.Tag = strJobTag
	job1.Download(strFiBaDbRoot & strPath & ".json"  & "?auth=" & Starter.strFirebaseSignedInUserToken) 
End Sub

public Sub GET2(strPath As String, strJobTag As String)  
	Dim job1 As HttpJob
	job1.Initialize("FiBaDb_read", Me)
	job1.Tag = strJobTag
	job1.Download(strFiBaDbRoot & strPath & ".json"  & "?auth=" & Starter.strFirebaseSignedInUserToken) 
End Sub

' GET with callback
'   The data of the path and all data below will be returned
'   to the callback sub
public Sub GET_cb(strPath As String, strJobTag As String, strCallback As String)  
	Dim job1 As HttpJob
	job1.Initialize("FiBaDb_read", Me)
	job1.Tag = strJobTag
	job1.Download(strFiBaDbRoot & strPath & ".json"  & "?auth=" & Starter.strFirebaseSignedInUserToken & "&callback=" & strCallback) 
End Sub

' GET with download
'   The data of the path and all data below will be returned
'   and saved as a file
public Sub GET_dl(strPath As String, strJobTag As String, strFile As String)  
	Dim job1 As HttpJob
	job1.Initialize("FiBaDb_read", Me)
	job1.Tag = strJobTag
	job1.Download(strFiBaDbRoot & strPath & ".json"  & "?auth=" & Starter.strFirebaseSignedInUserToken & "&download=" & strFile) 
End Sub

' --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- 
' Result
' --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- 
Sub JobDone (Job As HttpJob)
	Dim mapRet As Map : mapRet.Initialize
	mapRet.Put("Job.JobName", Job.JobName)
	mapRet.Put("Job.Success", Job.Success)
	mapRet.Put("Job.Tag", Job.Tag)
	
	If Job.Success Then 
		mapRet.Put("Job.Getstring", Job.GetString)
		mapRet.Put("Job.ErrorMessage", "")
		mapRet.Put("Job.info", Job.GetString)
	Else
		mapRet.Put("Job.Getstring", "")
		mapRet.Put("Job.ErrorMessage", Job.ErrorMessage)
		mapRet.Put("Job.info", Job.ErrorMessage)
	End If
	
	If SubExists(objCaller1, strCallBack1) Then 
		CallSubDelayed2(objCaller1, strCallBack1, mapRet)
	End If
	
    Job.Release
End Sub
 

' --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- 
' Info
' --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- 
' GET --> https://firebase.google.com/docs/database/rest/retrieve-data
' POST, PUT, PATCH, DELETE --> https://firebase.google.com/docs/database/rest/save-data
' --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- 
'	Ways To Save Data
'		PUT 	Write Or replace data To a defined path, like messages/users/user1/<data>
'		*PATCH 	Update some of the keys For a defined path without replacing all of the data.
'		POST 	Add To a list of data in our Firebase database. *** Every time we send a POST request, the Firebase client generates a unique key, like messages/users/<unique-id>/<data>
'		DELETE 	Remove data from the specified Firebase database reference.
' --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- 
'   * unfinished

#Region --- Firebase Docs ---
	' *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** 
	'     Information from --> https://firebase.google.com/docs/reference/rest/database/

	'			 Firebase Database REST API
	'			API Usage
	'
	'			We can use any Firebase Database URL As a REST endpoint. All we need To Do Is append .json To the end of the URL And send a request from our favorite HTTPS client.
	'			HTTPS Is required. Firebase only responds To encrypted traffic so that our data remains safe.
	'			GET - Reading Data
	'
	'			Data from our Firebase database can be read by issuing an HTTP GET request To an endpoint:
	'
	'			curl 'https://samplechat.firebaseio-demo.com/users/jack/name.json'
	'
	'			A successful request will be indicated by a 200 OK HTTP status code. The response will contain the data being retrieved:
	'
	'			{ "first": "Jack", "last": "Sparrow" }
	'
	'			PUT - Writing Data
	'
	'			We can write data with a PUT request:
	'
	'			curl -X PUT -d '{ "first": "Jack", "last": "Sparrow" }' \
	'			  'https://samplechat.firebaseio-demo.com/users/jack/name.json'
	'
	'			A successful request will be indicated by a 200 OK HTTP status code. The response will contain the data written:
	'
	'			{ "first": "Jack", "last": "Sparrow" }
	'
	'			POST - Pushing Data
	'
	'			To accomplish the equivalent of the JavaScript push() method (see Lists of Data), we can issue a POST request:
	'
	'			curl -X POST -d '{"user_id" : "jack", "text" : "Ahoy!"}' \
	'			  'https://samplechat.firebaseio-demo.com/message_list.json'
	'
	'			A successful request will be indicated by a 200 OK HTTP status code. The response will contain the child name of the new data that was added:
	'
	'			{ "name": "-INOQPH-aV_psbk3ZXEX" }
	'
	'			PATCH - Updating Data
	'
	'			We can update specific children at a location without overwriting existing data using a PATCH request. Named children in the data being written with PATCH will be overwritten, but omitted children will Not be deleted. This Is equivalent To the JavaScript update() function.
	'
	'			curl -X PATCH -d '{"last":"Jones"}' \
	'			 'https://samplechat.firebaseio-demo.com/users/jack/name/.json'
	'
	'			A successful request will be indicated by a 200 OK HTTP status code. The response will contain the data written:
	'
	'			{ "last": "Jones" }
	'
	'			DELETE - Removing Data
	'
	'			We can delete data with a DELETE request:
	'
	'			curl -X DELETE \
	'			  'https://samplechat.firebaseio-demo.com/users/jack/name/last.json'
	'
	'			A successful DELETE request will be indicated by a 200 OK HTTP status code with a response containing JSON null.
	'			Method Override
	'
	'			If we are making REST calls from a browser that does not support the above methods, we can override the request method by making a POST request and setting our method via the X-HTTP-Method-Override request header:
	'
	'			curl -X POST -H "X-HTTP-Method-Override: DELETE" \
	'			  'https://samplechat.firebaseio-demo.com/users/jack/name/last.json'
	'
	'			We can also use the x-http-method-override query parameter:
	'
	'			curl -X POST \
	'			  'https://samplechat.firebaseio-demo.com/users/jack/name/last.json?x-http-method-override=DELETE'
	'
	'			Query Parameters
	'
	'			The Firebase Database REST API accepts the following query parameters And values:
	'			auth
	
	'			Supported by all request types. Authenticates this request To allow access To data protected by Firebase Realtime Database Rules. The argument can either be your Firebase app's secret or an authentication token. See the REST authentication documentation for details.
	'
	'			curl \
	'			'https://samplechat.firebaseio-demo.com/users/jack/name.json?auth=CREDENTIAL'
	
	'
	'			X-Firebase-Auth-Debug
	'
	'			If the token's debug flag is set, debug information can be found in the X-Firebase-Auth-Debug header of the response.
	'			shallow
	'
	'			This Is an advanced feature, designed To help you work with large datasets without needing To download everything. Set this To True To limit the depth of the data returned at a location. If the data at the location Is a JSON primitive (string, number Or boolean), its value will simply be returned. If the data snapshot at the location Is a JSON object, the values For Each key will be truncated To True.
	'			Arguments 	REST Methods 	Description
	'			shallow 	GET 	Limit the depth of the response.
	'
	'			curl 'https://samplechat.firebaseio-demo.com/.json?shallow=true'
	'
	'			Note that shallow cannot be mixed with any other query parameters.
	'			print
	'
	'			Formats the data returned in the response from the server.
	'			Arguments 	REST Methods 	Description
	'			pretty 	GET, PUT, POST, PATCH, DELETE 	View the data in a human-readable format.
	'			silent 	GET, PUT, POST, PATCH 	Used To suppress the output from the server when writing data. The resulting response will be empty And indicated by a 204 No Content HTTP status code.
	'
	'			curl 'https://samplechat.firebaseio-demo.com/users/jack/name.json?print=pretty'
	'
	'			curl -X PUT -d '{ "first": "Jack", "last": "Sparrow" }' \
	'			  'https://samplechat.firebaseio-demo.com/users/jack/name.json?print=silent'
	'
	'			callback
	'
	'			Supported by GET only. To make REST calls from a web browser across domains, you can use JSONP To wrap the response in a JavaScript callback function. Add callback= To have the REST API wrap the the returned data in the callback function you specify.
	'
	'			<script>
	'			  function gotData(data) {
	'			    console.log(data);
	'			  }
	'			</script>
	'			<script src="https://samplechat.firebaseio-demo.com/.json?callback=gotData"></script>
	'
	'			format
	'
	'			If set To export, the server will encode priorities in the response.
	'			Arguments 	REST Methods 	Description
	'			export 	GET 	Include priority information in the response.
	'
	'			curl 'https://samplechat.firebaseio-demo.com/.json?format=export'
	'
	'			download
	'
	'			Supported by GET only. If you would like To trigger a File download of your data from a web browser, add download=. This will cause our REST service To add the appropriate headers so that browsers know To save the data To a File.
	'
	'			curl 'https://samplechat.firebaseio-demo.com/.json?download=myfilename.txt'
	'
	'			Private Backups: We Do Not recommend using the REST API To regularly back up your Firebase database. Instead, GET set up with Firebase Private Backups.
	'			orderBy
	'
	'			See the section in the guide on ordered data For more information.
	'			limitToFirst, limitToLast, startAt, endAt, equalTo
	'
	'			See the section in the guide on querying data For more information.
	'			Streaming from the REST API
	'
	'			Firebase REST endpoints support the EventSource / Server-Sent Events protocol. To stream changes To a single location in your Firebase database, you need To Do a few things:
	'
	'			    Set the client's Accept header to "text/event-stream"
	'			    Respect HTTP Redirects, in particular HTTP status code 307
	'			    If the location requires permission To read, you must include the auth parameter
	'
	'			In Return, the server will send named events As the state of the data at the requested URL changes. The structure of these messages conforms To the EventSource protocol:
	'
	'			event: event name
	'			data: JSON encoded data payload
	'
	'			The server may send the following events:
	'			PUT
	'
	'			The JSON-encoded data will be an object with two keys: path And data. The path key points To a location relative To the request URL. The client should replace all of the data at that location in its cache with data.
	'			PATCH
	'
	'			The JSON-encoded data will be an object with two keys: path And data. The path key points To a location relative To the request URL. For each key in data, the client should replace the corresponding key in its cache with the data For that key in the message.
	'			keep-alive
	'
	'			The data For this event Is Null. No action Is required.
	'			cancel
	'
	'			The data For this event Is Null. This event will be sent If the {[firebase_database_rules}} cause a read at the requested location To no longer be allowed.
	'			auth_revoked
	'
	'			The data For this event Is a string indicating that a the credential has expired. This event will be sent when the supplied auth parameter Is no longer valid.
	'
	'			Here's an example set of events that the server may send:
	'
	'			// Set your entire cache To {"a": 1, "b": 2}
	'			event: PUT
	'			data: {"path": "/", "data": {"a": 1, "b": 2}}
	'
	'			// PUT the new data in your cache under the key 'c', so that the complete cache now looks like:
	'			// {"a": 1, "b": 2, "c": {"foo": True, "bar": False}}
	'			event: PUT
	'			data: {"path": "/c", "data": {"foo": True, "bar": False}}
	'
	'			// For Each key in the data, update (Or add) the corresponding key in your cache at path /c,
	'			// For a final cache of: {"a": 1, "b": 2, "c": {"foo": 3, "bar": False, "baz": 4}}
	'			event: PATCH
	'			data: {"path": "/c", "data": {"foo": 3, "baz": 4}}
	'
	'			Priorities
	'
	'			Priority information For a location can be referenced with a "virtual child" named .priority. Priorities can be read with GET requests And written with PUT requests. For example, the following request retrieves the priority of the users/tom node:
	'
	'			curl 'https://samplechat.firebaseio-demo.com/users/tom/.priority.json'
	'
	'			To write priority And data at the same time, you can add a .priority child To the JSON payload being written:
	'
	'			curl -X PUT -d '{"name": {"first": "Tom"}, ".priority": 1.0}' \
	'			  'https://samplechat.firebaseio-demo.com/users/tom.json'
	'
	'			To write priority And a primitive value (e.g. a string) at the same time, you can add a .priority child And PUT the primitive value in a .value child:
	'
	'			curl -X PUT -d '{".value": "Tom", ".priority": 1.0}' \
	'			  'https://samplechat.firebaseio-demo.com/users/tom/name/first.json'
	'
	'			This writes "Tom" with a priority of 1.0. Priorities can be included at any depth in the JSON payload.
	'			Server Values
	'
	'			Server values can be written at a location using a placeholder value which Is an object with a single .sv key. The value For that key Is the Type of server value you wish To set. For example, the following request sets the node's value to the Firebase server's current timestamp:
	'
	'			curl -X PUT -d '{".sv": "timestamp"}' \
	'			  'https://samplechat.firebaseio-demo.com/users/tom/startedAtTime.json'
	'
	'			Priorities may also be written using server values, using the "virtual child" path noted above.
	'
	'			Supported server values include:
	'			Server Value
	'			timestamp 	The time since UNIX epoch, in milliseconds.
	'			Retrieving And Updating Firebase Realtime Database Rules
	'
	'			The REST API can also be used To retrieve And update the Firebase Realtime Database Rules For your Firebase app. You'll need your Firebase app's secret, which you can find under the Secrets panel of your Firebase app's dashboard.
	'
	'			curl 'https://samplechat.firebaseio-demo.com/.settings/rules.json?auth=FIREBASE_SECRET'
	'			curl -X PUT -d '{ "rules": { ".read": true } }' 'https://samplechat.firebaseio-demo.com/.settings/rules.json?auth=FIREBASE_SECRET'
	'
	'			Error Conditions
	'
	'			The Firebase Database REST API will Return error codes under these circumstances.
	'			HTTP Status Codes
	'			404 Not Found 	A request made over HTTP instead of HTTPS
	'			400 Bad Request 	Unable To parse PUT Or POST data
	'			400 Bad Request 	Missing PUT Or POST data
	'			400 Bad Request 	Attempting To PUT Or POST data which Is too large
	'			417 Expectation Failed 	A REST API call that doesn't specify a Firebase name
	'			400 Bad Request 	A REST API call that contains invalid child names As part of the path
	'			403 Forbidden 	A request that violates your Firebase Realtime Database Rules

' *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** 
#End Region
