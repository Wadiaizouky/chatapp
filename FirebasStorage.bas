﻿B4A=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=10.2
@EndOfDesignText@
Sub Class_Globals
	Private bucket As String = "gs://first-project-5317d.appspot.com"
	Dim storage As FirebaseStorage
	
	Dim UploadImageNumber As Int = 0
	Dim UploadVoiceNumber As Int = 0
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	storage.Initialize("storage", bucket)
	'DownloadImageCover("/public/2835be38b5274a4b20155999a7613542.jpg")
End Sub

Public Sub DownloadImage(ImagUrl As String,ImBit As Bitmap,FilN As String)
	If Not(File.Exists(File.DirDefaultExternal,FilN)) Then
 	storage.DownloadFile(ImagUrl, File.DirDefaultExternal,FilN)
	Wait For (storage) Storage_DownloadCompleted (ServerPath As String, Success As Boolean)
	ToastMessageShow($"DownloadCompleted. Success = ${Success}"$, True)
	If Not(Success) Then Log(LastException)
	End If
	
	ImBit.Initialize(File.DirDefaultExternal, FilN)
End Sub

Public Sub DownloadProfilImage(ImagUrl As String,ImBit As Bitmap,FilN As String,IsPerson As Boolean,i As Int)
	If Not(File.Exists(File.DirDefaultExternal,FilN)) Then
 	storage.DownloadFile(ImagUrl, File.DirDefaultExternal,FilN)
		Wait For (storage) Storage_DownloadCompleted (ServerPath As String, Success As Boolean)
		If Not(Success) Then Log(LastException)
	End If
	If Not(IsPerson) Then
		ImBit.Initialize(File.DirDefaultExternal, FilN)
		CallSubDelayed(Lobby,"UserProfilePicSet")
	Else
		ImBit.Initialize(File.DirDefaultExternal, FilN)
		CallSubDelayed3(Lobby,"PersonProfilePicSet",ImBit,i)
	End If
End Sub

Public Sub DownloadProfilPersonImage(Allimages As List)
	'If Not(File.Exists(File.DirDefaultExternal,FilN)) Then
	For i = 0 To Allimages.Size-1
		Dim PhotN As String = Allimages.Get(i)
		Dim Imbit As Bitmap
		If Not(Allimages.Get(i) == "") Then
			If Not(File.Exists(File.DirDefaultExternal,PhotN.SubString(6))) Then
			storage.DownloadFile(Allimages.Get(i), File.DirDefaultExternal,PhotN.SubString(6))
		    Wait For (storage) Storage_DownloadCompleted (ServerPath As String, Success As Boolean)
			Imbit.Initialize(File.DirDefaultExternal, PhotN.SubString(6))
			CallSubDelayed3(Lobby,"PersonProfilePicSet",Imbit,i)
         	If Not(Success) Then Log(LastException)
	        End If
		Imbit.Initialize(File.DirDefaultExternal, PhotN.SubString(6))
		CallSubDelayed3(Lobby,"PersonProfilePicSet",Imbit,i)
		End If
	Next
End Sub

Public Sub DownloadProfilFriendsImage(Allimages As List)
	'If Not(File.Exists(File.DirDefaultExternal,FilN)) Then
	For i = 0 To Allimages.Size-1
		Dim PhotN As String = Allimages.Get(i)
		Dim Imbit As Bitmap
		If Not(Allimages.Get(i) == "") Then
			If Not(File.Exists(File.DirDefaultExternal,PhotN.SubString(6))) Then
			storage.DownloadFile(Allimages.Get(i), File.DirDefaultExternal,PhotN.SubString(6))
		    Wait For (storage) Storage_DownloadCompleted (ServerPath As String, Success As Boolean)
			Imbit.Initialize(File.DirDefaultExternal, PhotN.SubString(6))
				CallSubDelayed3(Lobby,"FriendProfilePicSet",Imbit,i)
         	If Not(Success) Then Log(LastException)
	        End If
		Imbit.Initialize(File.DirDefaultExternal, PhotN.SubString(6))
		CallSubDelayed3(Lobby,"FriendProfilePicSet",Imbit,i)
		End If
		Next
End Sub

Public Sub UploadProfilImage(ImagUrl As String,Dir As String,FilName As String)
	storage.UploadFile(Dir, FilName, ImagUrl)
	Wait For (storage) Storage_UploadCompleted (ServerPath As String, Success As Boolean)
	ToastMessageShow($"UploadCompleted. Success = ${Success}"$, True)
	CallSubDelayed2(Lobby,"sentimage",ImagUrl)
	If Not(Success) Then Log(LastException)
End Sub


Public Sub UploadImage(ImagUrl As String,Dir As String,FilName As String)
	storage.UploadFile(Dir, FilName, ImagUrl)
	Wait For (storage) Storage_UploadCompleted (ServerPath As String, Success As Boolean)
	ToastMessageShow($"UploadCompleted. Success = ${Success}"$, True)
	CallSubDelayed2(Lobby,"SentImageToDataBase",FilName)
	If Not(Success) Then Log(LastException)
End Sub

Public Sub UploadVoice(ImagUrl As String,Dir As String,FilName As String)
	storage.UploadFile(Dir, FilName, ImagUrl)
	Wait For (storage) Storage_UploadCompleted (ServerPath As String, Success As Boolean)
	ToastMessageShow($"UploadCompleted. Success = ${Success}"$, True)
	CallSubDelayed(Lobby,"SentvoiceToDataBase")
	If Not(Success) Then Log(LastException)
End Sub
'Sub btnDownloadPublic_Click
'	'You need to first upload a file from Firebase console.
'	Dim storage As FirebaseStorage = CreateFirebaseStorage
'	storage.DownloadFile("/public/Untitled.png", File.DirInternal, "out.txt")
'	Wait For (storage) Storage_DownloadCompleted (ServerPath As String, Success As Boolean)
'	ToastMessageShow($"DownloadCompleted. Success = ${Success}"$, True)
'	If Not(Success) Then Log(LastException)
'End Sub
'
'
'Sub btnUploadUser_Click
'	Dim storage As FirebaseStorage = CreateFirebaseStorage
'	File.WriteString(File.DirInternal, "1.txt", "Only I can access this resource.")
'	storage.UploadFile(File.DirInternal, "1.txt", $"/user/${auth.CurrentUser.Uid}/1.txt"$)
'	Wait For (storage) Storage_UploadCompleted (ServerPath As String, Success As Boolean)
'	ToastMessageShow($"UploadCompleted. Success = ${Success}"$, True)
'	If Not(Success) Then Log(LastException)
'End Sub
'
'Sub btnUploadAuth_Click
'	Dim storage As FirebaseStorage = CreateFirebaseStorage
'	File.WriteString(File.DirInternal, "1.txt", "Any authenticated user can access this resource.")
'	storage.UploadFile(File.DirInternal, "1.txt", $"/auth/1.txt"$)
'	Wait For (storage) Storage_UploadCompleted (ServerPath As String, Success As Boolean)
'	ToastMessageShow($"UploadCompleted. Success = ${Success}"$, True)
'	If Not(Success) Then Log(LastException)
'End Sub
'
''By creating a new object each time, we can use the storage as a "sender filter" for the Wait For call.
''This is a lightweight object.
'Sub CreateFirebaseStorage As FirebaseStorage
'	Dim storage As FirebaseStorage
'	storage.Initialize("storage", bucket)
'	Return storage
'End Sub
Public Sub DownloadImageCover(ImagUrl As String,M As String,isr As Boolean,ImBit As Bitmap)
	storage.DownloadFile(ImagUrl, File.DirInternal, "out"&UploadImageNumber&".jpg")
	Wait For (storage) Storage_DownloadCompleted (ServerPath As String, Success As Boolean)
	ToastMessageShow($"DownloadCompleted. Success = ${Success}"$, True)
'	Dim Bitcover As Bitmap
'	Bitcover.Initialize(File.DirInternal, "out.txt")
	ImBit.Initialize(File.DirInternal, "out"&UploadImageNumber&".jpg")
	CallSubDelayed3(Lobby,"AddImageToChat",M,isr)
	If Not(Success) Then Log(LastException)
	
	UploadImageNumber = UploadImageNumber + 1
End Sub

Public Sub DownloadVoiceCover(ImagUrl As String,M As String,isr As Boolean)
	Dim FileN As String = ImagUrl.SubString(9)
	If( Not(File.Exists(File.DirDefaultExternal,FileN))) Then
	storage.DownloadFile(ImagUrl, File.DirDefaultExternal, FileN)
	Wait For (storage) Storage_DownloadCompleted (ServerPath As String, Success As Boolean)
	ToastMessageShow($"DownloadCompleted. Success = ${Success}"$, True)
'	Dim Bitcover As Bitmap
'	Bitcover.Initialize(File.DirInternal, "out.txt")
	CallSubDelayed3(Lobby,"insializChatvoice",File.DirDefaultExternal,FileN)
	CallSubDelayed3(Lobby,"AddVoiceToChat",M,isr)
	If Not(Success) Then Log(LastException)
	
	End If
	'UploadVoiceNumber = UploadVoiceNumber + 1
End Sub
