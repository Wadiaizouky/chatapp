package b4a.example.fbrtdb;


import anywheresoftware.b4a.B4AMenuItem;
import android.app.Activity;
import android.os.Bundle;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.ObjectWrapper;
import anywheresoftware.b4a.objects.ActivityWrapper;
import java.lang.reflect.InvocationTargetException;
import anywheresoftware.b4a.B4AUncaughtException;
import anywheresoftware.b4a.debug.*;
import java.lang.ref.WeakReference;

public class main extends Activity implements B4AActivity{
	public static main mostCurrent;
	static boolean afterFirstLayout;
	static boolean isFirst = true;
    private static boolean processGlobalsRun = false;
	BALayout layout;
	public static BA processBA;
	BA activityBA;
    ActivityWrapper _activity;
    java.util.ArrayList<B4AMenuItem> menuItems;
	public static final boolean fullScreen = true;
	public static final boolean includeTitle = false;
    public static WeakReference<Activity> previousOne;
    public static boolean dontPause;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        mostCurrent = this;
		if (processBA == null) {
			processBA = new BA(this.getApplicationContext(), null, null, "b4a.example.fbrtdb", "b4a.example.fbrtdb.main");
			processBA.loadHtSubs(this.getClass());
	        float deviceScale = getApplicationContext().getResources().getDisplayMetrics().density;
	        BALayout.setDeviceScale(deviceScale);
            
		}
		else if (previousOne != null) {
			Activity p = previousOne.get();
			if (p != null && p != this) {
                BA.LogInfo("Killing previous instance (main).");
				p.finish();
			}
		}
        processBA.setActivityPaused(true);
        processBA.runHook("oncreate", this, null);
		if (!includeTitle) {
        	this.getWindow().requestFeature(android.view.Window.FEATURE_NO_TITLE);
        }
        if (fullScreen) {
        	getWindow().setFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN,   
        			android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
		
        processBA.sharedProcessBA.activityBA = null;
		layout = new BALayout(this);
		setContentView(layout);
		afterFirstLayout = false;
        WaitForLayout wl = new WaitForLayout();
        if (anywheresoftware.b4a.objects.ServiceHelper.StarterHelper.startFromActivity(this, processBA, wl, false))
		    BA.handler.postDelayed(wl, 5);

	}
	static class WaitForLayout implements Runnable {
		public void run() {
			if (afterFirstLayout)
				return;
			if (mostCurrent == null)
				return;
            
			if (mostCurrent.layout.getWidth() == 0) {
				BA.handler.postDelayed(this, 5);
				return;
			}
			mostCurrent.layout.getLayoutParams().height = mostCurrent.layout.getHeight();
			mostCurrent.layout.getLayoutParams().width = mostCurrent.layout.getWidth();
			afterFirstLayout = true;
			mostCurrent.afterFirstLayout();
		}
	}
	private void afterFirstLayout() {
        if (this != mostCurrent)
			return;
		activityBA = new BA(this, layout, processBA, "b4a.example.fbrtdb", "b4a.example.fbrtdb.main");
        
        processBA.sharedProcessBA.activityBA = new java.lang.ref.WeakReference<BA>(activityBA);
        anywheresoftware.b4a.objects.ViewWrapper.lastId = 0;
        _activity = new ActivityWrapper(activityBA, "activity");
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (BA.isShellModeRuntimeCheck(processBA)) {
			if (isFirst)
				processBA.raiseEvent2(null, true, "SHELL", false);
			processBA.raiseEvent2(null, true, "CREATE", true, "b4a.example.fbrtdb.main", processBA, activityBA, _activity, anywheresoftware.b4a.keywords.Common.Density, mostCurrent);
			_activity.reinitializeForShell(activityBA, "activity");
		}
        initializeProcessGlobals();		
        initializeGlobals();
        
        BA.LogInfo("** Activity (main) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, isFirst);
		isFirst = false;
		if (this != mostCurrent)
			return;
        processBA.setActivityPaused(false);
        BA.LogInfo("** Activity (main) Resume **");
        processBA.raiseEvent(null, "activity_resume");
        if (android.os.Build.VERSION.SDK_INT >= 11) {
			try {
				android.app.Activity.class.getMethod("invalidateOptionsMenu").invoke(this,(Object[]) null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
	public void addMenuItem(B4AMenuItem item) {
		if (menuItems == null)
			menuItems = new java.util.ArrayList<B4AMenuItem>();
		menuItems.add(item);
	}
	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		super.onCreateOptionsMenu(menu);
        try {
            if (processBA.subExists("activity_actionbarhomeclick")) {
                Class.forName("android.app.ActionBar").getMethod("setHomeButtonEnabled", boolean.class).invoke(
                    getClass().getMethod("getActionBar").invoke(this), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (processBA.runHook("oncreateoptionsmenu", this, new Object[] {menu}))
            return true;
		if (menuItems == null)
			return false;
		for (B4AMenuItem bmi : menuItems) {
			android.view.MenuItem mi = menu.add(bmi.title);
			if (bmi.drawable != null)
				mi.setIcon(bmi.drawable);
            if (android.os.Build.VERSION.SDK_INT >= 11) {
				try {
                    if (bmi.addToBar) {
				        android.view.MenuItem.class.getMethod("setShowAsAction", int.class).invoke(mi, 1);
                    }
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			mi.setOnMenuItemClickListener(new B4AMenuItemsClickListener(bmi.eventName.toLowerCase(BA.cul)));
		}
        
		return true;
	}   
 @Override
 public boolean onOptionsItemSelected(android.view.MenuItem item) {
    if (item.getItemId() == 16908332) {
        processBA.raiseEvent(null, "activity_actionbarhomeclick");
        return true;
    }
    else
        return super.onOptionsItemSelected(item); 
}
@Override
 public boolean onPrepareOptionsMenu(android.view.Menu menu) {
    super.onPrepareOptionsMenu(menu);
    processBA.runHook("onprepareoptionsmenu", this, new Object[] {menu});
    return true;
    
 }
 protected void onStart() {
    super.onStart();
    processBA.runHook("onstart", this, null);
}
 protected void onStop() {
    super.onStop();
    processBA.runHook("onstop", this, null);
}
    public void onWindowFocusChanged(boolean hasFocus) {
       super.onWindowFocusChanged(hasFocus);
       if (processBA.subExists("activity_windowfocuschanged"))
           processBA.raiseEvent2(null, true, "activity_windowfocuschanged", false, hasFocus);
    }
	private class B4AMenuItemsClickListener implements android.view.MenuItem.OnMenuItemClickListener {
		private final String eventName;
		public B4AMenuItemsClickListener(String eventName) {
			this.eventName = eventName;
		}
		public boolean onMenuItemClick(android.view.MenuItem item) {
			processBA.raiseEventFromUI(item.getTitle(), eventName + "_click");
			return true;
		}
	}
    public static Class<?> getObject() {
		return main.class;
	}
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;
	@Override
	public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {
        if (processBA.runHook("onkeydown", this, new Object[] {keyCode, event}))
            return true;
		if (onKeySubExist == null)
			onKeySubExist = processBA.subExists("activity_keypress");
		if (onKeySubExist) {
			if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK &&
					android.os.Build.VERSION.SDK_INT >= 18) {
				HandleKeyDelayed hk = new HandleKeyDelayed();
				hk.kc = keyCode;
				BA.handler.post(hk);
				return true;
			}
			else {
				boolean res = new HandleKeyDelayed().runDirectly(keyCode);
				if (res)
					return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	private class HandleKeyDelayed implements Runnable {
		int kc;
		public void run() {
			runDirectly(kc);
		}
		public boolean runDirectly(int keyCode) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keypress", false, keyCode);
			if (res == null || res == true) {
                return true;
            }
            else if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK) {
				finish();
				return true;
			}
            return false;
		}
		
	}
    @Override
	public boolean onKeyUp(int keyCode, android.view.KeyEvent event) {
        if (processBA.runHook("onkeyup", this, new Object[] {keyCode, event}))
            return true;
		if (onKeyUpSubExist == null)
			onKeyUpSubExist = processBA.subExists("activity_keyup");
		if (onKeyUpSubExist) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keyup", false, keyCode);
			if (res == null || res == true)
				return true;
		}
		return super.onKeyUp(keyCode, event);
	}
	@Override
	public void onNewIntent(android.content.Intent intent) {
        super.onNewIntent(intent);
		this.setIntent(intent);
        processBA.runHook("onnewintent", this, new Object[] {intent});
	}
    @Override 
	public void onPause() {
		super.onPause();
        if (_activity == null)
            return;
        if (this != mostCurrent)
			return;
		anywheresoftware.b4a.Msgbox.dismiss(true);
        if (!dontPause)
            BA.LogInfo("** Activity (main) Pause, UserClosed = " + activityBA.activity.isFinishing() + " **");
        else
            BA.LogInfo("** Activity (main) Pause event (activity is not paused). **");
        if (mostCurrent != null)
            processBA.raiseEvent2(_activity, true, "activity_pause", false, activityBA.activity.isFinishing());		
        if (!dontPause) {
            processBA.setActivityPaused(true);
            mostCurrent = null;
        }

        if (!activityBA.activity.isFinishing())
			previousOne = new WeakReference<Activity>(this);
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        processBA.runHook("onpause", this, null);
	}

	@Override
	public void onDestroy() {
        super.onDestroy();
		previousOne = null;
        processBA.runHook("ondestroy", this, null);
	}
    @Override 
	public void onResume() {
		super.onResume();
        mostCurrent = this;
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (activityBA != null) { //will be null during activity create (which waits for AfterLayout).
        	ResumeMessage rm = new ResumeMessage(mostCurrent);
        	BA.handler.post(rm);
        }
        processBA.runHook("onresume", this, null);
	}
    private static class ResumeMessage implements Runnable {
    	private final WeakReference<Activity> activity;
    	public ResumeMessage(Activity activity) {
    		this.activity = new WeakReference<Activity>(activity);
    	}
		public void run() {
            main mc = mostCurrent;
			if (mc == null || mc != activity.get())
				return;
			processBA.setActivityPaused(false);
            BA.LogInfo("** Activity (main) Resume **");
            if (mc != mostCurrent)
                return;
		    processBA.raiseEvent(mc._activity, "activity_resume", (Object[])null);
		}
    }
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
	      android.content.Intent data) {
		processBA.onActivityResult(requestCode, resultCode, data);
        processBA.runHook("onactivityresult", this, new Object[] {requestCode, resultCode});
	}
	private static void initializeGlobals() {
		processBA.raiseEvent2(null, true, "globals", false, (Object[])null);
	}
    public void onRequestPermissionsResult(int requestCode,
        String permissions[], int[] grantResults) {
        for (int i = 0;i < permissions.length;i++) {
            Object[] o = new Object[] {permissions[i], grantResults[i] == 0};
            processBA.raiseEventFromDifferentThread(null,null, 0, "activity_permissionresult", true, o);
        }
            
    }

public anywheresoftware.b4a.keywords.Common __c = null;
public static anywheresoftware.b4a.objects.FirebaseAuthWrapper _auth = null;
public static de.donmanfred.FirebaseDatabaseWrapper _realtime = null;
public static de.donmanfred.DatabaseReferenceWrapper _refusers = null;
public static String _userid = "";
public anywheresoftware.b4a.objects.B4XViewWrapper.XUI _xui = null;
public anywheresoftware.b4a.objects.PanelWrapper _plcover = null;
public anywheresoftware.b4a.objects.PanelWrapper _plwhite = null;
public anywheresoftware.b4a.objects.ButtonWrapper _bsingup = null;
public anywheresoftware.b4a.objects.EditTextWrapper _editusername = null;
public anywheresoftware.b4a.objects.EditTextWrapper _editpassword = null;
public anywheresoftware.b4a.objects.EditTextWrapper _editemail = null;
public anywheresoftware.b4a.objects.EditTextWrapper _editage = null;
public anywheresoftware.b4a.objects.ButtonWrapper _bsingup2 = null;
public anywheresoftware.b4a.objects.ButtonWrapper _baugoogle = null;
public anywheresoftware.b4a.objects.ButtonWrapper _bsingin = null;
public anywheresoftware.b4a.objects.collections.List _allusers = null;
public anywheresoftware.b4a.objects.EditTextWrapper _editusernamemain = null;
public anywheresoftware.b4a.objects.EditTextWrapper _editpasswordmain = null;
public b4a.example.dateutils _dateutils = null;
public b4a.example.fbrtdb.lobby _lobby = null;
public b4a.example.fbrtdb.romeservice _romeservice = null;
public b4a.example.fbrtdb.messageservice _messageservice = null;
public b4a.example.fbrtdb.useservice _useservice = null;
public b4a.example.fbrtdb.starter _starter = null;
public b4a.example.fbrtdb.httputils2service _httputils2service = null;
public b4a.example.fbrtdb.xuiviewsutils _xuiviewsutils = null;
public b4a.example.fbrtdb.b4xcollections _b4xcollections = null;

public static boolean isAnyActivityVisible() {
    boolean vis = false;
vis = vis | (main.mostCurrent != null);
vis = vis | (lobby.mostCurrent != null);
return vis;}
public static void  _activity_click() throws Exception{
ResumableSub_Activity_Click rsub = new ResumableSub_Activity_Click(null);
rsub.resume(processBA, null);
}
public static class ResumableSub_Activity_Click extends BA.ResumableSub {
public ResumableSub_Activity_Click(b4a.example.fbrtdb.main parent) {
this.parent = parent;
}
b4a.example.fbrtdb.main parent;

@Override
public void resume(BA ba, Object[] result) throws Exception{

    while (true) {
        switch (state) {
            case -1:
return;

case 0:
//C
this.state = -1;
 //BA.debugLineNum = 142;BA.debugLine="Plcover.SetVisibleAnimated(130,False)";
parent.mostCurrent._plcover.SetVisibleAnimated((int) (130),anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 143;BA.debugLine="Sleep(200)";
anywheresoftware.b4a.keywords.Common.Sleep(mostCurrent.activityBA,this,(int) (200));
this.state = 1;
return;
case 1:
//C
this.state = -1;
;
 //BA.debugLineNum = 144;BA.debugLine="Plwhite.SetLayoutAnimated(300,0,Activity.Height,A";
parent.mostCurrent._plwhite.SetLayoutAnimated((int) (300),(int) (0),parent.mostCurrent._activity.getHeight(),parent.mostCurrent._activity.getWidth(),(int) (parent.mostCurrent._activity.getHeight()*.8));
 //BA.debugLineNum = 145;BA.debugLine="End Sub";
if (true) break;

            }
        }
    }
}
public static String  _activity_create(boolean _firsttime) throws Exception{
anywheresoftware.b4a.objects.drawable.ColorDrawable _cd = null;
 //BA.debugLineNum = 76;BA.debugLine="Sub Activity_Create(FirstTime As Boolean)";
 //BA.debugLineNum = 78;BA.debugLine="Activity.LoadLayout(\"mainpage\")";
mostCurrent._activity.LoadLayout("mainpage",mostCurrent.activityBA);
 //BA.debugLineNum = 80;BA.debugLine="auth.Initialize(\"auth\")";
_auth.Initialize(processBA,"auth");
 //BA.debugLineNum = 81;BA.debugLine="AllUsers.Initialize";
mostCurrent._allusers.Initialize();
 //BA.debugLineNum = 83;BA.debugLine="Plcover.Initialize(\"Plcover\")";
mostCurrent._plcover.Initialize(mostCurrent.activityBA,"Plcover");
 //BA.debugLineNum = 84;BA.debugLine="Dim cd As ColorDrawable";
_cd = new anywheresoftware.b4a.objects.drawable.ColorDrawable();
 //BA.debugLineNum = 85;BA.debugLine="cd.Initialize(Colors.ARGB(150,0,0,0),0)";
_cd.Initialize(anywheresoftware.b4a.keywords.Common.Colors.ARGB((int) (150),(int) (0),(int) (0),(int) (0)),(int) (0));
 //BA.debugLineNum = 86;BA.debugLine="Plcover.Background = cd";
mostCurrent._plcover.setBackground((android.graphics.drawable.Drawable)(_cd.getObject()));
 //BA.debugLineNum = 87;BA.debugLine="Activity.AddView(Plcover,0,0,Activity.Width,Activ";
mostCurrent._activity.AddView((android.view.View)(mostCurrent._plcover.getObject()),(int) (0),(int) (0),mostCurrent._activity.getWidth(),mostCurrent._activity.getHeight());
 //BA.debugLineNum = 88;BA.debugLine="Plcover.SetVisibleAnimated(0,False)";
mostCurrent._plcover.SetVisibleAnimated((int) (0),anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 89;BA.debugLine="Plcover.SetElevationAnimated(0,2dip)";
mostCurrent._plcover.SetElevationAnimated((int) (0),(float) (anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (2))));
 //BA.debugLineNum = 91;BA.debugLine="Plwhite.Initialize(\"Plwhite\")";
mostCurrent._plwhite.Initialize(mostCurrent.activityBA,"Plwhite");
 //BA.debugLineNum = 92;BA.debugLine="cd.Initialize(Colors.White,30dip)";
_cd.Initialize(anywheresoftware.b4a.keywords.Common.Colors.White,anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (30)));
 //BA.debugLineNum = 93;BA.debugLine="Plwhite.Background = cd";
mostCurrent._plwhite.setBackground((android.graphics.drawable.Drawable)(_cd.getObject()));
 //BA.debugLineNum = 94;BA.debugLine="Activity.AddView(Plwhite,0,Activity.Height,Activi";
mostCurrent._activity.AddView((android.view.View)(mostCurrent._plwhite.getObject()),(int) (0),mostCurrent._activity.getHeight(),mostCurrent._activity.getWidth(),(int) (mostCurrent._activity.getHeight()*.5));
 //BA.debugLineNum = 95;BA.debugLine="Plwhite.LoadLayout(\"account\")";
mostCurrent._plwhite.LoadLayout("account",mostCurrent.activityBA);
 //BA.debugLineNum = 96;BA.debugLine="Plwhite.SetElevationAnimated(0,4dip)";
mostCurrent._plwhite.SetElevationAnimated((int) (0),(float) (anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (4))));
 //BA.debugLineNum = 98;BA.debugLine="SetBackgroundTintList(EditUserName, Colors.Red, 0";
_setbackgroundtintlist((anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._editusername.getObject())),anywheresoftware.b4a.keywords.Common.Colors.Red,(int) (0xff0020ff));
 //BA.debugLineNum = 99;BA.debugLine="SetBackgroundTintList(EditPassword, Colors.Red, 0";
_setbackgroundtintlist((anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._editpassword.getObject())),anywheresoftware.b4a.keywords.Common.Colors.Red,(int) (0xff0020ff));
 //BA.debugLineNum = 100;BA.debugLine="SetBackgroundTintList(EditEmail, Colors.Red, 0xFF";
_setbackgroundtintlist((anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._editemail.getObject())),anywheresoftware.b4a.keywords.Common.Colors.Red,(int) (0xff0020ff));
 //BA.debugLineNum = 101;BA.debugLine="SetBackgroundTintList(EditAge, Colors.Red, 0xFF00";
_setbackgroundtintlist((anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(mostCurrent._editage.getObject())),anywheresoftware.b4a.keywords.Common.Colors.Red,(int) (0xff0020ff));
 //BA.debugLineNum = 111;BA.debugLine="realtime.Initialize(\"Realtime\")";
_realtime.Initialize(processBA,"Realtime");
 //BA.debugLineNum = 112;BA.debugLine="realtime.PersistenceEnabled = True";
_realtime.setPersistenceEnabled(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 113;BA.debugLine="realtime.goOnline";
_realtime.goOnline();
 //BA.debugLineNum = 117;BA.debugLine="refUsers.Initialize(\"Reference_Users\",realtime.ge";
_refusers.Initialize(processBA,"Reference_Users",(com.google.firebase.database.DatabaseReference)(_realtime.getReferencefromUrl(("https://first-project-5317d.firebaseio.com/Users")).getObject()),(Object)("User"));
 //BA.debugLineNum = 118;BA.debugLine="refUsers.addChildEventListener";
_refusers.addChildEventListener();
 //BA.debugLineNum = 124;BA.debugLine="End Sub";
return "";
}
public static String  _activity_pause(boolean _userclosed) throws Exception{
 //BA.debugLineNum = 130;BA.debugLine="Sub Activity_Pause (UserClosed As Boolean)";
 //BA.debugLineNum = 132;BA.debugLine="End Sub";
return "";
}
public static String  _activity_resume() throws Exception{
 //BA.debugLineNum = 126;BA.debugLine="Sub Activity_Resume";
 //BA.debugLineNum = 128;BA.debugLine="End Sub";
return "";
}
public static String  _baugoogle_click() throws Exception{
 //BA.debugLineNum = 185;BA.debugLine="Sub BAuGoogle_Click";
 //BA.debugLineNum = 186;BA.debugLine="auth.SignInWithGoogle";
_auth.SignInWithGoogle(processBA);
 //BA.debugLineNum = 187;BA.debugLine="End Sub";
return "";
}
public static void  _bsingin_click() throws Exception{
ResumableSub_BSingIn_Click rsub = new ResumableSub_BSingIn_Click(null);
rsub.resume(processBA, null);
}
public static class ResumableSub_BSingIn_Click extends BA.ResumableSub {
public ResumableSub_BSingIn_Click(b4a.example.fbrtdb.main parent) {
this.parent = parent;
}
b4a.example.fbrtdb.main parent;
String _username = "";
String _password = "";
boolean _errorinfo = false;
int _i = 0;
de.donmanfred.DataSnapshotWrapper _sn = null;
int step4;
int limit4;

@Override
public void resume(BA ba, Object[] result) throws Exception{

    while (true) {
        switch (state) {
            case -1:
return;

case 0:
//C
this.state = 1;
 //BA.debugLineNum = 207;BA.debugLine="Dim UserName As String = EditUserNameMain.Text";
_username = parent.mostCurrent._editusernamemain.getText();
 //BA.debugLineNum = 208;BA.debugLine="Dim Password As String = EditPasswordMain.Text";
_password = parent.mostCurrent._editpasswordmain.getText();
 //BA.debugLineNum = 209;BA.debugLine="Dim Errorinfo As Boolean = True";
_errorinfo = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 211;BA.debugLine="For i=0 To AllUsers.Size-1";
if (true) break;

case 1:
//for
this.state = 11;
step4 = 1;
limit4 = (int) (parent.mostCurrent._allusers.getSize()-1);
_i = (int) (0) ;
this.state = 12;
if (true) break;

case 12:
//C
this.state = 11;
if ((step4 > 0 && _i <= limit4) || (step4 < 0 && _i >= limit4)) this.state = 3;
if (true) break;

case 13:
//C
this.state = 12;
_i = ((int)(0 + _i + step4)) ;
if (true) break;

case 3:
//C
this.state = 4;
 //BA.debugLineNum = 212;BA.debugLine="Dim sn As DataSnapshot";
_sn = new de.donmanfred.DataSnapshotWrapper();
 //BA.debugLineNum = 213;BA.debugLine="sn.Initialize(AllUsers.Get(i))";
_sn.Initialize(processBA,(com.google.firebase.database.DataSnapshot)(parent.mostCurrent._allusers.Get(_i)));
 //BA.debugLineNum = 214;BA.debugLine="If(sn.getChild(\"UserName\").Value == UserName And";
if (true) break;

case 4:
//if
this.state = 7;
if (((_sn.getChild("UserName").getValue()).equals((Object)(_username)) && (_sn.getChild("Password").getValue()).equals((Object)(_password)))) { 
this.state = 6;
}if (true) break;

case 6:
//C
this.state = 7;
 //BA.debugLineNum = 215;BA.debugLine="UserId = UserName";
parent._userid = _username;
 //BA.debugLineNum = 216;BA.debugLine="UseService.UserName = UserName";
parent.mostCurrent._useservice._username /*String*/  = _username;
 //BA.debugLineNum = 217;BA.debugLine="UseService.Age = sn.getChild(\"Age\").Value";
parent.mostCurrent._useservice._age /*String*/  = BA.ObjectToString(_sn.getChild("Age").getValue());
 //BA.debugLineNum = 218;BA.debugLine="UseService.IsOnline = sn.getChild(\"IsOnline\").V";
parent.mostCurrent._useservice._isonline /*boolean*/  = BA.ObjectToBoolean(_sn.getChild("IsOnline").getValue());
 //BA.debugLineNum = 219;BA.debugLine="UseService.Status = sn.getChild(\"Status\").Value";
parent.mostCurrent._useservice._status /*String*/  = BA.ObjectToString(_sn.getChild("Status").getValue());
 //BA.debugLineNum = 220;BA.debugLine="UseService.ProfilPhotoUrl = sn.getChild(\"Profil";
parent.mostCurrent._useservice._profilphotourl /*String*/  = BA.ObjectToString(_sn.getChild("ProfilPhotoURL").getValue());
 //BA.debugLineNum = 221;BA.debugLine="UseService.childd = sn.Key";
parent.mostCurrent._useservice._childd /*Object*/  = (Object)(_sn.getKey());
 //BA.debugLineNum = 224;BA.debugLine="Errorinfo = False";
_errorinfo = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 225;BA.debugLine="refUsers.goOffline";
parent._refusers.goOffline();
 //BA.debugLineNum = 226;BA.debugLine="ProgressDialogShow(\"Wait ..\")";
anywheresoftware.b4a.keywords.Common.ProgressDialogShow(mostCurrent.activityBA,BA.ObjectToCharSequence("Wait .."));
 //BA.debugLineNum = 227;BA.debugLine="refUsers.Child(sn.Key).updateChildren(CreateMap";
parent._refusers.Child(_sn.getKey()).updateChildren(processBA,anywheresoftware.b4a.keywords.Common.createMap(new Object[] {(Object)("IsOnline"),(Object)(anywheresoftware.b4a.keywords.Common.True)}));
 //BA.debugLineNum = 228;BA.debugLine="UseService.IsOnline = True";
parent.mostCurrent._useservice._isonline /*boolean*/  = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 229;BA.debugLine="Sleep(1000)";
anywheresoftware.b4a.keywords.Common.Sleep(mostCurrent.activityBA,this,(int) (1000));
this.state = 14;
return;
case 14:
//C
this.state = 7;
;
 //BA.debugLineNum = 230;BA.debugLine="ProgressDialogHide";
anywheresoftware.b4a.keywords.Common.ProgressDialogHide();
 //BA.debugLineNum = 231;BA.debugLine="StartActivity(Lobby)";
anywheresoftware.b4a.keywords.Common.StartActivity(processBA,(Object)(parent.mostCurrent._lobby.getObject()));
 if (true) break;
;
 //BA.debugLineNum = 235;BA.debugLine="If(Errorinfo) Then";

case 7:
//if
this.state = 10;
if ((_errorinfo)) { 
this.state = 9;
}if (true) break;

case 9:
//C
this.state = 10;
 //BA.debugLineNum = 236;BA.debugLine="ToastMessageShow(\"Error info ...\",True)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow(BA.ObjectToCharSequence("Error info ..."),anywheresoftware.b4a.keywords.Common.True);
 if (true) break;

case 10:
//C
this.state = 13;
;
 if (true) break;
if (true) break;

case 11:
//C
this.state = -1;
;
 //BA.debugLineNum = 239;BA.debugLine="End Sub";
if (true) break;

            }
        }
    }
}
public static void  _bsingup_click() throws Exception{
ResumableSub_BSingUp_Click rsub = new ResumableSub_BSingUp_Click(null);
rsub.resume(processBA, null);
}
public static class ResumableSub_BSingUp_Click extends BA.ResumableSub {
public ResumableSub_BSingUp_Click(b4a.example.fbrtdb.main parent) {
this.parent = parent;
}
b4a.example.fbrtdb.main parent;

@Override
public void resume(BA ba, Object[] result) throws Exception{

    while (true) {
        switch (state) {
            case -1:
return;

case 0:
//C
this.state = -1;
 //BA.debugLineNum = 136;BA.debugLine="Plcover.SetVisibleAnimated(150,True)";
parent.mostCurrent._plcover.SetVisibleAnimated((int) (150),anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 137;BA.debugLine="Sleep(200)";
anywheresoftware.b4a.keywords.Common.Sleep(mostCurrent.activityBA,this,(int) (200));
this.state = 1;
return;
case 1:
//C
this.state = -1;
;
 //BA.debugLineNum = 138;BA.debugLine="Plwhite.SetLayoutAnimated(300,0,Activity.Height *";
parent.mostCurrent._plwhite.SetLayoutAnimated((int) (300),(int) (0),(int) (parent.mostCurrent._activity.getHeight()*.5),parent.mostCurrent._activity.getWidth(),(int) (parent.mostCurrent._activity.getHeight()*.8));
 //BA.debugLineNum = 139;BA.debugLine="End Sub";
if (true) break;

            }
        }
    }
}
public static String  _bsingup2_click() throws Exception{
String _username = "";
String _password = "";
String _email = "";
String _age = "";
anywheresoftware.b4a.objects.collections.Map _mapuser = null;
 //BA.debugLineNum = 159;BA.debugLine="Sub BSingUp2_Click";
 //BA.debugLineNum = 161;BA.debugLine="ProgressDialogShow(\"Loding..\")";
anywheresoftware.b4a.keywords.Common.ProgressDialogShow(mostCurrent.activityBA,BA.ObjectToCharSequence("Loding.."));
 //BA.debugLineNum = 163;BA.debugLine="Dim UserName As String = EditUserName.Text";
_username = mostCurrent._editusername.getText();
 //BA.debugLineNum = 164;BA.debugLine="Dim Password As String = EditPassword.Text";
_password = mostCurrent._editpassword.getText();
 //BA.debugLineNum = 165;BA.debugLine="Dim Email As String = EditEmail.Text";
_email = mostCurrent._editemail.getText();
 //BA.debugLineNum = 166;BA.debugLine="Dim Age As String = EditAge.Text";
_age = mostCurrent._editage.getText();
 //BA.debugLineNum = 168;BA.debugLine="Dim MapUser As Map";
_mapuser = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 169;BA.debugLine="MapUser.Initialize";
_mapuser.Initialize();
 //BA.debugLineNum = 170;BA.debugLine="MapUser.Put(\"UserName\",UserName)";
_mapuser.Put((Object)("UserName"),(Object)(_username));
 //BA.debugLineNum = 171;BA.debugLine="MapUser.Put(\"Password\",Password)";
_mapuser.Put((Object)("Password"),(Object)(_password));
 //BA.debugLineNum = 172;BA.debugLine="MapUser.Put(\"Email\",Email)";
_mapuser.Put((Object)("Email"),(Object)(_email));
 //BA.debugLineNum = 173;BA.debugLine="MapUser.Put(\"Age\",Age)";
_mapuser.Put((Object)("Age"),(Object)(_age));
 //BA.debugLineNum = 174;BA.debugLine="MapUser.Put(\"Contact\",\"\")";
_mapuser.Put((Object)("Contact"),(Object)(""));
 //BA.debugLineNum = 175;BA.debugLine="MapUser.Put(\"ProfilPhotoURL\",\"\")";
_mapuser.Put((Object)("ProfilPhotoURL"),(Object)(""));
 //BA.debugLineNum = 176;BA.debugLine="MapUser.Put(\"IsOnline\",False)";
_mapuser.Put((Object)("IsOnline"),(Object)(anywheresoftware.b4a.keywords.Common.False));
 //BA.debugLineNum = 177;BA.debugLine="MapUser.Put(\"Status\",\"\")";
_mapuser.Put((Object)("Status"),(Object)(""));
 //BA.debugLineNum = 179;BA.debugLine="refUsers.push.setValue(MapUser,\"Users\",\"\")";
_refusers.push().setValue(processBA,_mapuser,"Users","");
 //BA.debugLineNum = 181;BA.debugLine="ProgressDialogHide";
anywheresoftware.b4a.keywords.Common.ProgressDialogHide();
 //BA.debugLineNum = 182;BA.debugLine="Activity_Click";
_activity_click();
 //BA.debugLineNum = 183;BA.debugLine="End Sub";
return "";
}
public static String  _globals() throws Exception{
 //BA.debugLineNum = 32;BA.debugLine="Sub Globals";
 //BA.debugLineNum = 33;BA.debugLine="Private xui As XUI";
mostCurrent._xui = new anywheresoftware.b4a.objects.B4XViewWrapper.XUI();
 //BA.debugLineNum = 34;BA.debugLine="Dim Plcover,Plwhite As Panel";
mostCurrent._plcover = new anywheresoftware.b4a.objects.PanelWrapper();
mostCurrent._plwhite = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 62;BA.debugLine="Private BSingUp As Button";
mostCurrent._bsingup = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 63;BA.debugLine="Private EditUserName As EditText";
mostCurrent._editusername = new anywheresoftware.b4a.objects.EditTextWrapper();
 //BA.debugLineNum = 64;BA.debugLine="Private EditPassword As EditText";
mostCurrent._editpassword = new anywheresoftware.b4a.objects.EditTextWrapper();
 //BA.debugLineNum = 65;BA.debugLine="Private EditEmail As EditText";
mostCurrent._editemail = new anywheresoftware.b4a.objects.EditTextWrapper();
 //BA.debugLineNum = 66;BA.debugLine="Private EditAge As EditText";
mostCurrent._editage = new anywheresoftware.b4a.objects.EditTextWrapper();
 //BA.debugLineNum = 67;BA.debugLine="Private BSingUp2 As Button";
mostCurrent._bsingup2 = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 68;BA.debugLine="Private BAuGoogle As Button";
mostCurrent._baugoogle = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 69;BA.debugLine="Private BSingIn As Button";
mostCurrent._bsingin = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 71;BA.debugLine="Dim AllUsers As List";
mostCurrent._allusers = new anywheresoftware.b4a.objects.collections.List();
 //BA.debugLineNum = 72;BA.debugLine="Private EditUserNameMain As EditText";
mostCurrent._editusernamemain = new anywheresoftware.b4a.objects.EditTextWrapper();
 //BA.debugLineNum = 73;BA.debugLine="Private EditPasswordMain As EditText";
mostCurrent._editpasswordmain = new anywheresoftware.b4a.objects.EditTextWrapper();
 //BA.debugLineNum = 74;BA.debugLine="End Sub";
return "";
}

public static void initializeProcessGlobals() {
    
    if (main.processGlobalsRun == false) {
	    main.processGlobalsRun = true;
		try {
		        b4a.example.dateutils._process_globals();
main._process_globals();
lobby._process_globals();
romeservice._process_globals();
messageservice._process_globals();
useservice._process_globals();
starter._process_globals();
httputils2service._process_globals();
xuiviewsutils._process_globals();
b4xcollections._process_globals();
		
        } catch (Exception e) {
			throw new RuntimeException(e);
		}
    }
}public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 21;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 24;BA.debugLine="Private auth As FirebaseAuth";
_auth = new anywheresoftware.b4a.objects.FirebaseAuthWrapper();
 //BA.debugLineNum = 25;BA.debugLine="Public realtime As FirebaseDatabase";
_realtime = new de.donmanfred.FirebaseDatabaseWrapper();
 //BA.debugLineNum = 26;BA.debugLine="Private refUsers As DatabaseReference";
_refusers = new de.donmanfred.DatabaseReferenceWrapper();
 //BA.debugLineNum = 28;BA.debugLine="Public UserId As String";
_userid = "";
 //BA.debugLineNum = 30;BA.debugLine="End Sub";
return "";
}
public static String  _reference_users_onchildadded(Object _snapshot,String _child,Object _tag) throws Exception{
 //BA.debugLineNum = 202;BA.debugLine="Sub Reference_Users_onChildAdded(Snapshot As Objec";
 //BA.debugLineNum = 203;BA.debugLine="AllUsers.Add(Snapshot)";
mostCurrent._allusers.Add(_snapshot);
 //BA.debugLineNum = 204;BA.debugLine="End Sub";
return "";
}
public static String  _setbackgroundtintlist(anywheresoftware.b4a.objects.ConcreteViewWrapper _view,int _active,int _enabled) throws Exception{
int[][] _states = null;
int[] _color = null;
anywheresoftware.b4j.object.JavaObject _csl = null;
anywheresoftware.b4j.object.JavaObject _jo = null;
 //BA.debugLineNum = 147;BA.debugLine="Sub SetBackgroundTintList(View As View,Active As I";
 //BA.debugLineNum = 148;BA.debugLine="Dim States(2,1) As Int";
_states = new int[(int) (2)][];
{
int d0 = _states.length;
int d1 = (int) (1);
for (int i0 = 0;i0 < d0;i0++) {
_states[i0] = new int[d1];
}
}
;
 //BA.debugLineNum = 149;BA.debugLine="States(0,0) = 16842908     'Active";
_states[(int) (0)][(int) (0)] = (int) (16842908);
 //BA.debugLineNum = 150;BA.debugLine="States(1,0) = 16842910    'Enabled";
_states[(int) (1)][(int) (0)] = (int) (16842910);
 //BA.debugLineNum = 151;BA.debugLine="Dim Color(2) As Int = Array As Int(Active,Enabled";
_color = new int[]{_active,_enabled};
 //BA.debugLineNum = 152;BA.debugLine="Dim CSL As JavaObject";
_csl = new anywheresoftware.b4j.object.JavaObject();
 //BA.debugLineNum = 153;BA.debugLine="CSL.InitializeNewInstance(\"android.content.res.Co";
_csl.InitializeNewInstance("android.content.res.ColorStateList",new Object[]{(Object)(_states),(Object)(_color)});
 //BA.debugLineNum = 154;BA.debugLine="Dim jo As JavaObject";
_jo = new anywheresoftware.b4j.object.JavaObject();
 //BA.debugLineNum = 155;BA.debugLine="jo.InitializeStatic(\"android.support.v4.view.View";
_jo.InitializeStatic("androidx.core.view.ViewCompat");
 //BA.debugLineNum = 156;BA.debugLine="jo.RunMethod(\"setBackgroundTintList\", Array(View,";
_jo.RunMethod("setBackgroundTintList",new Object[]{(Object)(_view.getObject()),(Object)(_csl.getObject())});
 //BA.debugLineNum = 157;BA.debugLine="End Sub";
return "";
}
}
