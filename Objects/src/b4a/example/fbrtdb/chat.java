package b4a.example.fbrtdb;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.B4AClass;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.debug.*;

public class chat extends B4AClass.ImplB4AClass implements BA.SubDelegator{
    private static java.util.HashMap<String, java.lang.reflect.Method> htSubs;
    private void innerInitialize(BA _ba) throws Exception {
        if (ba == null) {
            ba = new BA(_ba, this, htSubs, "b4a.example.fbrtdb.chat");
            if (htSubs == null) {
                ba.loadHtSubs(this.getClass());
                htSubs = ba.htSubs;
            }
            
        }
        if (BA.isShellModeRuntimeCheck(ba)) 
			   this.getClass().getMethod("_class_globals", b4a.example.fbrtdb.chat.class).invoke(this, new Object[] {null});
        else
            ba.raiseEvent2(null, true, "class_globals", false);
    }

 public anywheresoftware.b4a.keywords.Common __c = null;
public anywheresoftware.b4a.objects.B4XViewWrapper.XUI _xui = null;
public b4a.example.fbrtdb.b4xfloattextfield _textfield = null;
public b4a.example3.customlistview _clv = null;
public b4a.example.fbrtdb.bbcodeview _bbcodeview1 = null;
public b4a.example.fbrtdb.bctextengine _engine = null;
public b4a.example.bitmapcreator _bc = null;
public int _arrowwidth = 0;
public int _gap = 0;
public boolean _lastuserleft = false;
public anywheresoftware.b4a.objects.B4XViewWrapper _lblsend = null;
public anywheresoftware.b4a.objects.B4XViewWrapper _pnlbottom = null;
public anywheresoftware.b4a.phone.Phone.ContentChooser _chooser = null;
public anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _chooserbitmap = null;
public String _dirvoice = "";
public String _filenamevoice = "";
public b4a.example.fbrtdb.firebasstorage _storagechat = null;
public anywheresoftware.b4a.objects.IME _ime = null;
public anywheresoftware.b4a.objects.MediaPlayerWrapper _ap = null;
public boolean _audiostart = false;
public int _audionumber = 0;
public com.rootsoft.audiorecorder.AudioRecorder _ar = null;
public anywheresoftware.b4a.objects.PanelWrapper _paneaudio = null;
public anywheresoftware.b4a.objects.PanelWrapper _paneaudioleft = null;
public anywheresoftware.b4a.objects.LabelWrapper _buttshowvoiceleft = null;
public anywheresoftware.b4a.objects.LabelWrapper _buttshowvoice = null;
public anywheresoftware.b4a.objects.PanelWrapper _panelchat = null;
public b4a.example.dateutils _dateutils = null;
public b4a.example.fbrtdb.main _main = null;
public b4a.example.fbrtdb.lobby _lobby = null;
public b4a.example.fbrtdb.romeservice _romeservice = null;
public b4a.example.fbrtdb.messageservice _messageservice = null;
public b4a.example.fbrtdb.useservice _useservice = null;
public b4a.example.fbrtdb.starter _starter = null;
public b4a.example.fbrtdb.httputils2service _httputils2service = null;
public b4a.example.fbrtdb.xuiviewsutils _xuiviewsutils = null;
public b4a.example.fbrtdb.b4xcollections _b4xcollections = null;
public String  _addiaudio(String _dir,String _filename,boolean _right) throws Exception{
anywheresoftware.b4a.objects.B4XViewWrapper _p = null;
String _user = "";
anywheresoftware.b4a.objects.B4XViewWrapper _pn = null;
anywheresoftware.b4a.objects.B4XViewWrapper _ivtext = null;
anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper _bmptext = null;
int _textwidth = 0;
int _textheight = 0;
anywheresoftware.b4a.objects.B4XViewWrapper _ivbg = null;
anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper _bmpbg = null;
 //BA.debugLineNum = 145;BA.debugLine="public  Sub AddIAudio (Dir As String,FileName As S";
 //BA.debugLineNum = 146;BA.debugLine="Dim p As B4XView = xui.CreatePanel(\"\")";
_p = new anywheresoftware.b4a.objects.B4XViewWrapper();
_p = _xui.CreatePanel(ba,"");
 //BA.debugLineNum = 147;BA.debugLine="p.Color = xui.Color_Transparent";
_p.setColor(_xui.Color_Transparent);
 //BA.debugLineNum = 148;BA.debugLine="Dim User As String";
_user = "";
 //BA.debugLineNum = 149;BA.debugLine="If Right Then User = \"User 2\" Else User = \"User 1";
if (_right) { 
_user = "User 2";}
else {
_user = "User 1";};
 //BA.debugLineNum = 150;BA.debugLine="BBCodeView1.ExternalRuns = BuildMessage(FileName,";
_bbcodeview1._externalruns /*anywheresoftware.b4a.objects.collections.List*/  = _buildmessage(_filename,_user);
 //BA.debugLineNum = 151;BA.debugLine="BBCodeView1.ParseAndDraw";
_bbcodeview1._parseanddraw /*String*/ ();
 //BA.debugLineNum = 153;BA.debugLine="Dim pn As B4XView = xui.CreatePanel(\"\")";
_pn = new anywheresoftware.b4a.objects.B4XViewWrapper();
_pn = _xui.CreatePanel(ba,"");
 //BA.debugLineNum = 154;BA.debugLine="pn.SetLayoutAnimated(0,0,0,CLV.AsView.Width,60dip";
_pn.SetLayoutAnimated((int) (0),(int) (0),(int) (0),_clv._asview().getWidth(),__c.DipToCurrent((int) (60)));
 //BA.debugLineNum = 155;BA.debugLine="If(Right) Then";
if ((_right)) { 
 //BA.debugLineNum = 156;BA.debugLine="pn.LoadLayout(\"AudioPanel\")";
_pn.LoadLayout("AudioPanel",ba);
 //BA.debugLineNum = 157;BA.debugLine="PaneAudio.Tag =Dir";
_paneaudio.setTag((Object)(_dir));
 //BA.debugLineNum = 158;BA.debugLine="ButtShowVoice.Tag = FileName";
_buttshowvoice.setTag((Object)(_filename));
 }else {
 //BA.debugLineNum = 160;BA.debugLine="pn.LoadLayout(\"AudioPanlLeft\")";
_pn.LoadLayout("AudioPanlLeft",ba);
 //BA.debugLineNum = 161;BA.debugLine="PaneAudioLeft.Tag = Dir";
_paneaudioleft.setTag((Object)(_dir));
 //BA.debugLineNum = 162;BA.debugLine="ButtShowVoiceLeft.Tag = FileName";
_buttshowvoiceleft.setTag((Object)(_filename));
 };
 //BA.debugLineNum = 165;BA.debugLine="Dim ivText As B4XView = CreateImageView";
_ivtext = new anywheresoftware.b4a.objects.B4XViewWrapper();
_ivtext = _createimageview();
 //BA.debugLineNum = 167;BA.debugLine="Dim bmpText As B4XBitmap = GetBitmap(BBCodeView1.";
_bmptext = new anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper();
_bmptext = _getbitmap((anywheresoftware.b4a.objects.ImageViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ImageViewWrapper(), (android.widget.ImageView)(_bbcodeview1._foregroundimageview /*anywheresoftware.b4a.objects.B4XViewWrapper*/ .getObject())));
 //BA.debugLineNum = 169;BA.debugLine="Dim TextWidth As Int = bmpText.Width / Engine.mSc";
_textwidth = (int) (_bmptext.getWidth()/(double)_engine._mscale /*float*/ );
 //BA.debugLineNum = 170;BA.debugLine="Dim TextHeight As Int = bmpText.Height / Engine.m";
_textheight = (int) (_bmptext.getHeight()/(double)_engine._mscale /*float*/ );
 //BA.debugLineNum = 172;BA.debugLine="bc.SetBitmapToImageView(bmpText, ivText)";
_bc._setbitmaptoimageview(_bmptext,_ivtext);
 //BA.debugLineNum = 173;BA.debugLine="Dim ivBG As B4XView = CreateImageView";
_ivbg = new anywheresoftware.b4a.objects.B4XViewWrapper();
_ivbg = _createimageview();
 //BA.debugLineNum = 175;BA.debugLine="Dim bmpBG As B4XBitmap = DrawBubble(TextWidth, Te";
_bmpbg = new anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper();
_bmpbg = _drawbubble(_textwidth,_textheight,_right);
 //BA.debugLineNum = 176;BA.debugLine="bc.SetBitmapToImageView(bmpBG, ivBG)";
_bc._setbitmaptoimageview(_bmpbg,_ivbg);
 //BA.debugLineNum = 177;BA.debugLine="p.SetLayoutAnimated(0, 0, 0, CLV.sv.ScrollViewCon";
_p.SetLayoutAnimated((int) (0),(int) (0),(int) (0),(int) (_clv._sv.getScrollViewContentWidth()-__c.DipToCurrent((int) (2))),(int) (_textheight+3*_gap));
 //BA.debugLineNum = 178;BA.debugLine="If Right Then";
if (_right) { 
 //BA.debugLineNum = 180;BA.debugLine="p.AddView(pn,0,0,CLV.AsView.Width,60dip)";
_p.AddView((android.view.View)(_pn.getObject()),(int) (0),(int) (0),_clv._asview().getWidth(),__c.DipToCurrent((int) (60)));
 }else {
 //BA.debugLineNum = 183;BA.debugLine="p.AddView(pn,0,0,CLV.AsView.Width,60dip)";
_p.AddView((android.view.View)(_pn.getObject()),(int) (0),(int) (0),_clv._asview().getWidth(),__c.DipToCurrent((int) (60)));
 };
 //BA.debugLineNum = 186;BA.debugLine="CLV.Add(p, FileName)";
_clv._add(_p,(Object)(_filename));
 //BA.debugLineNum = 187;BA.debugLine="ScrollToLastItem";
_scrolltolastitem();
 //BA.debugLineNum = 188;BA.debugLine="End Sub";
return "";
}
public String  _addimage(String _text,boolean _right,anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _imag) throws Exception{
anywheresoftware.b4a.objects.B4XViewWrapper _p = null;
String _user = "";
anywheresoftware.b4a.objects.B4XViewWrapper _ivtext = null;
anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper _bmptext = null;
int _textwidth = 0;
int _textheight = 0;
anywheresoftware.b4a.objects.B4XViewWrapper _ivbg = null;
anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper _bmpbg = null;
 //BA.debugLineNum = 112;BA.debugLine="public Sub AddImage (Text As String, Right As Bool";
 //BA.debugLineNum = 113;BA.debugLine="Dim p As B4XView = xui.CreatePanel(\"\")";
_p = new anywheresoftware.b4a.objects.B4XViewWrapper();
_p = _xui.CreatePanel(ba,"");
 //BA.debugLineNum = 114;BA.debugLine="p.Color = xui.Color_Transparent";
_p.setColor(_xui.Color_Transparent);
 //BA.debugLineNum = 115;BA.debugLine="Dim User As String";
_user = "";
 //BA.debugLineNum = 116;BA.debugLine="If Right Then User = \"User 2\" Else User = \"User 1";
if (_right) { 
_user = "User 2";}
else {
_user = "User 1";};
 //BA.debugLineNum = 117;BA.debugLine="BBCodeView1.ExternalRuns = BuildMessage(Text, Use";
_bbcodeview1._externalruns /*anywheresoftware.b4a.objects.collections.List*/  = _buildmessage(_text,_user);
 //BA.debugLineNum = 118;BA.debugLine="BBCodeView1.ParseAndDraw";
_bbcodeview1._parseanddraw /*String*/ ();
 //BA.debugLineNum = 119;BA.debugLine="Dim ivText As B4XView = CreateImageView";
_ivtext = new anywheresoftware.b4a.objects.B4XViewWrapper();
_ivtext = _createimageview();
 //BA.debugLineNum = 121;BA.debugLine="Dim bmpText As B4XBitmap = GetBitmap(BBCodeView1.";
_bmptext = new anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper();
_bmptext = _getbitmap((anywheresoftware.b4a.objects.ImageViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ImageViewWrapper(), (android.widget.ImageView)(_bbcodeview1._foregroundimageview /*anywheresoftware.b4a.objects.B4XViewWrapper*/ .getObject())));
 //BA.debugLineNum = 123;BA.debugLine="Dim TextWidth As Int = bmpText.Width / Engine.mSc";
_textwidth = (int) (_bmptext.getWidth()/(double)_engine._mscale /*float*/ );
 //BA.debugLineNum = 124;BA.debugLine="Dim TextHeight As Int = bmpText.Height / Engine.m";
_textheight = (int) (_bmptext.getHeight()/(double)_engine._mscale /*float*/ );
 //BA.debugLineNum = 126;BA.debugLine="imag.Resize(TextWidth + 100dip,TextHeight+100dip,";
_imag.Resize((float) (_textwidth+__c.DipToCurrent((int) (100))),(float) (_textheight+__c.DipToCurrent((int) (100))),__c.True);
 //BA.debugLineNum = 127;BA.debugLine="bc.SetBitmapToImageView(imag, ivText)";
_bc._setbitmaptoimageview((anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper(), (android.graphics.Bitmap)(_imag.getObject())),_ivtext);
 //BA.debugLineNum = 128;BA.debugLine="Dim ivBG As B4XView = CreateImageView";
_ivbg = new anywheresoftware.b4a.objects.B4XViewWrapper();
_ivbg = _createimageview();
 //BA.debugLineNum = 130;BA.debugLine="Dim bmpBG As B4XBitmap = DrawBubble(TextWidth, Te";
_bmpbg = new anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper();
_bmpbg = _drawbubble(_textwidth,_textheight,_right);
 //BA.debugLineNum = 131;BA.debugLine="bc.SetBitmapToImageView(bmpBG, ivBG)";
_bc._setbitmaptoimageview(_bmpbg,_ivbg);
 //BA.debugLineNum = 132;BA.debugLine="p.SetLayoutAnimated(0, 0, 0, CLV.sv.ScrollViewCon";
_p.SetLayoutAnimated((int) (0),(int) (0),(int) (0),(int) (_clv._sv.getScrollViewContentWidth()-__c.DipToCurrent((int) (2))-__c.DipToCurrent((int) (100))),(int) (_textheight+3*_gap+__c.DipToCurrent((int) (100))));
 //BA.debugLineNum = 133;BA.debugLine="If Right Then";
if (_right) { 
 //BA.debugLineNum = 134;BA.debugLine="p.AddView(ivBG, p.Width - bmpBG.Width * xui.Scal";
_p.AddView((android.view.View)(_ivbg.getObject()),(int) (_p.getWidth()-_bmpbg.getWidth()*_xui.getScale()),_gap,(int) (_bmpbg.getWidth()*_xui.getScale()+__c.DipToCurrent((int) (100))),(int) (_bmpbg.getHeight()*_xui.getScale()+__c.DipToCurrent((int) (250))));
 //BA.debugLineNum = 136;BA.debugLine="p.AddView(ivText, p.Width - Gap - ArrowWidth - T";
_p.AddView((android.view.View)(_ivtext.getObject()),(int) (_p.getWidth()-_gap-_arrowwidth-_textwidth),(int) (2*_gap),(int) (_textwidth+__c.DipToCurrent((int) (100))),(int) (_textheight+__c.DipToCurrent((int) (100))));
 }else {
 //BA.debugLineNum = 138;BA.debugLine="p.AddView(ivBG, 0, Gap, bmpBG.Width * xui.Scale+";
_p.AddView((android.view.View)(_ivbg.getObject()),(int) (0),_gap,(int) (_bmpbg.getWidth()*_xui.getScale()+__c.DipToCurrent((int) (100))),(int) (_bmpbg.getHeight()*_xui.getScale()+__c.DipToCurrent((int) (250))));
 //BA.debugLineNum = 139;BA.debugLine="p.AddView(ivText, Gap + ArrowWidth, 2 * Gap, Tex";
_p.AddView((android.view.View)(_ivtext.getObject()),(int) (_gap+_arrowwidth),(int) (2*_gap),(int) (_textwidth+__c.DipToCurrent((int) (100))),(int) (_textheight+__c.DipToCurrent((int) (100))));
 };
 //BA.debugLineNum = 141;BA.debugLine="CLV.Add(p, Null)";
_clv._add(_p,__c.Null);
 //BA.debugLineNum = 142;BA.debugLine="ScrollToLastItem";
_scrolltolastitem();
 //BA.debugLineNum = 143;BA.debugLine="End Sub";
return "";
}
public String  _additem(String _text,boolean _right,String _sendername) throws Exception{
anywheresoftware.b4a.objects.B4XViewWrapper _p = null;
String _user = "";
anywheresoftware.b4a.objects.B4XViewWrapper _ivtext = null;
anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper _bmptext = null;
int _textwidth = 0;
int _textheight = 0;
anywheresoftware.b4a.objects.B4XViewWrapper _ivbg = null;
anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper _bmpbg = null;
 //BA.debugLineNum = 81;BA.debugLine="Public Sub AddItem (Text As String, Right As Boole";
 //BA.debugLineNum = 82;BA.debugLine="Dim p As B4XView = xui.CreatePanel(\"\")";
_p = new anywheresoftware.b4a.objects.B4XViewWrapper();
_p = _xui.CreatePanel(ba,"");
 //BA.debugLineNum = 83;BA.debugLine="p.Color = xui.Color_Transparent";
_p.setColor(_xui.Color_Transparent);
 //BA.debugLineNum = 84;BA.debugLine="Dim User As String";
_user = "";
 //BA.debugLineNum = 85;BA.debugLine="User = SenderName";
_user = _sendername;
 //BA.debugLineNum = 86;BA.debugLine="BBCodeView1.ExternalRuns = BuildMessage(Text, Use";
_bbcodeview1._externalruns /*anywheresoftware.b4a.objects.collections.List*/  = _buildmessage(_text,_user);
 //BA.debugLineNum = 87;BA.debugLine="BBCodeView1.ParseAndDraw";
_bbcodeview1._parseanddraw /*String*/ ();
 //BA.debugLineNum = 88;BA.debugLine="Dim ivText As B4XView = CreateImageView";
_ivtext = new anywheresoftware.b4a.objects.B4XViewWrapper();
_ivtext = _createimageview();
 //BA.debugLineNum = 90;BA.debugLine="Dim bmpText As B4XBitmap = GetBitmap(BBCodeView1.";
_bmptext = new anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper();
_bmptext = _getbitmap((anywheresoftware.b4a.objects.ImageViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ImageViewWrapper(), (android.widget.ImageView)(_bbcodeview1._foregroundimageview /*anywheresoftware.b4a.objects.B4XViewWrapper*/ .getObject())));
 //BA.debugLineNum = 92;BA.debugLine="Dim TextWidth As Int = bmpText.Width / Engine.mSc";
_textwidth = (int) (_bmptext.getWidth()/(double)_engine._mscale /*float*/ );
 //BA.debugLineNum = 93;BA.debugLine="Dim TextHeight As Int = bmpText.Height / Engine.m";
_textheight = (int) (_bmptext.getHeight()/(double)_engine._mscale /*float*/ );
 //BA.debugLineNum = 95;BA.debugLine="bc.SetBitmapToImageView(bmpText, ivText)";
_bc._setbitmaptoimageview(_bmptext,_ivtext);
 //BA.debugLineNum = 96;BA.debugLine="Dim ivBG As B4XView = CreateImageView";
_ivbg = new anywheresoftware.b4a.objects.B4XViewWrapper();
_ivbg = _createimageview();
 //BA.debugLineNum = 98;BA.debugLine="Dim bmpBG As B4XBitmap = DrawBubble(TextWidth, Te";
_bmpbg = new anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper();
_bmpbg = _drawbubble(_textwidth,_textheight,_right);
 //BA.debugLineNum = 99;BA.debugLine="bc.SetBitmapToImageView(bmpBG, ivBG)";
_bc._setbitmaptoimageview(_bmpbg,_ivbg);
 //BA.debugLineNum = 100;BA.debugLine="p.SetLayoutAnimated(0, 0, 0, CLV.sv.ScrollViewCon";
_p.SetLayoutAnimated((int) (0),(int) (0),(int) (0),(int) (_clv._sv.getScrollViewContentWidth()-__c.DipToCurrent((int) (2))),(int) (_textheight+3*_gap));
 //BA.debugLineNum = 101;BA.debugLine="If Right Then";
if (_right) { 
 //BA.debugLineNum = 102;BA.debugLine="p.AddView(ivBG, p.Width - bmpBG.Width * xui.Scal";
_p.AddView((android.view.View)(_ivbg.getObject()),(int) (_p.getWidth()-_bmpbg.getWidth()*_xui.getScale()),_gap,(int) (_bmpbg.getWidth()*_xui.getScale()),(int) (_bmpbg.getHeight()*_xui.getScale()));
 //BA.debugLineNum = 103;BA.debugLine="p.AddView(ivText, p.Width - Gap - ArrowWidth - T";
_p.AddView((android.view.View)(_ivtext.getObject()),(int) (_p.getWidth()-_gap-_arrowwidth-_textwidth),(int) (2*_gap),_textwidth,_textheight);
 }else {
 //BA.debugLineNum = 105;BA.debugLine="p.AddView(ivBG, 0, Gap, bmpBG.Width * xui.Scale,";
_p.AddView((android.view.View)(_ivbg.getObject()),(int) (0),_gap,(int) (_bmpbg.getWidth()*_xui.getScale()),(int) (_bmpbg.getHeight()*_xui.getScale()));
 //BA.debugLineNum = 106;BA.debugLine="p.AddView(ivText, Gap + ArrowWidth, 2 * Gap, Tex";
_p.AddView((android.view.View)(_ivtext.getObject()),(int) (_gap+_arrowwidth),(int) (2*_gap),_textwidth,_textheight);
 };
 //BA.debugLineNum = 108;BA.debugLine="CLV.Add(p, Null)";
_clv._add(_p,__c.Null);
 //BA.debugLineNum = 109;BA.debugLine="ScrollToLastItem";
_scrolltolastitem();
 //BA.debugLineNum = 110;BA.debugLine="End Sub";
return "";
}
public anywheresoftware.b4a.objects.collections.List  _buildmessage(String _text,String _user) throws Exception{
b4a.example.fbrtdb.bctextengine._bctextrun _title = null;
b4a.example.fbrtdb.bctextengine._bctextrun _textrun = null;
b4a.example.fbrtdb.bctextengine._bctextrun _time = null;
 //BA.debugLineNum = 235;BA.debugLine="Private Sub BuildMessage (Text As String, User As";
 //BA.debugLineNum = 236;BA.debugLine="Dim title As BCTextRun = Engine.CreateRun(User &";
_title = _engine._createrun /*b4a.example.fbrtdb.bctextengine._bctextrun*/ (_user+__c.CRLF);
 //BA.debugLineNum = 237;BA.debugLine="title.TextFont  = BBCodeView1.ParseData.DefaultBo";
_title.TextFont /*anywheresoftware.b4a.objects.B4XViewWrapper.B4XFont*/  = _bbcodeview1._parsedata /*b4a.example.fbrtdb.bbcodeparser._bbcodeparsedata*/ .DefaultBoldFont /*anywheresoftware.b4a.objects.B4XViewWrapper.B4XFont*/ ;
 //BA.debugLineNum = 238;BA.debugLine="Dim TextRun As BCTextRun = Engine.CreateRun(Text";
_textrun = _engine._createrun /*b4a.example.fbrtdb.bctextengine._bctextrun*/ (_text+__c.CRLF);
 //BA.debugLineNum = 239;BA.debugLine="Dim time As BCTextRun = Engine.CreateRun(DateTime";
_time = _engine._createrun /*b4a.example.fbrtdb.bctextengine._bctextrun*/ (__c.DateTime.Time(__c.DateTime.getNow()));
 //BA.debugLineNum = 240;BA.debugLine="time.TextFont = xui.CreateDefaultFont(10)";
_time.TextFont /*anywheresoftware.b4a.objects.B4XViewWrapper.B4XFont*/  = _xui.CreateDefaultFont((float) (10));
 //BA.debugLineNum = 241;BA.debugLine="time.TextColor = xui.Color_Red";
_time.TextColor /*int*/  = _xui.Color_Red;
 //BA.debugLineNum = 242;BA.debugLine="Return Array(title, TextRun, time)";
if (true) return anywheresoftware.b4a.keywords.Common.ArrayToList(new Object[]{(Object)(_title),(Object)(_textrun),(Object)(_time)});
 //BA.debugLineNum = 243;BA.debugLine="End Sub";
return null;
}
public String  _buttshowvoice_click() throws Exception{
anywheresoftware.b4a.objects.LabelWrapper _b = null;
anywheresoftware.b4a.objects.B4XViewWrapper _pn = null;
 //BA.debugLineNum = 307;BA.debugLine="Sub ButtShowVoice_Click";
 //BA.debugLineNum = 308;BA.debugLine="Dim b As Label = Sender";
_b = new anywheresoftware.b4a.objects.LabelWrapper();
_b = (anywheresoftware.b4a.objects.LabelWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.LabelWrapper(), (android.widget.TextView)(__c.Sender(ba)));
 //BA.debugLineNum = 309;BA.debugLine="Dim pn As B4XView = b.Parent";
_pn = new anywheresoftware.b4a.objects.B4XViewWrapper();
_pn = (anywheresoftware.b4a.objects.B4XViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.B4XViewWrapper(), (java.lang.Object)(_b.getParent()));
 //BA.debugLineNum = 310;BA.debugLine="AP.Load(pn.Tag,b.Tag)";
_ap.Load(BA.ObjectToString(_pn.getTag()),BA.ObjectToString(_b.getTag()));
 //BA.debugLineNum = 311;BA.debugLine="AP.Play";
_ap.Play();
 //BA.debugLineNum = 313;BA.debugLine="End Sub";
return "";
}
public String  _buttshowvoiceleft_click() throws Exception{
anywheresoftware.b4a.objects.LabelWrapper _b = null;
anywheresoftware.b4a.objects.B4XViewWrapper _pn = null;
 //BA.debugLineNum = 315;BA.debugLine="Sub ButtShowVoiceLeft_Click";
 //BA.debugLineNum = 316;BA.debugLine="Dim b As Label = Sender";
_b = new anywheresoftware.b4a.objects.LabelWrapper();
_b = (anywheresoftware.b4a.objects.LabelWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.LabelWrapper(), (android.widget.TextView)(__c.Sender(ba)));
 //BA.debugLineNum = 317;BA.debugLine="Dim pn As B4XView = b.Parent";
_pn = new anywheresoftware.b4a.objects.B4XViewWrapper();
_pn = (anywheresoftware.b4a.objects.B4XViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.B4XViewWrapper(), (java.lang.Object)(_b.getParent()));
 //BA.debugLineNum = 318;BA.debugLine="Log(b.Tag)";
__c.LogImpl("05177347",BA.ObjectToString(_b.getTag()),0);
 //BA.debugLineNum = 319;BA.debugLine="AP.Load(File.DirDefaultExternal,b.Tag)";
_ap.Load(__c.File.getDirDefaultExternal(),BA.ObjectToString(_b.getTag()));
 //BA.debugLineNum = 320;BA.debugLine="AP.Play";
_ap.Play();
 //BA.debugLineNum = 321;BA.debugLine="End Sub";
return "";
}
public String  _chooser_result(boolean _success,String _dir,String _filename) throws Exception{
 //BA.debugLineNum = 295;BA.debugLine="Sub chooser_Result (Success As Boolean, Dir As Str";
 //BA.debugLineNum = 296;BA.debugLine="If Success Then";
if (_success) { 
 //BA.debugLineNum = 297;BA.debugLine="chooserBitmap.Initialize(Dir, FileName)";
_chooserbitmap.Initialize(_dir,_filename);
 //BA.debugLineNum = 298;BA.debugLine="Storagechat.UploadImage(\"/message/\"&FileName,Dir";
_storagechat._uploadimage /*void*/ ("/message/"+_filename,_dir,_filename);
 }else {
 //BA.debugLineNum = 301;BA.debugLine="ToastMessageShow(\"No image selected\", True)";
__c.ToastMessageShow(BA.ObjectToCharSequence("No image selected"),__c.True);
 };
 //BA.debugLineNum = 305;BA.debugLine="End Sub";
return "";
}
public String  _class_globals() throws Exception{
 //BA.debugLineNum = 2;BA.debugLine="Sub Class_Globals";
 //BA.debugLineNum = 3;BA.debugLine="Private xui As XUI";
_xui = new anywheresoftware.b4a.objects.B4XViewWrapper.XUI();
 //BA.debugLineNum = 4;BA.debugLine="Private TextField As B4XFloatTextField";
_textfield = new b4a.example.fbrtdb.b4xfloattextfield();
 //BA.debugLineNum = 5;BA.debugLine="Private CLV As CustomListView";
_clv = new b4a.example3.customlistview();
 //BA.debugLineNum = 6;BA.debugLine="Private BBCodeView1 As BBCodeView";
_bbcodeview1 = new b4a.example.fbrtdb.bbcodeview();
 //BA.debugLineNum = 7;BA.debugLine="Private Engine As BCTextEngine";
_engine = new b4a.example.fbrtdb.bctextengine();
 //BA.debugLineNum = 8;BA.debugLine="Private bc As BitmapCreator";
_bc = new b4a.example.bitmapcreator();
 //BA.debugLineNum = 9;BA.debugLine="Private ArrowWidth As Int = 10dip";
_arrowwidth = __c.DipToCurrent((int) (10));
 //BA.debugLineNum = 10;BA.debugLine="Private Gap As Int = 6dip";
_gap = __c.DipToCurrent((int) (6));
 //BA.debugLineNum = 11;BA.debugLine="Private LastUserLeft As Boolean = True";
_lastuserleft = __c.True;
 //BA.debugLineNum = 12;BA.debugLine="Private lblSend As B4XView";
_lblsend = new anywheresoftware.b4a.objects.B4XViewWrapper();
 //BA.debugLineNum = 13;BA.debugLine="Private pnlBottom As B4XView";
_pnlbottom = new anywheresoftware.b4a.objects.B4XViewWrapper();
 //BA.debugLineNum = 15;BA.debugLine="Dim chooser As ContentChooser";
_chooser = new anywheresoftware.b4a.phone.Phone.ContentChooser();
 //BA.debugLineNum = 16;BA.debugLine="Dim chooserBitmap As Bitmap";
_chooserbitmap = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper();
 //BA.debugLineNum = 17;BA.debugLine="Dim Dirvoice As String";
_dirvoice = "";
 //BA.debugLineNum = 18;BA.debugLine="Dim FileNameVoice As String";
_filenamevoice = "";
 //BA.debugLineNum = 19;BA.debugLine="Public Storagechat As FirebasStorage";
_storagechat = new b4a.example.fbrtdb.firebasstorage();
 //BA.debugLineNum = 20;BA.debugLine="Private ime As IME";
_ime = new anywheresoftware.b4a.objects.IME();
 //BA.debugLineNum = 22;BA.debugLine="Dim AP As MediaPlayer";
_ap = new anywheresoftware.b4a.objects.MediaPlayerWrapper();
 //BA.debugLineNum = 23;BA.debugLine="Dim AudioStart As Boolean = False";
_audiostart = __c.False;
 //BA.debugLineNum = 24;BA.debugLine="Dim AudioNumber As Int";
_audionumber = 0;
 //BA.debugLineNum = 25;BA.debugLine="Dim AR As AudioRecorder";
_ar = new com.rootsoft.audiorecorder.AudioRecorder();
 //BA.debugLineNum = 27;BA.debugLine="Private PaneAudio As Panel";
_paneaudio = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 28;BA.debugLine="Private PaneAudioLeft As Panel";
_paneaudioleft = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 29;BA.debugLine="Private ButtShowVoiceLeft As Label";
_buttshowvoiceleft = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 30;BA.debugLine="Private ButtShowVoice As Label";
_buttshowvoice = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 32;BA.debugLine="Private Panelchat As Panel";
_panelchat = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 33;BA.debugLine="End Sub";
return "";
}
public String  _clearmessage() throws Exception{
 //BA.debugLineNum = 282;BA.debugLine="Public Sub ClearMessage";
 //BA.debugLineNum = 283;BA.debugLine="CLV.Clear";
_clv._clear();
 //BA.debugLineNum = 284;BA.debugLine="End Sub";
return "";
}
public String  _clv_itemclick(int _index,Object _value) throws Exception{
 //BA.debugLineNum = 253;BA.debugLine="Private Sub CLV_ItemClick (Index As Int, Value As";
 //BA.debugLineNum = 258;BA.debugLine="End Sub";
return "";
}
public anywheresoftware.b4a.objects.B4XViewWrapper  _createimageview() throws Exception{
anywheresoftware.b4a.objects.ImageViewWrapper _iv = null;
 //BA.debugLineNum = 260;BA.debugLine="Private Sub CreateImageView As B4XView";
 //BA.debugLineNum = 261;BA.debugLine="Dim iv As ImageView";
_iv = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 262;BA.debugLine="iv.Initialize(\"\")";
_iv.Initialize(ba,"");
 //BA.debugLineNum = 263;BA.debugLine="Return iv";
if (true) return (anywheresoftware.b4a.objects.B4XViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.B4XViewWrapper(), (java.lang.Object)(_iv.getObject()));
 //BA.debugLineNum = 264;BA.debugLine="End Sub";
return null;
}
public anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper  _drawbubble(int _width,int _height,boolean _right) throws Exception{
int _scaledgap = 0;
int _scaledarrowwidth = 0;
int _nw = 0;
int _nh = 0;
anywheresoftware.b4a.objects.B4XCanvas.B4XRect _r = null;
b4a.example.bcpath _path = null;
int _clr = 0;
anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper _b = null;
 //BA.debugLineNum = 199;BA.debugLine="Private Sub DrawBubble (Width As Int, Height As In";
 //BA.debugLineNum = 201;BA.debugLine="Width = Ceil(Width / xui.Scale)";
_width = (int) (__c.Ceil(_width/(double)_xui.getScale()));
 //BA.debugLineNum = 202;BA.debugLine="Height = Ceil(Height / xui.Scale)";
_height = (int) (__c.Ceil(_height/(double)_xui.getScale()));
 //BA.debugLineNum = 203;BA.debugLine="Dim ScaledGap As Int = Ceil(Gap / xui.Scale)";
_scaledgap = (int) (__c.Ceil(_gap/(double)_xui.getScale()));
 //BA.debugLineNum = 204;BA.debugLine="Dim ScaledArrowWidth As Int = Ceil(ArrowWidth / x";
_scaledarrowwidth = (int) (__c.Ceil(_arrowwidth/(double)_xui.getScale()));
 //BA.debugLineNum = 205;BA.debugLine="Dim nw As Int = Width + 2 * ScaledGap + ScaledArr";
_nw = (int) (_width+2*_scaledgap+_scaledarrowwidth);
 //BA.debugLineNum = 206;BA.debugLine="Dim nh As Int = Height + 2 * ScaledGap";
_nh = (int) (_height+2*_scaledgap);
 //BA.debugLineNum = 207;BA.debugLine="If bc.mWidth < nw Or bc.mHeight < nh Then";
if (_bc._mwidth<_nw || _bc._mheight<_nh) { 
 //BA.debugLineNum = 208;BA.debugLine="bc.Initialize(Max(bc.mWidth, nw), Max(bc.mHeight";
_bc._initialize(ba,(int) (__c.Max(_bc._mwidth,_nw)),(int) (__c.Max(_bc._mheight,_nh)));
 };
 //BA.debugLineNum = 210;BA.debugLine="bc.DrawRect(bc.TargetRect, xui.Color_Transparent,";
_bc._drawrect(_bc._targetrect,_xui.Color_Transparent,__c.True,(int) (0));
 //BA.debugLineNum = 211;BA.debugLine="Dim r As B4XRect";
_r = new anywheresoftware.b4a.objects.B4XCanvas.B4XRect();
 //BA.debugLineNum = 212;BA.debugLine="Dim path As BCPath";
_path = new b4a.example.bcpath();
 //BA.debugLineNum = 213;BA.debugLine="Dim clr As Int";
_clr = 0;
 //BA.debugLineNum = 214;BA.debugLine="If Right Then clr = 0xFFEFEFEF Else clr = 0xFFC1F";
if (_right) { 
_clr = (int) (0xffefefef);}
else {
_clr = (int) (0xffc1f7a3);};
 //BA.debugLineNum = 215;BA.debugLine="If Right Then";
if (_right) { 
 //BA.debugLineNum = 216;BA.debugLine="r.Initialize(0, 0, nw - ScaledArrowWidth, nh)";
_r.Initialize((float) (0),(float) (0),(float) (_nw-_scaledarrowwidth),(float) (_nh));
 //BA.debugLineNum = 217;BA.debugLine="path.Initialize(nw - 1, 1)";
_path._initialize(ba,(float) (_nw-1),(float) (1));
 //BA.debugLineNum = 218;BA.debugLine="path.LineTo(nw - 1 - (10 + ScaledArrowWidth), 1)";
_path._lineto((float) (_nw-1-(10+_scaledarrowwidth)),(float) (1));
 //BA.debugLineNum = 219;BA.debugLine="path.LineTo(nw - 1 - ScaledArrowWidth, 10)";
_path._lineto((float) (_nw-1-_scaledarrowwidth),(float) (10));
 //BA.debugLineNum = 220;BA.debugLine="path.LineTo(nw - 1, 1)";
_path._lineto((float) (_nw-1),(float) (1));
 }else {
 //BA.debugLineNum = 222;BA.debugLine="r.Initialize(ScaledArrowWidth, 1, nw, nh)";
_r.Initialize((float) (_scaledarrowwidth),(float) (1),(float) (_nw),(float) (_nh));
 //BA.debugLineNum = 223;BA.debugLine="path.Initialize(1, 1)";
_path._initialize(ba,(float) (1),(float) (1));
 //BA.debugLineNum = 224;BA.debugLine="path.LineTo((10 + ScaledArrowWidth), 1)";
_path._lineto((float) ((10+_scaledarrowwidth)),(float) (1));
 //BA.debugLineNum = 225;BA.debugLine="path.LineTo(ScaledArrowWidth, 10)";
_path._lineto((float) (_scaledarrowwidth),(float) (10));
 //BA.debugLineNum = 226;BA.debugLine="path.LineTo(1, 1)";
_path._lineto((float) (1),(float) (1));
 };
 //BA.debugLineNum = 228;BA.debugLine="bc.DrawRectRounded(r, clr, True, 0, 10)";
_bc._drawrectrounded(_r,_clr,__c.True,(int) (0),(int) (10));
 //BA.debugLineNum = 229;BA.debugLine="bc.DrawPath(path, clr, True, 0)";
_bc._drawpath(_path,_clr,__c.True,(int) (0));
 //BA.debugLineNum = 230;BA.debugLine="bc.DrawPath(path, clr, False, 2)";
_bc._drawpath(_path,_clr,__c.False,(int) (2));
 //BA.debugLineNum = 231;BA.debugLine="Dim b As B4XBitmap = bc.Bitmap";
_b = new anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper();
_b = _bc._getbitmap();
 //BA.debugLineNum = 232;BA.debugLine="Return b.Crop(0, 1, nw, nh)";
if (true) return _b.Crop((int) (0),(int) (1),_nw,_nh);
 //BA.debugLineNum = 233;BA.debugLine="End Sub";
return null;
}
public anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper  _getbitmap(anywheresoftware.b4a.objects.ImageViewWrapper _iv) throws Exception{
 //BA.debugLineNum = 245;BA.debugLine="Private Sub GetBitmap (iv As ImageView) As B4XBitm";
 //BA.debugLineNum = 249;BA.debugLine="Return iv.Bitmap";
if (true) return (anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper(), (android.graphics.Bitmap)(_iv.getBitmap()));
 //BA.debugLineNum = 251;BA.debugLine="End Sub";
return null;
}
public String  _heightchanged(int _newheight) throws Exception{
anywheresoftware.b4a.objects.B4XViewWrapper _c = null;
 //BA.debugLineNum = 72;BA.debugLine="Public Sub HeightChanged (NewHeight As Int)";
 //BA.debugLineNum = 73;BA.debugLine="Dim c As B4XView = CLV.AsView";
_c = new anywheresoftware.b4a.objects.B4XViewWrapper();
_c = _clv._asview();
 //BA.debugLineNum = 74;BA.debugLine="c.Height = NewHeight - pnlBottom.Height";
_c.setHeight((int) (_newheight-_pnlbottom.getHeight()));
 //BA.debugLineNum = 75;BA.debugLine="CLV.Base_Resize(c.Width, c.Height)";
_clv._base_resize(_c.getWidth(),_c.getHeight());
 //BA.debugLineNum = 76;BA.debugLine="pnlBottom.Top = NewHeight - pnlBottom.Height";
_pnlbottom.setTop((int) (_newheight-_pnlbottom.getHeight()));
 //BA.debugLineNum = 77;BA.debugLine="Panelchat.Top = NewHeight - Panelchat.Height";
_panelchat.setTop((int) (_newheight-_panelchat.getHeight()));
 //BA.debugLineNum = 78;BA.debugLine="ScrollToLastItem";
_scrolltolastitem();
 //BA.debugLineNum = 79;BA.debugLine="End Sub";
return "";
}
public String  _ime_heightchanged(int _newheight,int _oldheight) throws Exception{
 //BA.debugLineNum = 358;BA.debugLine="Sub ime_HeightChanged (NewHeight As Int, OldHeight";
 //BA.debugLineNum = 359;BA.debugLine="HeightChanged(NewHeight)";
_heightchanged(_newheight);
 //BA.debugLineNum = 360;BA.debugLine="End Sub";
return "";
}
public String  _initialize(anywheresoftware.b4a.BA _ba,anywheresoftware.b4a.objects.B4XViewWrapper _parent) throws Exception{
innerInitialize(_ba);
 //BA.debugLineNum = 35;BA.debugLine="Public Sub Initialize (Parent As B4XView)";
 //BA.debugLineNum = 36;BA.debugLine="Parent.LoadLayout(\"Chat\")";
_parent.LoadLayout("Chat",ba);
 //BA.debugLineNum = 37;BA.debugLine="Engine.Initialize(Parent)";
_engine._initialize /*String*/ (ba,_parent);
 //BA.debugLineNum = 39;BA.debugLine="bc.Initialize(300, 300)";
_bc._initialize(ba,(int) (300),(int) (300));
 //BA.debugLineNum = 40;BA.debugLine="TextField.NextField = TextField";
_textfield._setnextfield /*b4a.example.fbrtdb.b4xfloattextfield*/ (_textfield);
 //BA.debugLineNum = 42;BA.debugLine="Storagechat.Initialize";
_storagechat._initialize /*String*/ (ba);
 //BA.debugLineNum = 43;BA.debugLine="chooser.Initialize(\"chooser\")";
_chooser.Initialize("chooser");
 //BA.debugLineNum = 45;BA.debugLine="AR.Initialize()";
_ar.Initialize(ba);
 //BA.debugLineNum = 47;BA.debugLine="AudioNumber = 0";
_audionumber = (int) (0);
 //BA.debugLineNum = 48;BA.debugLine="AP.Initialize2(\"MediaPlayer\")";
_ap.Initialize2(ba,"MediaPlayer");
 //BA.debugLineNum = 50;BA.debugLine="ime.Initialize(\"ime\")";
_ime.Initialize("ime");
 //BA.debugLineNum = 51;BA.debugLine="End Sub";
return "";
}
public String  _labchatclose_click() throws Exception{
 //BA.debugLineNum = 287;BA.debugLine="Sub LabChatClose_Click";
 //BA.debugLineNum = 288;BA.debugLine="CallSubDelayed(Lobby,\"CloseChat\")";
__c.CallSubDelayed(ba,(Object)(_lobby.getObject()),"CloseChat");
 //BA.debugLineNum = 289;BA.debugLine="End Sub";
return "";
}
public String  _labeaudio_click() throws Exception{
anywheresoftware.b4a.objects.LabelWrapper _lab = null;
 //BA.debugLineNum = 325;BA.debugLine="Sub LabeAudio_Click";
 //BA.debugLineNum = 326;BA.debugLine="Dim lab As Label = Sender";
_lab = new anywheresoftware.b4a.objects.LabelWrapper();
_lab = (anywheresoftware.b4a.objects.LabelWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.LabelWrapper(), (android.widget.TextView)(__c.Sender(ba)));
 //BA.debugLineNum = 328;BA.debugLine="If Not(AudioStart) Then";
if (__c.Not(_audiostart)) { 
 //BA.debugLineNum = 329;BA.debugLine="AR.AudioSource = AR.AS_MIC";
_ar.setAudioSource(_ar.AS_MIC);
 //BA.debugLineNum = 330;BA.debugLine="AR.OutputFormat = AR.OF_THREE_GPP";
_ar.setOutputFormat(_ar.OF_THREE_GPP);
 //BA.debugLineNum = 331;BA.debugLine="AR.AudioEncoder = AR.AE_AMR_NB";
_ar.setAudioEncoder(_ar.AE_AMR_NB);
 //BA.debugLineNum = 332;BA.debugLine="AudioNumber = Rnd(0,1000)";
_audionumber = __c.Rnd((int) (0),(int) (1000));
 //BA.debugLineNum = 333;BA.debugLine="AR.setOutputFile(File.DirDefaultExternal,\"myReco";
_ar.setOutputFile(__c.File.getDirDefaultExternal(),"myRecording"+BA.NumberToString(_audionumber)+".wav");
 //BA.debugLineNum = 334;BA.debugLine="AR.prepare()";
_ar.prepare();
 //BA.debugLineNum = 335;BA.debugLine="AudioStart = True";
_audiostart = __c.True;
 //BA.debugLineNum = 336;BA.debugLine="AR.start";
_ar.start();
 //BA.debugLineNum = 338;BA.debugLine="lab.TextColor = Colors.Red";
_lab.setTextColor(__c.Colors.Red);
 }else {
 //BA.debugLineNum = 340;BA.debugLine="AudioStart = False";
_audiostart = __c.False;
 //BA.debugLineNum = 341;BA.debugLine="AR.stop";
_ar.stop();
 //BA.debugLineNum = 343;BA.debugLine="lab.TextColor = Colors.Black";
_lab.setTextColor(__c.Colors.Black);
 //BA.debugLineNum = 346;BA.debugLine="Storagechat.UploadVoice(\"/message/\"&\"myRecording";
_storagechat._uploadvoice /*void*/ ("/message/"+"myRecording"+BA.NumberToString(_audionumber)+".wav",__c.File.getDirDefaultExternal(),"myRecording"+BA.NumberToString(_audionumber)+".wav");
 };
 //BA.debugLineNum = 351;BA.debugLine="End Sub";
return "";
}
public String  _labephoto_click() throws Exception{
 //BA.debugLineNum = 291;BA.debugLine="Sub LabePhoto_Click";
 //BA.debugLineNum = 292;BA.debugLine="chooser.Show(\"image/*\", \"Choose image\")";
_chooser.Show(ba,"image/*","Choose image");
 //BA.debugLineNum = 293;BA.debugLine="End Sub";
return "";
}
public String  _lblsend_click() throws Exception{
anywheresoftware.b4a.objects.EditTextWrapper _et = null;
 //BA.debugLineNum = 53;BA.debugLine="Private Sub lblSend_Click";
 //BA.debugLineNum = 54;BA.debugLine="If TextField.Text.Length > 0 Then";
if (_textfield._gettext /*String*/ ().length()>0) { 
 //BA.debugLineNum = 55;BA.debugLine="Sent(TextField.Text)";
_sent(_textfield._gettext /*String*/ ());
 };
 //BA.debugLineNum = 57;BA.debugLine="TextField.RequestFocusAndShowKeyboard";
_textfield._requestfocusandshowkeyboard /*String*/ ();
 //BA.debugLineNum = 62;BA.debugLine="Dim et As EditText = TextField.TextField";
_et = new anywheresoftware.b4a.objects.EditTextWrapper();
_et = (anywheresoftware.b4a.objects.EditTextWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.EditTextWrapper(), (android.widget.EditText)(_textfield._gettextfield /*anywheresoftware.b4a.objects.B4XViewWrapper*/ ().getObject()));
 //BA.debugLineNum = 63;BA.debugLine="et.SelectAll";
_et.SelectAll();
 //BA.debugLineNum = 69;BA.debugLine="End Sub";
return "";
}
public void  _scrolltolastitem() throws Exception{
ResumableSub_ScrollToLastItem rsub = new ResumableSub_ScrollToLastItem(this);
rsub.resume(ba, null);
}
public static class ResumableSub_ScrollToLastItem extends BA.ResumableSub {
public ResumableSub_ScrollToLastItem(b4a.example.fbrtdb.chat parent) {
this.parent = parent;
}
b4a.example.fbrtdb.chat parent;

@Override
public void resume(BA ba, Object[] result) throws Exception{

    while (true) {
        switch (state) {
            case -1:
return;

case 0:
//C
this.state = 1;
 //BA.debugLineNum = 191;BA.debugLine="Sleep(50)";
parent.__c.Sleep(ba,this,(int) (50));
this.state = 9;
return;
case 9:
//C
this.state = 1;
;
 //BA.debugLineNum = 192;BA.debugLine="If CLV.Size > 0 Then";
if (true) break;

case 1:
//if
this.state = 8;
if (parent._clv._getsize()>0) { 
this.state = 3;
}if (true) break;

case 3:
//C
this.state = 4;
 //BA.debugLineNum = 193;BA.debugLine="If CLV.sv.ScrollViewContentHeight > CLV.sv.Heigh";
if (true) break;

case 4:
//if
this.state = 7;
if (parent._clv._sv.getScrollViewContentHeight()>parent._clv._sv.getHeight()) { 
this.state = 6;
}if (true) break;

case 6:
//C
this.state = 7;
 //BA.debugLineNum = 194;BA.debugLine="CLV.ScrollToItem(CLV.Size - 1)";
parent._clv._scrolltoitem((int) (parent._clv._getsize()-1));
 if (true) break;

case 7:
//C
this.state = 8;
;
 if (true) break;

case 8:
//C
this.state = -1;
;
 //BA.debugLineNum = 197;BA.debugLine="End Sub";
if (true) break;

            }
        }
    }
}
public String  _sent(String _messag) throws Exception{
 //BA.debugLineNum = 273;BA.debugLine="Sub Sent(Messag As String)";
 //BA.debugLineNum = 275;BA.debugLine="MessageService.refMessage.push.setValue(Create";
_messageservice._refmessage /*de.donmanfred.DatabaseReferenceWrapper*/ .push().setValue(ba,__c.createMap(new Object[] {(Object)("Sender"),(Object)(_useservice._username /*String*/ ),(Object)("Message"),(Object)(_messag)}),"Messages","");
 //BA.debugLineNum = 277;BA.debugLine="End Sub";
return "";
}
public String  _setbackchatimag(anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _image) throws Exception{
 //BA.debugLineNum = 353;BA.debugLine="Public Sub SetBackChatImag(image As Bitmap)";
 //BA.debugLineNum = 354;BA.debugLine="image.Resize(CLV.AsView.Width,CLV.AsView.Height,T";
_image.Resize((float) (_clv._asview().getWidth()),(float) (_clv._asview().getHeight()),__c.True);
 //BA.debugLineNum = 355;BA.debugLine="CLV.AsView.SetBitmap(image)";
_clv._asview().SetBitmap((android.graphics.Bitmap)(_image.getObject()));
 //BA.debugLineNum = 356;BA.debugLine="End Sub";
return "";
}
public Object callSub(String sub, Object sender, Object[] args) throws Exception {
BA.senderHolder.set(sender);
return BA.SubDelegator.SubNotFound;
}
}
