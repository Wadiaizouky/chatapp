package b4a.example.fbrtdb;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.B4AClass;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.debug.*;

public class firebasstorage extends B4AClass.ImplB4AClass implements BA.SubDelegator{
    private static java.util.HashMap<String, java.lang.reflect.Method> htSubs;
    private void innerInitialize(BA _ba) throws Exception {
        if (ba == null) {
            ba = new BA(_ba, this, htSubs, "b4a.example.fbrtdb.firebasstorage");
            if (htSubs == null) {
                ba.loadHtSubs(this.getClass());
                htSubs = ba.htSubs;
            }
            
        }
        if (BA.isShellModeRuntimeCheck(ba)) 
			   this.getClass().getMethod("_class_globals", b4a.example.fbrtdb.firebasstorage.class).invoke(this, new Object[] {null});
        else
            ba.raiseEvent2(null, true, "class_globals", false);
    }

 public anywheresoftware.b4a.keywords.Common __c = null;
public String _bucket = "";
public anywheresoftware.b4x.objects.FirebaseStorageWrapper _storage = null;
public int _uploadimagenumber = 0;
public int _uploadvoicenumber = 0;
public b4a.example.dateutils _dateutils = null;
public b4a.example.fbrtdb.main _main = null;
public b4a.example.fbrtdb.lobby _lobby = null;
public b4a.example.fbrtdb.romeservice _romeservice = null;
public b4a.example.fbrtdb.messageservice _messageservice = null;
public b4a.example.fbrtdb.useservice _useservice = null;
public b4a.example.fbrtdb.starter _starter = null;
public b4a.example.fbrtdb.httputils2service _httputils2service = null;
public b4a.example.fbrtdb.xuiviewsutils _xuiviewsutils = null;
public b4a.example.fbrtdb.b4xcollections _b4xcollections = null;
public String  _class_globals() throws Exception{
 //BA.debugLineNum = 1;BA.debugLine="Sub Class_Globals";
 //BA.debugLineNum = 2;BA.debugLine="Private bucket As String = \"gs://first-project-53";
_bucket = "gs://first-project-5317d.appspot.com";
 //BA.debugLineNum = 3;BA.debugLine="Dim storage As FirebaseStorage";
_storage = new anywheresoftware.b4x.objects.FirebaseStorageWrapper();
 //BA.debugLineNum = 5;BA.debugLine="Dim UploadImageNumber As Int = 0";
_uploadimagenumber = (int) (0);
 //BA.debugLineNum = 6;BA.debugLine="Dim UploadVoiceNumber As Int = 0";
_uploadvoicenumber = (int) (0);
 //BA.debugLineNum = 7;BA.debugLine="End Sub";
return "";
}
public void  _downloadimage(String _imagurl,anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _imbit,String _filn) throws Exception{
ResumableSub_DownloadImage rsub = new ResumableSub_DownloadImage(this,_imagurl,_imbit,_filn);
rsub.resume(ba, null);
}
public static class ResumableSub_DownloadImage extends BA.ResumableSub {
public ResumableSub_DownloadImage(b4a.example.fbrtdb.firebasstorage parent,String _imagurl,anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _imbit,String _filn) {
this.parent = parent;
this._imagurl = _imagurl;
this._imbit = _imbit;
this._filn = _filn;
}
b4a.example.fbrtdb.firebasstorage parent;
String _imagurl;
anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _imbit;
String _filn;
String _serverpath = "";
boolean _success = false;

@Override
public void resume(BA ba, Object[] result) throws Exception{

    while (true) {
        switch (state) {
            case -1:
return;

case 0:
//C
this.state = 1;
 //BA.debugLineNum = 16;BA.debugLine="If Not(File.Exists(File.DirDefaultExternal,FilN))";
if (true) break;

case 1:
//if
this.state = 10;
if (parent.__c.Not(parent.__c.File.Exists(parent.__c.File.getDirDefaultExternal(),_filn))) { 
this.state = 3;
}if (true) break;

case 3:
//C
this.state = 4;
 //BA.debugLineNum = 17;BA.debugLine="storage.DownloadFile(ImagUrl, File.DirDefaultExt";
parent._storage.DownloadFile(ba,_imagurl,parent.__c.File.getDirDefaultExternal(),_filn);
 //BA.debugLineNum = 18;BA.debugLine="Wait For (storage) Storage_DownloadCompleted (Ser";
parent.__c.WaitFor("storage_downloadcompleted", ba, this, (Object)(parent._storage));
this.state = 11;
return;
case 11:
//C
this.state = 4;
_serverpath = (String) result[0];
_success = (Boolean) result[1];
;
 //BA.debugLineNum = 19;BA.debugLine="ToastMessageShow($\"DownloadCompleted. Success = $";
parent.__c.ToastMessageShow(BA.ObjectToCharSequence(("DownloadCompleted. Success = "+parent.__c.SmartStringFormatter("",(Object)(_success))+"")),parent.__c.True);
 //BA.debugLineNum = 20;BA.debugLine="If Not(Success) Then Log(LastException)";
if (true) break;

case 4:
//if
this.state = 9;
if (parent.__c.Not(_success)) { 
this.state = 6;
;}if (true) break;

case 6:
//C
this.state = 9;
parent.__c.LogImpl("07340037",BA.ObjectToString(parent.__c.LastException(parent.getActivityBA())),0);
if (true) break;

case 9:
//C
this.state = 10;
;
 if (true) break;

case 10:
//C
this.state = -1;
;
 //BA.debugLineNum = 23;BA.debugLine="ImBit.Initialize(File.DirDefaultExternal, FilN)";
_imbit.Initialize(parent.__c.File.getDirDefaultExternal(),_filn);
 //BA.debugLineNum = 24;BA.debugLine="End Sub";
if (true) break;

            }
        }
    }
}
public void  _storage_downloadcompleted(String _serverpath,boolean _success) throws Exception{
}
public void  _downloadimagecover(String _imagurl,String _m,boolean _isr,anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _imbit) throws Exception{
ResumableSub_DownloadImageCover rsub = new ResumableSub_DownloadImageCover(this,_imagurl,_m,_isr,_imbit);
rsub.resume(ba, null);
}
public static class ResumableSub_DownloadImageCover extends BA.ResumableSub {
public ResumableSub_DownloadImageCover(b4a.example.fbrtdb.firebasstorage parent,String _imagurl,String _m,boolean _isr,anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _imbit) {
this.parent = parent;
this._imagurl = _imagurl;
this._m = _m;
this._isr = _isr;
this._imbit = _imbit;
}
b4a.example.fbrtdb.firebasstorage parent;
String _imagurl;
String _m;
boolean _isr;
anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _imbit;
String _serverpath = "";
boolean _success = false;

@Override
public void resume(BA ba, Object[] result) throws Exception{

    while (true) {
        switch (state) {
            case -1:
return;

case 0:
//C
this.state = 1;
 //BA.debugLineNum = 139;BA.debugLine="storage.DownloadFile(ImagUrl, File.DirInternal, \"";
parent._storage.DownloadFile(ba,_imagurl,parent.__c.File.getDirInternal(),"out"+BA.NumberToString(parent._uploadimagenumber)+".jpg");
 //BA.debugLineNum = 140;BA.debugLine="Wait For (storage) Storage_DownloadCompleted (Ser";
parent.__c.WaitFor("storage_downloadcompleted", ba, this, (Object)(parent._storage));
this.state = 7;
return;
case 7:
//C
this.state = 1;
_serverpath = (String) result[0];
_success = (Boolean) result[1];
;
 //BA.debugLineNum = 141;BA.debugLine="ToastMessageShow($\"DownloadCompleted. Success = $";
parent.__c.ToastMessageShow(BA.ObjectToCharSequence(("DownloadCompleted. Success = "+parent.__c.SmartStringFormatter("",(Object)(_success))+"")),parent.__c.True);
 //BA.debugLineNum = 144;BA.debugLine="ImBit.Initialize(File.DirInternal, \"out\"&UploadIm";
_imbit.Initialize(parent.__c.File.getDirInternal(),"out"+BA.NumberToString(parent._uploadimagenumber)+".jpg");
 //BA.debugLineNum = 145;BA.debugLine="CallSubDelayed3(Lobby,\"AddImageToChat\",M,isr)";
parent.__c.CallSubDelayed3(ba,(Object)(parent._lobby.getObject()),"AddImageToChat",(Object)(_m),(Object)(_isr));
 //BA.debugLineNum = 146;BA.debugLine="If Not(Success) Then Log(LastException)";
if (true) break;

case 1:
//if
this.state = 6;
if (parent.__c.Not(_success)) { 
this.state = 3;
;}if (true) break;

case 3:
//C
this.state = 6;
parent.__c.LogImpl("07798792",BA.ObjectToString(parent.__c.LastException(parent.getActivityBA())),0);
if (true) break;

case 6:
//C
this.state = -1;
;
 //BA.debugLineNum = 148;BA.debugLine="UploadImageNumber = UploadImageNumber + 1";
parent._uploadimagenumber = (int) (parent._uploadimagenumber+1);
 //BA.debugLineNum = 149;BA.debugLine="End Sub";
if (true) break;

            }
        }
    }
}
public void  _downloadprofilfriendsimage(anywheresoftware.b4a.objects.collections.List _allimages) throws Exception{
ResumableSub_DownloadProfilFriendsImage rsub = new ResumableSub_DownloadProfilFriendsImage(this,_allimages);
rsub.resume(ba, null);
}
public static class ResumableSub_DownloadProfilFriendsImage extends BA.ResumableSub {
public ResumableSub_DownloadProfilFriendsImage(b4a.example.fbrtdb.firebasstorage parent,anywheresoftware.b4a.objects.collections.List _allimages) {
this.parent = parent;
this._allimages = _allimages;
}
b4a.example.fbrtdb.firebasstorage parent;
anywheresoftware.b4a.objects.collections.List _allimages;
int _i = 0;
String _photn = "";
anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _imbit = null;
String _serverpath = "";
boolean _success = false;
int step1;
int limit1;

@Override
public void resume(BA ba, Object[] result) throws Exception{

    while (true) {
        switch (state) {
            case -1:
return;

case 0:
//C
this.state = 1;
 //BA.debugLineNum = 62;BA.debugLine="For i = 0 To Allimages.Size-1";
if (true) break;

case 1:
//for
this.state = 18;
step1 = 1;
limit1 = (int) (_allimages.getSize()-1);
_i = (int) (0) ;
this.state = 19;
if (true) break;

case 19:
//C
this.state = 18;
if ((step1 > 0 && _i <= limit1) || (step1 < 0 && _i >= limit1)) this.state = 3;
if (true) break;

case 20:
//C
this.state = 19;
_i = ((int)(0 + _i + step1)) ;
if (true) break;

case 3:
//C
this.state = 4;
 //BA.debugLineNum = 63;BA.debugLine="Dim PhotN As String = Allimages.Get(i)";
_photn = BA.ObjectToString(_allimages.Get(_i));
 //BA.debugLineNum = 64;BA.debugLine="Dim Imbit As Bitmap";
_imbit = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper();
 //BA.debugLineNum = 65;BA.debugLine="If Not(Allimages.Get(i) == \"\") Then";
if (true) break;

case 4:
//if
this.state = 17;
if (parent.__c.Not((_allimages.Get(_i)).equals((Object)("")))) { 
this.state = 6;
}if (true) break;

case 6:
//C
this.state = 7;
 //BA.debugLineNum = 66;BA.debugLine="If Not(File.Exists(File.DirDefaultExternal,Phot";
if (true) break;

case 7:
//if
this.state = 16;
if (parent.__c.Not(parent.__c.File.Exists(parent.__c.File.getDirDefaultExternal(),_photn.substring((int) (6))))) { 
this.state = 9;
}if (true) break;

case 9:
//C
this.state = 10;
 //BA.debugLineNum = 67;BA.debugLine="storage.DownloadFile(Allimages.Get(i), File.Dir";
parent._storage.DownloadFile(ba,BA.ObjectToString(_allimages.Get(_i)),parent.__c.File.getDirDefaultExternal(),_photn.substring((int) (6)));
 //BA.debugLineNum = 68;BA.debugLine="Wait For (storage) Storage_DownloadCompleted";
parent.__c.WaitFor("storage_downloadcompleted", ba, this, (Object)(parent._storage));
this.state = 21;
return;
case 21:
//C
this.state = 10;
_serverpath = (String) result[0];
_success = (Boolean) result[1];
;
 //BA.debugLineNum = 69;BA.debugLine="Imbit.Initialize(File.DirDefaultExternal, PhotN";
_imbit.Initialize(parent.__c.File.getDirDefaultExternal(),_photn.substring((int) (6)));
 //BA.debugLineNum = 70;BA.debugLine="CallSubDelayed3(Lobby,\"FriendProfilePicSet\",Im";
parent.__c.CallSubDelayed3(ba,(Object)(parent._lobby.getObject()),"FriendProfilePicSet",(Object)(_imbit),(Object)(_i));
 //BA.debugLineNum = 71;BA.debugLine="If Not(Success) Then Log(LastException)";
if (true) break;

case 10:
//if
this.state = 15;
if (parent.__c.Not(_success)) { 
this.state = 12;
;}if (true) break;

case 12:
//C
this.state = 15;
parent.__c.LogImpl("07536651",BA.ObjectToString(parent.__c.LastException(parent.getActivityBA())),0);
if (true) break;

case 15:
//C
this.state = 16;
;
 if (true) break;

case 16:
//C
this.state = 17;
;
 //BA.debugLineNum = 73;BA.debugLine="Imbit.Initialize(File.DirDefaultExternal, PhotN.";
_imbit.Initialize(parent.__c.File.getDirDefaultExternal(),_photn.substring((int) (6)));
 //BA.debugLineNum = 74;BA.debugLine="CallSubDelayed3(Lobby,\"FriendProfilePicSet\",Imbi";
parent.__c.CallSubDelayed3(ba,(Object)(parent._lobby.getObject()),"FriendProfilePicSet",(Object)(_imbit),(Object)(_i));
 if (true) break;

case 17:
//C
this.state = 20;
;
 if (true) break;
if (true) break;

case 18:
//C
this.state = -1;
;
 //BA.debugLineNum = 77;BA.debugLine="End Sub";
if (true) break;

            }
        }
    }
}
public void  _downloadprofilimage(String _imagurl,anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _imbit,String _filn,boolean _isperson,int _i) throws Exception{
ResumableSub_DownloadProfilImage rsub = new ResumableSub_DownloadProfilImage(this,_imagurl,_imbit,_filn,_isperson,_i);
rsub.resume(ba, null);
}
public static class ResumableSub_DownloadProfilImage extends BA.ResumableSub {
public ResumableSub_DownloadProfilImage(b4a.example.fbrtdb.firebasstorage parent,String _imagurl,anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _imbit,String _filn,boolean _isperson,int _i) {
this.parent = parent;
this._imagurl = _imagurl;
this._imbit = _imbit;
this._filn = _filn;
this._isperson = _isperson;
this._i = _i;
}
b4a.example.fbrtdb.firebasstorage parent;
String _imagurl;
anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _imbit;
String _filn;
boolean _isperson;
int _i;
String _serverpath = "";
boolean _success = false;

@Override
public void resume(BA ba, Object[] result) throws Exception{

    while (true) {
        switch (state) {
            case -1:
return;

case 0:
//C
this.state = 1;
 //BA.debugLineNum = 27;BA.debugLine="If Not(File.Exists(File.DirDefaultExternal,FilN))";
if (true) break;

case 1:
//if
this.state = 10;
if (parent.__c.Not(parent.__c.File.Exists(parent.__c.File.getDirDefaultExternal(),_filn))) { 
this.state = 3;
}if (true) break;

case 3:
//C
this.state = 4;
 //BA.debugLineNum = 28;BA.debugLine="storage.DownloadFile(ImagUrl, File.DirDefaultExt";
parent._storage.DownloadFile(ba,_imagurl,parent.__c.File.getDirDefaultExternal(),_filn);
 //BA.debugLineNum = 29;BA.debugLine="Wait For (storage) Storage_DownloadCompleted (Se";
parent.__c.WaitFor("storage_downloadcompleted", ba, this, (Object)(parent._storage));
this.state = 16;
return;
case 16:
//C
this.state = 4;
_serverpath = (String) result[0];
_success = (Boolean) result[1];
;
 //BA.debugLineNum = 30;BA.debugLine="If Not(Success) Then Log(LastException)";
if (true) break;

case 4:
//if
this.state = 9;
if (parent.__c.Not(_success)) { 
this.state = 6;
;}if (true) break;

case 6:
//C
this.state = 9;
parent.__c.LogImpl("07405572",BA.ObjectToString(parent.__c.LastException(parent.getActivityBA())),0);
if (true) break;

case 9:
//C
this.state = 10;
;
 if (true) break;
;
 //BA.debugLineNum = 32;BA.debugLine="If Not(IsPerson) Then";

case 10:
//if
this.state = 15;
if (parent.__c.Not(_isperson)) { 
this.state = 12;
}else {
this.state = 14;
}if (true) break;

case 12:
//C
this.state = 15;
 //BA.debugLineNum = 33;BA.debugLine="ImBit.Initialize(File.DirDefaultExternal, FilN)";
_imbit.Initialize(parent.__c.File.getDirDefaultExternal(),_filn);
 //BA.debugLineNum = 34;BA.debugLine="CallSubDelayed(Lobby,\"UserProfilePicSet\")";
parent.__c.CallSubDelayed(ba,(Object)(parent._lobby.getObject()),"UserProfilePicSet");
 if (true) break;

case 14:
//C
this.state = 15;
 //BA.debugLineNum = 36;BA.debugLine="ImBit.Initialize(File.DirDefaultExternal, FilN)";
_imbit.Initialize(parent.__c.File.getDirDefaultExternal(),_filn);
 //BA.debugLineNum = 37;BA.debugLine="CallSubDelayed3(Lobby,\"PersonProfilePicSet\",ImBi";
parent.__c.CallSubDelayed3(ba,(Object)(parent._lobby.getObject()),"PersonProfilePicSet",(Object)(_imbit),(Object)(_i));
 if (true) break;

case 15:
//C
this.state = -1;
;
 //BA.debugLineNum = 39;BA.debugLine="End Sub";
if (true) break;

            }
        }
    }
}
public void  _downloadprofilpersonimage(anywheresoftware.b4a.objects.collections.List _allimages) throws Exception{
ResumableSub_DownloadProfilPersonImage rsub = new ResumableSub_DownloadProfilPersonImage(this,_allimages);
rsub.resume(ba, null);
}
public static class ResumableSub_DownloadProfilPersonImage extends BA.ResumableSub {
public ResumableSub_DownloadProfilPersonImage(b4a.example.fbrtdb.firebasstorage parent,anywheresoftware.b4a.objects.collections.List _allimages) {
this.parent = parent;
this._allimages = _allimages;
}
b4a.example.fbrtdb.firebasstorage parent;
anywheresoftware.b4a.objects.collections.List _allimages;
int _i = 0;
String _photn = "";
anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _imbit = null;
String _serverpath = "";
boolean _success = false;
int step1;
int limit1;

@Override
public void resume(BA ba, Object[] result) throws Exception{

    while (true) {
        switch (state) {
            case -1:
return;

case 0:
//C
this.state = 1;
 //BA.debugLineNum = 43;BA.debugLine="For i = 0 To Allimages.Size-1";
if (true) break;

case 1:
//for
this.state = 18;
step1 = 1;
limit1 = (int) (_allimages.getSize()-1);
_i = (int) (0) ;
this.state = 19;
if (true) break;

case 19:
//C
this.state = 18;
if ((step1 > 0 && _i <= limit1) || (step1 < 0 && _i >= limit1)) this.state = 3;
if (true) break;

case 20:
//C
this.state = 19;
_i = ((int)(0 + _i + step1)) ;
if (true) break;

case 3:
//C
this.state = 4;
 //BA.debugLineNum = 44;BA.debugLine="Dim PhotN As String = Allimages.Get(i)";
_photn = BA.ObjectToString(_allimages.Get(_i));
 //BA.debugLineNum = 45;BA.debugLine="Dim Imbit As Bitmap";
_imbit = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper();
 //BA.debugLineNum = 46;BA.debugLine="If Not(Allimages.Get(i) == \"\") Then";
if (true) break;

case 4:
//if
this.state = 17;
if (parent.__c.Not((_allimages.Get(_i)).equals((Object)("")))) { 
this.state = 6;
}if (true) break;

case 6:
//C
this.state = 7;
 //BA.debugLineNum = 47;BA.debugLine="If Not(File.Exists(File.DirDefaultExternal,Phot";
if (true) break;

case 7:
//if
this.state = 16;
if (parent.__c.Not(parent.__c.File.Exists(parent.__c.File.getDirDefaultExternal(),_photn.substring((int) (6))))) { 
this.state = 9;
}if (true) break;

case 9:
//C
this.state = 10;
 //BA.debugLineNum = 48;BA.debugLine="storage.DownloadFile(Allimages.Get(i), File.Dir";
parent._storage.DownloadFile(ba,BA.ObjectToString(_allimages.Get(_i)),parent.__c.File.getDirDefaultExternal(),_photn.substring((int) (6)));
 //BA.debugLineNum = 49;BA.debugLine="Wait For (storage) Storage_DownloadCompleted";
parent.__c.WaitFor("storage_downloadcompleted", ba, this, (Object)(parent._storage));
this.state = 21;
return;
case 21:
//C
this.state = 10;
_serverpath = (String) result[0];
_success = (Boolean) result[1];
;
 //BA.debugLineNum = 50;BA.debugLine="Imbit.Initialize(File.DirDefaultExternal, PhotN";
_imbit.Initialize(parent.__c.File.getDirDefaultExternal(),_photn.substring((int) (6)));
 //BA.debugLineNum = 51;BA.debugLine="CallSubDelayed3(Lobby,\"PersonProfilePicSet\",Imb";
parent.__c.CallSubDelayed3(ba,(Object)(parent._lobby.getObject()),"PersonProfilePicSet",(Object)(_imbit),(Object)(_i));
 //BA.debugLineNum = 52;BA.debugLine="If Not(Success) Then Log(LastException)";
if (true) break;

case 10:
//if
this.state = 15;
if (parent.__c.Not(_success)) { 
this.state = 12;
;}if (true) break;

case 12:
//C
this.state = 15;
parent.__c.LogImpl("07471115",BA.ObjectToString(parent.__c.LastException(parent.getActivityBA())),0);
if (true) break;

case 15:
//C
this.state = 16;
;
 if (true) break;

case 16:
//C
this.state = 17;
;
 //BA.debugLineNum = 54;BA.debugLine="Imbit.Initialize(File.DirDefaultExternal, PhotN.";
_imbit.Initialize(parent.__c.File.getDirDefaultExternal(),_photn.substring((int) (6)));
 //BA.debugLineNum = 55;BA.debugLine="CallSubDelayed3(Lobby,\"PersonProfilePicSet\",Imbi";
parent.__c.CallSubDelayed3(ba,(Object)(parent._lobby.getObject()),"PersonProfilePicSet",(Object)(_imbit),(Object)(_i));
 if (true) break;

case 17:
//C
this.state = 20;
;
 if (true) break;
if (true) break;

case 18:
//C
this.state = -1;
;
 //BA.debugLineNum = 58;BA.debugLine="End Sub";
if (true) break;

            }
        }
    }
}
public void  _downloadvoicecover(String _imagurl,String _m,boolean _isr) throws Exception{
ResumableSub_DownloadVoiceCover rsub = new ResumableSub_DownloadVoiceCover(this,_imagurl,_m,_isr);
rsub.resume(ba, null);
}
public static class ResumableSub_DownloadVoiceCover extends BA.ResumableSub {
public ResumableSub_DownloadVoiceCover(b4a.example.fbrtdb.firebasstorage parent,String _imagurl,String _m,boolean _isr) {
this.parent = parent;
this._imagurl = _imagurl;
this._m = _m;
this._isr = _isr;
}
b4a.example.fbrtdb.firebasstorage parent;
String _imagurl;
String _m;
boolean _isr;
String _filen = "";
String _serverpath = "";
boolean _success = false;

@Override
public void resume(BA ba, Object[] result) throws Exception{

    while (true) {
        switch (state) {
            case -1:
return;

case 0:
//C
this.state = 1;
 //BA.debugLineNum = 152;BA.debugLine="Dim FileN As String = ImagUrl.SubString(9)";
_filen = _imagurl.substring((int) (9));
 //BA.debugLineNum = 153;BA.debugLine="If( Not(File.Exists(File.DirDefaultExternal,FileN";
if (true) break;

case 1:
//if
this.state = 10;
if ((parent.__c.Not(parent.__c.File.Exists(parent.__c.File.getDirDefaultExternal(),_filen)))) { 
this.state = 3;
}if (true) break;

case 3:
//C
this.state = 4;
 //BA.debugLineNum = 154;BA.debugLine="storage.DownloadFile(ImagUrl, File.DirDefaultExte";
parent._storage.DownloadFile(ba,_imagurl,parent.__c.File.getDirDefaultExternal(),_filen);
 //BA.debugLineNum = 155;BA.debugLine="Wait For (storage) Storage_DownloadCompleted (Ser";
parent.__c.WaitFor("storage_downloadcompleted", ba, this, (Object)(parent._storage));
this.state = 11;
return;
case 11:
//C
this.state = 4;
_serverpath = (String) result[0];
_success = (Boolean) result[1];
;
 //BA.debugLineNum = 156;BA.debugLine="ToastMessageShow($\"DownloadCompleted. Success = $";
parent.__c.ToastMessageShow(BA.ObjectToCharSequence(("DownloadCompleted. Success = "+parent.__c.SmartStringFormatter("",(Object)(_success))+"")),parent.__c.True);
 //BA.debugLineNum = 159;BA.debugLine="CallSubDelayed3(Lobby,\"insializChatvoice\",File.Di";
parent.__c.CallSubDelayed3(ba,(Object)(parent._lobby.getObject()),"insializChatvoice",(Object)(parent.__c.File.getDirDefaultExternal()),(Object)(_filen));
 //BA.debugLineNum = 160;BA.debugLine="CallSubDelayed3(Lobby,\"AddVoiceToChat\",M,isr)";
parent.__c.CallSubDelayed3(ba,(Object)(parent._lobby.getObject()),"AddVoiceToChat",(Object)(_m),(Object)(_isr));
 //BA.debugLineNum = 161;BA.debugLine="If Not(Success) Then Log(LastException)";
if (true) break;

case 4:
//if
this.state = 9;
if (parent.__c.Not(_success)) { 
this.state = 6;
;}if (true) break;

case 6:
//C
this.state = 9;
parent.__c.LogImpl("07864330",BA.ObjectToString(parent.__c.LastException(parent.getActivityBA())),0);
if (true) break;

case 9:
//C
this.state = 10;
;
 if (true) break;

case 10:
//C
this.state = -1;
;
 //BA.debugLineNum = 165;BA.debugLine="End Sub";
if (true) break;

            }
        }
    }
}
public String  _initialize(anywheresoftware.b4a.BA _ba) throws Exception{
innerInitialize(_ba);
 //BA.debugLineNum = 10;BA.debugLine="Public Sub Initialize";
 //BA.debugLineNum = 11;BA.debugLine="storage.Initialize(\"storage\", bucket)";
_storage.Initialize("storage",_bucket);
 //BA.debugLineNum = 13;BA.debugLine="End Sub";
return "";
}
public void  _uploadimage(String _imagurl,String _dir,String _filname) throws Exception{
ResumableSub_UploadImage rsub = new ResumableSub_UploadImage(this,_imagurl,_dir,_filname);
rsub.resume(ba, null);
}
public static class ResumableSub_UploadImage extends BA.ResumableSub {
public ResumableSub_UploadImage(b4a.example.fbrtdb.firebasstorage parent,String _imagurl,String _dir,String _filname) {
this.parent = parent;
this._imagurl = _imagurl;
this._dir = _dir;
this._filname = _filname;
}
b4a.example.fbrtdb.firebasstorage parent;
String _imagurl;
String _dir;
String _filname;
String _serverpath = "";
boolean _success = false;

@Override
public void resume(BA ba, Object[] result) throws Exception{

    while (true) {
        switch (state) {
            case -1:
return;

case 0:
//C
this.state = 1;
 //BA.debugLineNum = 89;BA.debugLine="storage.UploadFile(Dir, FilName, ImagUrl)";
parent._storage.UploadFile(ba,_dir,_filname,_imagurl);
 //BA.debugLineNum = 90;BA.debugLine="Wait For (storage) Storage_UploadCompleted (Serve";
parent.__c.WaitFor("storage_uploadcompleted", ba, this, (Object)(parent._storage));
this.state = 7;
return;
case 7:
//C
this.state = 1;
_serverpath = (String) result[0];
_success = (Boolean) result[1];
;
 //BA.debugLineNum = 91;BA.debugLine="ToastMessageShow($\"UploadCompleted. Success = ${S";
parent.__c.ToastMessageShow(BA.ObjectToCharSequence(("UploadCompleted. Success = "+parent.__c.SmartStringFormatter("",(Object)(_success))+"")),parent.__c.True);
 //BA.debugLineNum = 92;BA.debugLine="CallSubDelayed2(Lobby,\"SentImageToDataBase\",FilNa";
parent.__c.CallSubDelayed2(ba,(Object)(parent._lobby.getObject()),"SentImageToDataBase",(Object)(_filname));
 //BA.debugLineNum = 93;BA.debugLine="If Not(Success) Then Log(LastException)";
if (true) break;

case 1:
//if
this.state = 6;
if (parent.__c.Not(_success)) { 
this.state = 3;
;}if (true) break;

case 3:
//C
this.state = 6;
parent.__c.LogImpl("07667717",BA.ObjectToString(parent.__c.LastException(parent.getActivityBA())),0);
if (true) break;

case 6:
//C
this.state = -1;
;
 //BA.debugLineNum = 94;BA.debugLine="End Sub";
if (true) break;

            }
        }
    }
}
public void  _storage_uploadcompleted(String _serverpath,boolean _success) throws Exception{
}
public void  _uploadprofilimage(String _imagurl,String _dir,String _filname) throws Exception{
ResumableSub_UploadProfilImage rsub = new ResumableSub_UploadProfilImage(this,_imagurl,_dir,_filname);
rsub.resume(ba, null);
}
public static class ResumableSub_UploadProfilImage extends BA.ResumableSub {
public ResumableSub_UploadProfilImage(b4a.example.fbrtdb.firebasstorage parent,String _imagurl,String _dir,String _filname) {
this.parent = parent;
this._imagurl = _imagurl;
this._dir = _dir;
this._filname = _filname;
}
b4a.example.fbrtdb.firebasstorage parent;
String _imagurl;
String _dir;
String _filname;
String _serverpath = "";
boolean _success = false;

@Override
public void resume(BA ba, Object[] result) throws Exception{

    while (true) {
        switch (state) {
            case -1:
return;

case 0:
//C
this.state = 1;
 //BA.debugLineNum = 80;BA.debugLine="storage.UploadFile(Dir, FilName, ImagUrl)";
parent._storage.UploadFile(ba,_dir,_filname,_imagurl);
 //BA.debugLineNum = 81;BA.debugLine="Wait For (storage) Storage_UploadCompleted (Serve";
parent.__c.WaitFor("storage_uploadcompleted", ba, this, (Object)(parent._storage));
this.state = 7;
return;
case 7:
//C
this.state = 1;
_serverpath = (String) result[0];
_success = (Boolean) result[1];
;
 //BA.debugLineNum = 82;BA.debugLine="ToastMessageShow($\"UploadCompleted. Success = ${S";
parent.__c.ToastMessageShow(BA.ObjectToCharSequence(("UploadCompleted. Success = "+parent.__c.SmartStringFormatter("",(Object)(_success))+"")),parent.__c.True);
 //BA.debugLineNum = 83;BA.debugLine="CallSubDelayed2(Lobby,\"sentimage\",ImagUrl)";
parent.__c.CallSubDelayed2(ba,(Object)(parent._lobby.getObject()),"sentimage",(Object)(_imagurl));
 //BA.debugLineNum = 84;BA.debugLine="If Not(Success) Then Log(LastException)";
if (true) break;

case 1:
//if
this.state = 6;
if (parent.__c.Not(_success)) { 
this.state = 3;
;}if (true) break;

case 3:
//C
this.state = 6;
parent.__c.LogImpl("07602181",BA.ObjectToString(parent.__c.LastException(parent.getActivityBA())),0);
if (true) break;

case 6:
//C
this.state = -1;
;
 //BA.debugLineNum = 85;BA.debugLine="End Sub";
if (true) break;

            }
        }
    }
}
public void  _uploadvoice(String _imagurl,String _dir,String _filname) throws Exception{
ResumableSub_UploadVoice rsub = new ResumableSub_UploadVoice(this,_imagurl,_dir,_filname);
rsub.resume(ba, null);
}
public static class ResumableSub_UploadVoice extends BA.ResumableSub {
public ResumableSub_UploadVoice(b4a.example.fbrtdb.firebasstorage parent,String _imagurl,String _dir,String _filname) {
this.parent = parent;
this._imagurl = _imagurl;
this._dir = _dir;
this._filname = _filname;
}
b4a.example.fbrtdb.firebasstorage parent;
String _imagurl;
String _dir;
String _filname;
String _serverpath = "";
boolean _success = false;

@Override
public void resume(BA ba, Object[] result) throws Exception{

    while (true) {
        switch (state) {
            case -1:
return;

case 0:
//C
this.state = 1;
 //BA.debugLineNum = 97;BA.debugLine="storage.UploadFile(Dir, FilName, ImagUrl)";
parent._storage.UploadFile(ba,_dir,_filname,_imagurl);
 //BA.debugLineNum = 98;BA.debugLine="Wait For (storage) Storage_UploadCompleted (Serve";
parent.__c.WaitFor("storage_uploadcompleted", ba, this, (Object)(parent._storage));
this.state = 7;
return;
case 7:
//C
this.state = 1;
_serverpath = (String) result[0];
_success = (Boolean) result[1];
;
 //BA.debugLineNum = 99;BA.debugLine="ToastMessageShow($\"UploadCompleted. Success = ${S";
parent.__c.ToastMessageShow(BA.ObjectToCharSequence(("UploadCompleted. Success = "+parent.__c.SmartStringFormatter("",(Object)(_success))+"")),parent.__c.True);
 //BA.debugLineNum = 100;BA.debugLine="CallSubDelayed(Lobby,\"SentvoiceToDataBase\")";
parent.__c.CallSubDelayed(ba,(Object)(parent._lobby.getObject()),"SentvoiceToDataBase");
 //BA.debugLineNum = 101;BA.debugLine="If Not(Success) Then Log(LastException)";
if (true) break;

case 1:
//if
this.state = 6;
if (parent.__c.Not(_success)) { 
this.state = 3;
;}if (true) break;

case 3:
//C
this.state = 6;
parent.__c.LogImpl("07733253",BA.ObjectToString(parent.__c.LastException(parent.getActivityBA())),0);
if (true) break;

case 6:
//C
this.state = -1;
;
 //BA.debugLineNum = 102;BA.debugLine="End Sub";
if (true) break;

            }
        }
    }
}
public Object callSub(String sub, Object sender, Object[] args) throws Exception {
BA.senderHolder.set(sender);
return BA.SubDelegator.SubNotFound;
}
}
