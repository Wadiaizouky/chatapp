package b4a.example.fbrtdb;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.objects.ServiceHelper;
import anywheresoftware.b4a.debug.*;

public class romeservice extends  android.app.Service{
	public static class romeservice_BR extends android.content.BroadcastReceiver {

		@Override
		public void onReceive(android.content.Context context, android.content.Intent intent) {
            BA.LogInfo("** Receiver (romeservice) OnReceive **");
			android.content.Intent in = new android.content.Intent(context, romeservice.class);
			if (intent != null)
				in.putExtra("b4a_internal_intent", intent);
            ServiceHelper.StarterHelper.startServiceFromReceiver (context, in, false, BA.class);
		}

	}
    static romeservice mostCurrent;
	public static BA processBA;
    private ServiceHelper _service;
    public static Class<?> getObject() {
		return romeservice.class;
	}
	@Override
	public void onCreate() {
        super.onCreate();
        mostCurrent = this;
        if (processBA == null) {
		    processBA = new BA(this, null, null, "b4a.example.fbrtdb", "b4a.example.fbrtdb.romeservice");
            if (BA.isShellModeRuntimeCheck(processBA)) {
                processBA.raiseEvent2(null, true, "SHELL", false);
		    }
            try {
                Class.forName(BA.applicationContext.getPackageName() + ".main").getMethod("initializeProcessGlobals").invoke(null, null);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            processBA.loadHtSubs(this.getClass());
            ServiceHelper.init();
        }
        _service = new ServiceHelper(this);
        processBA.service = this;
        
        if (BA.isShellModeRuntimeCheck(processBA)) {
			processBA.raiseEvent2(null, true, "CREATE", true, "b4a.example.fbrtdb.romeservice", processBA, _service, anywheresoftware.b4a.keywords.Common.Density);
		}
        if (!false && ServiceHelper.StarterHelper.startFromServiceCreate(processBA, false) == false) {
				
		}
		else {
            processBA.setActivityPaused(false);
            BA.LogInfo("*** Service (romeservice) Create ***");
            processBA.raiseEvent(null, "service_create");
        }
        processBA.runHook("oncreate", this, null);
        if (false) {
			ServiceHelper.StarterHelper.runWaitForLayouts();
		}
    }
		@Override
	public void onStart(android.content.Intent intent, int startId) {
		onStartCommand(intent, 0, 0);
    }
    @Override
    public int onStartCommand(final android.content.Intent intent, int flags, int startId) {
    	if (ServiceHelper.StarterHelper.onStartCommand(processBA, new Runnable() {
            public void run() {
                handleStart(intent);
            }}))
			;
		else {
			ServiceHelper.StarterHelper.addWaitForLayout (new Runnable() {
				public void run() {
                    processBA.setActivityPaused(false);
                    BA.LogInfo("** Service (romeservice) Create **");
                    processBA.raiseEvent(null, "service_create");
					handleStart(intent);
                    ServiceHelper.StarterHelper.removeWaitForLayout();
				}
			});
		}
        processBA.runHook("onstartcommand", this, new Object[] {intent, flags, startId});
		return android.app.Service.START_NOT_STICKY;
    }
    public void onTaskRemoved(android.content.Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        if (false)
            processBA.raiseEvent(null, "service_taskremoved");
            
    }
    private void handleStart(android.content.Intent intent) {
    	BA.LogInfo("** Service (romeservice) Start **");
    	java.lang.reflect.Method startEvent = processBA.htSubs.get("service_start");
    	if (startEvent != null) {
    		if (startEvent.getParameterTypes().length > 0) {
    			anywheresoftware.b4a.objects.IntentWrapper iw = ServiceHelper.StarterHelper.handleStartIntent(intent, _service, processBA);
    			processBA.raiseEvent(null, "service_start", iw);
    		}
    		else {
    			processBA.raiseEvent(null, "service_start");
    		}
    	}
    }
	
	@Override
	public void onDestroy() {
        super.onDestroy();
        if (false) {
            BA.LogInfo("** Service (romeservice) Destroy (ignored)**");
        }
        else {
            BA.LogInfo("** Service (romeservice) Destroy **");
		    processBA.raiseEvent(null, "service_destroy");
            processBA.service = null;
		    mostCurrent = null;
		    processBA.setActivityPaused(true);
            processBA.runHook("ondestroy", this, null);
        }
	}

@Override
	public android.os.IBinder onBind(android.content.Intent intent) {
		return null;
	}public anywheresoftware.b4a.keywords.Common __c = null;
public static de.donmanfred.DatabaseReferenceWrapper _refroom = null;
public static anywheresoftware.b4a.objects.collections.List _allrooms = null;
public b4a.example.dateutils _dateutils = null;
public b4a.example.fbrtdb.main _main = null;
public b4a.example.fbrtdb.lobby _lobby = null;
public b4a.example.fbrtdb.messageservice _messageservice = null;
public b4a.example.fbrtdb.useservice _useservice = null;
public b4a.example.fbrtdb.starter _starter = null;
public b4a.example.fbrtdb.httputils2service _httputils2service = null;
public b4a.example.fbrtdb.xuiviewsutils _xuiviewsutils = null;
public b4a.example.fbrtdb.b4xcollections _b4xcollections = null;
public static String  _inti() throws Exception{
 //BA.debugLineNum = 22;BA.debugLine="Public Sub Inti()";
 //BA.debugLineNum = 25;BA.debugLine="AllRooms.Initialize";
_allrooms.Initialize();
 //BA.debugLineNum = 27;BA.debugLine="refRoom.Initialize(\"RefUserRoom\",Main.realtime.ge";
_refroom.Initialize(processBA,"RefUserRoom",(com.google.firebase.database.DatabaseReference)(mostCurrent._main._realtime /*de.donmanfred.FirebaseDatabaseWrapper*/ .getReferencefromUrl("https://first-project-5317d.firebaseio.com/Rooms").getObject()),(Object)("Room"));
 //BA.debugLineNum = 28;BA.debugLine="refRoom.addChildEventListener";
_refroom.addChildEventListener();
 //BA.debugLineNum = 32;BA.debugLine="End Sub";
return "";
}
public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 6;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 10;BA.debugLine="Public refRoom As DatabaseReference";
_refroom = new de.donmanfred.DatabaseReferenceWrapper();
 //BA.debugLineNum = 11;BA.debugLine="Public AllRooms As List";
_allrooms = new anywheresoftware.b4a.objects.collections.List();
 //BA.debugLineNum = 12;BA.debugLine="End Sub";
return "";
}
public static String  _refuserroom_onchildadded(Object _snapshot,String _child,Object _tag) throws Exception{
de.donmanfred.DataSnapshotWrapper _sn = null;
 //BA.debugLineNum = 34;BA.debugLine="Sub RefUserRoom_onChildAdded(Snapshot As Object, c";
 //BA.debugLineNum = 35;BA.debugLine="AllRooms.Add(Snapshot)";
_allrooms.Add(_snapshot);
 //BA.debugLineNum = 36;BA.debugLine="Log(\"Room added ..\")";
anywheresoftware.b4a.keywords.Common.LogImpl("05701634","Room added ..",0);
 //BA.debugLineNum = 38;BA.debugLine="Dim Sn As DataSnapshot";
_sn = new de.donmanfred.DataSnapshotWrapper();
 //BA.debugLineNum = 39;BA.debugLine="Sn.Initialize(Snapshot)";
_sn.Initialize(processBA,(com.google.firebase.database.DataSnapshot)(_snapshot));
 //BA.debugLineNum = 40;BA.debugLine="If(Sn.getChild(\"Members\").getChild(\"Member1\").Val";
if (((_sn.getChild("Members").getChild("Member1").getValue()).equals((Object)(mostCurrent._useservice._username /*String*/ )))) { 
 //BA.debugLineNum = 41;BA.debugLine="CallSubDelayed3(Lobby,\"AddScrollviewItem3\"";
anywheresoftware.b4a.keywords.Common.CallSubDelayed3(processBA,(Object)(mostCurrent._lobby.getObject()),"AddScrollviewItem3",_sn.getChild("Members").getChild("Member2").getValue(),_sn.getChild("Members").getChild("Member2").getValue());
 }else if((((_sn.getChild("Members").getChild("Member2").getValue()).equals((Object)(mostCurrent._useservice._username /*String*/ ))))) { 
 //BA.debugLineNum = 43;BA.debugLine="CallSubDelayed3(Lobby,\"AddScrollviewItem3\",Sn.ge";
anywheresoftware.b4a.keywords.Common.CallSubDelayed3(processBA,(Object)(mostCurrent._lobby.getObject()),"AddScrollviewItem3",_sn.getChild("Members").getChild("Member1").getValue(),_sn.getChild("Members").getChild("Member1").getValue());
 };
 //BA.debugLineNum = 45;BA.debugLine="End Sub";
return "";
}
public static String  _refuserroom_onchildchanged(Object _snapshot,String _child,Object _tag) throws Exception{
de.donmanfred.DataSnapshotWrapper _sn = null;
de.donmanfred.DataSnapshotWrapper _sm = null;
int _i = 0;
 //BA.debugLineNum = 47;BA.debugLine="Sub RefUserRoom_onChildChanged(Snapshot As Object,";
 //BA.debugLineNum = 48;BA.debugLine="Dim Sn,sm As DataSnapshot";
_sn = new de.donmanfred.DataSnapshotWrapper();
_sm = new de.donmanfred.DataSnapshotWrapper();
 //BA.debugLineNum = 49;BA.debugLine="Sn.Initialize(Snapshot)";
_sn.Initialize(processBA,(com.google.firebase.database.DataSnapshot)(_snapshot));
 //BA.debugLineNum = 50;BA.debugLine="Log(Snapshot)";
anywheresoftware.b4a.keywords.Common.LogImpl("05767171",BA.ObjectToString(_snapshot),0);
 //BA.debugLineNum = 52;BA.debugLine="If(MessageService.refMessage.IsInitialized) And N";
if ((mostCurrent._messageservice._refmessage /*de.donmanfred.DatabaseReferenceWrapper*/ .IsInitialized()) && anywheresoftware.b4a.keywords.Common.Not(mostCurrent._lobby._chatstate /*boolean*/ )) { 
 //BA.debugLineNum = 53;BA.debugLine="For i =0 To AllRooms.Size-1";
{
final int step5 = 1;
final int limit5 = (int) (_allrooms.getSize()-1);
_i = (int) (0) ;
for (;_i <= limit5 ;_i = _i + step5 ) {
 //BA.debugLineNum = 54;BA.debugLine="sm.Initialize(AllRooms.Get(i))";
_sm.Initialize(processBA,(com.google.firebase.database.DataSnapshot)(_allrooms.Get(_i)));
 //BA.debugLineNum = 55;BA.debugLine="If(sm.Key == Sn.Key) Then";
if (((_sm.getKey()).equals(_sn.getKey()))) { 
 //BA.debugLineNum = 57;BA.debugLine="If(sm.getChild(\"Members\").getChild(\"Member1\").V";
if (((_sm.getChild("Members").getChild("Member1").getValue()).equals((Object)(mostCurrent._useservice._username /*String*/ ))) || ((_sm.getChild("Members").getChild("Member2").getValue()).equals((Object)(mostCurrent._useservice._username /*String*/ )))) { 
 //BA.debugLineNum = 59;BA.debugLine="If(sm.getChild(\"Members\").getChild(\"Member1\")";
if (((_sm.getChild("Members").getChild("Member1").getValue()).equals((Object)(mostCurrent._useservice._username /*String*/ )))) { 
 //BA.debugLineNum = 60;BA.debugLine="If(MessageService.SenderMember == sm.getChil";
if (((mostCurrent._messageservice._sendermember /*String*/ ).equals(BA.ObjectToString(_sm.getChild("Members").getChild("Member2").getValue())) || (mostCurrent._messageservice._receivermember /*String*/ ).equals(BA.ObjectToString(_sm.getChild("Members").getChild("Member2").getValue())))) { 
 //BA.debugLineNum = 62;BA.debugLine="CallSubDelayed2(Lobby,\"AddMessageToLable\",sm";
anywheresoftware.b4a.keywords.Common.CallSubDelayed2(processBA,(Object)(mostCurrent._lobby.getObject()),"AddMessageToLable",_sm.getChild("Members").getChild("Member2").getValue());
 };
 }else if(((_sm.getChild("Members").getChild("Member2").getValue()).equals((Object)(mostCurrent._useservice._username /*String*/ )))) { 
 //BA.debugLineNum = 65;BA.debugLine="If(MessageService.SenderMember == sm.getChil";
if (((mostCurrent._messageservice._sendermember /*String*/ ).equals(BA.ObjectToString(_sm.getChild("Members").getChild("Member1").getValue())) || (mostCurrent._messageservice._receivermember /*String*/ ).equals(BA.ObjectToString(_sm.getChild("Members").getChild("Member1").getValue())))) { 
 //BA.debugLineNum = 66;BA.debugLine="CallSubDelayed2(Lobby,\"AddMessageToLable\",sm";
anywheresoftware.b4a.keywords.Common.CallSubDelayed2(processBA,(Object)(mostCurrent._lobby.getObject()),"AddMessageToLable",_sm.getChild("Members").getChild("Member1").getValue());
 };
 };
 };
 };
 }
};
 };
 //BA.debugLineNum = 77;BA.debugLine="End Sub";
return "";
}
public static String  _service_create() throws Exception{
 //BA.debugLineNum = 14;BA.debugLine="Sub Service_Create";
 //BA.debugLineNum = 16;BA.debugLine="End Sub";
return "";
}
public static String  _service_destroy() throws Exception{
 //BA.debugLineNum = 79;BA.debugLine="Sub Service_Destroy";
 //BA.debugLineNum = 81;BA.debugLine="End Sub";
return "";
}
public static String  _service_start(anywheresoftware.b4a.objects.IntentWrapper _startingintent) throws Exception{
 //BA.debugLineNum = 18;BA.debugLine="Sub Service_Start (StartingIntent As Intent)";
 //BA.debugLineNum = 19;BA.debugLine="Service.StopAutomaticForeground 'Call this when t";
mostCurrent._service.StopAutomaticForeground();
 //BA.debugLineNum = 20;BA.debugLine="End Sub";
return "";
}
}
