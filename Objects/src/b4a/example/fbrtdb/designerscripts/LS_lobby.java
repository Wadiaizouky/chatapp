package b4a.example.fbrtdb.designerscripts;
import anywheresoftware.b4a.objects.TextViewWrapper;
import anywheresoftware.b4a.objects.ImageViewWrapper;
import anywheresoftware.b4a.BA;


public class LS_lobby{

public static void LS_general(java.util.LinkedHashMap<String, anywheresoftware.b4a.keywords.LayoutBuilder.ViewWrapperAndAnchor> views, int width, int height, float scale) {
anywheresoftware.b4a.keywords.LayoutBuilder.setScaleRate(0.3);
//BA.debugLineNum = 5;BA.debugLine="Panel1.Height = 16%y"[Lobby/General script]
views.get("panel1").vw.setHeight((int)((16d / 100 * height)));
//BA.debugLineNum = 6;BA.debugLine="Panel1.Bottom = TabStrip1.Top"[Lobby/General script]
views.get("panel1").vw.setTop((int)((views.get("tabstrip1").vw.getTop()) - (views.get("panel1").vw.getHeight())));
//BA.debugLineNum = 7;BA.debugLine="Panel1.Top = 0dip"[Lobby/General script]
views.get("panel1").vw.setTop((int)((0d * scale)));

}
}