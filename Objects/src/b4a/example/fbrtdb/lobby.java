package b4a.example.fbrtdb;


import anywheresoftware.b4a.B4AMenuItem;
import android.app.Activity;
import android.os.Bundle;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.ObjectWrapper;
import anywheresoftware.b4a.objects.ActivityWrapper;
import java.lang.reflect.InvocationTargetException;
import anywheresoftware.b4a.B4AUncaughtException;
import anywheresoftware.b4a.debug.*;
import java.lang.ref.WeakReference;

public class lobby extends Activity implements B4AActivity{
	public static lobby mostCurrent;
	static boolean afterFirstLayout;
	static boolean isFirst = true;
    private static boolean processGlobalsRun = false;
	BALayout layout;
	public static BA processBA;
	BA activityBA;
    ActivityWrapper _activity;
    java.util.ArrayList<B4AMenuItem> menuItems;
	public static final boolean fullScreen = true;
	public static final boolean includeTitle = false;
    public static WeakReference<Activity> previousOne;
    public static boolean dontPause;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        mostCurrent = this;
		if (processBA == null) {
			processBA = new BA(this.getApplicationContext(), null, null, "b4a.example.fbrtdb", "b4a.example.fbrtdb.lobby");
			processBA.loadHtSubs(this.getClass());
	        float deviceScale = getApplicationContext().getResources().getDisplayMetrics().density;
	        BALayout.setDeviceScale(deviceScale);
            
		}
		else if (previousOne != null) {
			Activity p = previousOne.get();
			if (p != null && p != this) {
                BA.LogInfo("Killing previous instance (lobby).");
				p.finish();
			}
		}
        processBA.setActivityPaused(true);
        processBA.runHook("oncreate", this, null);
		if (!includeTitle) {
        	this.getWindow().requestFeature(android.view.Window.FEATURE_NO_TITLE);
        }
        if (fullScreen) {
        	getWindow().setFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN,   
        			android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
		
        processBA.sharedProcessBA.activityBA = null;
		layout = new BALayout(this);
		setContentView(layout);
		afterFirstLayout = false;
        WaitForLayout wl = new WaitForLayout();
        if (anywheresoftware.b4a.objects.ServiceHelper.StarterHelper.startFromActivity(this, processBA, wl, false))
		    BA.handler.postDelayed(wl, 5);

	}
	static class WaitForLayout implements Runnable {
		public void run() {
			if (afterFirstLayout)
				return;
			if (mostCurrent == null)
				return;
            
			if (mostCurrent.layout.getWidth() == 0) {
				BA.handler.postDelayed(this, 5);
				return;
			}
			mostCurrent.layout.getLayoutParams().height = mostCurrent.layout.getHeight();
			mostCurrent.layout.getLayoutParams().width = mostCurrent.layout.getWidth();
			afterFirstLayout = true;
			mostCurrent.afterFirstLayout();
		}
	}
	private void afterFirstLayout() {
        if (this != mostCurrent)
			return;
		activityBA = new BA(this, layout, processBA, "b4a.example.fbrtdb", "b4a.example.fbrtdb.lobby");
        
        processBA.sharedProcessBA.activityBA = new java.lang.ref.WeakReference<BA>(activityBA);
        anywheresoftware.b4a.objects.ViewWrapper.lastId = 0;
        _activity = new ActivityWrapper(activityBA, "activity");
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (BA.isShellModeRuntimeCheck(processBA)) {
			if (isFirst)
				processBA.raiseEvent2(null, true, "SHELL", false);
			processBA.raiseEvent2(null, true, "CREATE", true, "b4a.example.fbrtdb.lobby", processBA, activityBA, _activity, anywheresoftware.b4a.keywords.Common.Density, mostCurrent);
			_activity.reinitializeForShell(activityBA, "activity");
		}
        initializeProcessGlobals();		
        initializeGlobals();
        
        BA.LogInfo("** Activity (lobby) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, isFirst);
		isFirst = false;
		if (this != mostCurrent)
			return;
        processBA.setActivityPaused(false);
        BA.LogInfo("** Activity (lobby) Resume **");
        processBA.raiseEvent(null, "activity_resume");
        if (android.os.Build.VERSION.SDK_INT >= 11) {
			try {
				android.app.Activity.class.getMethod("invalidateOptionsMenu").invoke(this,(Object[]) null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
	public void addMenuItem(B4AMenuItem item) {
		if (menuItems == null)
			menuItems = new java.util.ArrayList<B4AMenuItem>();
		menuItems.add(item);
	}
	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		super.onCreateOptionsMenu(menu);
        try {
            if (processBA.subExists("activity_actionbarhomeclick")) {
                Class.forName("android.app.ActionBar").getMethod("setHomeButtonEnabled", boolean.class).invoke(
                    getClass().getMethod("getActionBar").invoke(this), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (processBA.runHook("oncreateoptionsmenu", this, new Object[] {menu}))
            return true;
		if (menuItems == null)
			return false;
		for (B4AMenuItem bmi : menuItems) {
			android.view.MenuItem mi = menu.add(bmi.title);
			if (bmi.drawable != null)
				mi.setIcon(bmi.drawable);
            if (android.os.Build.VERSION.SDK_INT >= 11) {
				try {
                    if (bmi.addToBar) {
				        android.view.MenuItem.class.getMethod("setShowAsAction", int.class).invoke(mi, 1);
                    }
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			mi.setOnMenuItemClickListener(new B4AMenuItemsClickListener(bmi.eventName.toLowerCase(BA.cul)));
		}
        
		return true;
	}   
 @Override
 public boolean onOptionsItemSelected(android.view.MenuItem item) {
    if (item.getItemId() == 16908332) {
        processBA.raiseEvent(null, "activity_actionbarhomeclick");
        return true;
    }
    else
        return super.onOptionsItemSelected(item); 
}
@Override
 public boolean onPrepareOptionsMenu(android.view.Menu menu) {
    super.onPrepareOptionsMenu(menu);
    processBA.runHook("onprepareoptionsmenu", this, new Object[] {menu});
    return true;
    
 }
 protected void onStart() {
    super.onStart();
    processBA.runHook("onstart", this, null);
}
 protected void onStop() {
    super.onStop();
    processBA.runHook("onstop", this, null);
}
    public void onWindowFocusChanged(boolean hasFocus) {
       super.onWindowFocusChanged(hasFocus);
       if (processBA.subExists("activity_windowfocuschanged"))
           processBA.raiseEvent2(null, true, "activity_windowfocuschanged", false, hasFocus);
    }
	private class B4AMenuItemsClickListener implements android.view.MenuItem.OnMenuItemClickListener {
		private final String eventName;
		public B4AMenuItemsClickListener(String eventName) {
			this.eventName = eventName;
		}
		public boolean onMenuItemClick(android.view.MenuItem item) {
			processBA.raiseEventFromUI(item.getTitle(), eventName + "_click");
			return true;
		}
	}
    public static Class<?> getObject() {
		return lobby.class;
	}
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;
	@Override
	public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {
        if (processBA.runHook("onkeydown", this, new Object[] {keyCode, event}))
            return true;
		if (onKeySubExist == null)
			onKeySubExist = processBA.subExists("activity_keypress");
		if (onKeySubExist) {
			if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK &&
					android.os.Build.VERSION.SDK_INT >= 18) {
				HandleKeyDelayed hk = new HandleKeyDelayed();
				hk.kc = keyCode;
				BA.handler.post(hk);
				return true;
			}
			else {
				boolean res = new HandleKeyDelayed().runDirectly(keyCode);
				if (res)
					return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	private class HandleKeyDelayed implements Runnable {
		int kc;
		public void run() {
			runDirectly(kc);
		}
		public boolean runDirectly(int keyCode) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keypress", false, keyCode);
			if (res == null || res == true) {
                return true;
            }
            else if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK) {
				finish();
				return true;
			}
            return false;
		}
		
	}
    @Override
	public boolean onKeyUp(int keyCode, android.view.KeyEvent event) {
        if (processBA.runHook("onkeyup", this, new Object[] {keyCode, event}))
            return true;
		if (onKeyUpSubExist == null)
			onKeyUpSubExist = processBA.subExists("activity_keyup");
		if (onKeyUpSubExist) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keyup", false, keyCode);
			if (res == null || res == true)
				return true;
		}
		return super.onKeyUp(keyCode, event);
	}
	@Override
	public void onNewIntent(android.content.Intent intent) {
        super.onNewIntent(intent);
		this.setIntent(intent);
        processBA.runHook("onnewintent", this, new Object[] {intent});
	}
    @Override 
	public void onPause() {
		super.onPause();
        if (_activity == null)
            return;
        if (this != mostCurrent)
			return;
		anywheresoftware.b4a.Msgbox.dismiss(true);
        if (!dontPause)
            BA.LogInfo("** Activity (lobby) Pause, UserClosed = " + activityBA.activity.isFinishing() + " **");
        else
            BA.LogInfo("** Activity (lobby) Pause event (activity is not paused). **");
        if (mostCurrent != null)
            processBA.raiseEvent2(_activity, true, "activity_pause", false, activityBA.activity.isFinishing());		
        if (!dontPause) {
            processBA.setActivityPaused(true);
            mostCurrent = null;
        }

        if (!activityBA.activity.isFinishing())
			previousOne = new WeakReference<Activity>(this);
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        processBA.runHook("onpause", this, null);
	}

	@Override
	public void onDestroy() {
        super.onDestroy();
		previousOne = null;
        processBA.runHook("ondestroy", this, null);
	}
    @Override 
	public void onResume() {
		super.onResume();
        mostCurrent = this;
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (activityBA != null) { //will be null during activity create (which waits for AfterLayout).
        	ResumeMessage rm = new ResumeMessage(mostCurrent);
        	BA.handler.post(rm);
        }
        processBA.runHook("onresume", this, null);
	}
    private static class ResumeMessage implements Runnable {
    	private final WeakReference<Activity> activity;
    	public ResumeMessage(Activity activity) {
    		this.activity = new WeakReference<Activity>(activity);
    	}
		public void run() {
            lobby mc = mostCurrent;
			if (mc == null || mc != activity.get())
				return;
			processBA.setActivityPaused(false);
            BA.LogInfo("** Activity (lobby) Resume **");
            if (mc != mostCurrent)
                return;
		    processBA.raiseEvent(mc._activity, "activity_resume", (Object[])null);
		}
    }
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
	      android.content.Intent data) {
		processBA.onActivityResult(requestCode, resultCode, data);
        processBA.runHook("onactivityresult", this, new Object[] {requestCode, resultCode});
	}
	private static void initializeGlobals() {
		processBA.raiseEvent2(null, true, "globals", false, (Object[])null);
	}
    public void onRequestPermissionsResult(int requestCode,
        String permissions[], int[] grantResults) {
        for (int i = 0;i < permissions.length;i++) {
            Object[] o = new Object[] {permissions[i], grantResults[i] == 0};
            processBA.raiseEventFromDifferentThread(null,null, 0, "activity_permissionresult", true, o);
        }
            
    }

public anywheresoftware.b4a.keywords.Common __c = null;
public static de.donmanfred.DatabaseReferenceWrapper _refuser = null;
public static anywheresoftware.b4a.phone.Phone.ContentChooser _chosser = null;
public static boolean _chatstate = false;
public anywheresoftware.b4a.objects.PanelWrapper _pnlprofile = null;
public anywheresoftware.b4a.objects.B4XViewWrapper.XUI _xui = null;
public anywheresoftware.b4a.objects.collections.List _allusers = null;
public static int _listvieexraitam = 0;
public b4a.example.fbrtdb.chat _chat1 = null;
public anywheresoftware.b4a.objects.IME _ime = null;
public b4a.example.fbrtdb.firebasstorage _storage = null;
public anywheresoftware.b4a.objects.PanelWrapper _panchat = null;
public anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _redcircle = null;
public anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _blckcircle = null;
public anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _profilepic = null;
public anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _peronprofilepic = null;
public anywheresoftware.b4a.objects.collections.List _allpersonpic = null;
public anywheresoftware.b4a.objects.collections.List _allfreindspic = null;
public anywheresoftware.b4a.objects.TabStripViewPager _tabstrip1 = null;
public anywheresoftware.b4a.objects.LabelWrapper _labusername = null;
public anywheresoftware.b4a.objects.LabelWrapper _laboptions = null;
public anywheresoftware.b4a.objects.B4XViewWrapper _imavuprofile = null;
public anywheresoftware.b4a.objects.LabelWrapper _labprofclose = null;
public b4a.example3.customlistview _customlisallusers = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imageuserprofile2 = null;
public anywheresoftware.b4a.objects.LabelWrapper _labeldeletelistite = null;
public anywheresoftware.b4a.objects.LabelWrapper _labepersonstatus = null;
public anywheresoftware.b4a.objects.LabelWrapper _labepersonname = null;
public anywheresoftware.b4a.objects.LabelWrapper _labeladdperson = null;
public anywheresoftware.b4a.objects.PanelWrapper _paneladdperson = null;
public b4a.example3.customlistview _customlisallcontacts = null;
public anywheresoftware.b4a.objects.PanelWrapper _panelfriendperson = null;
public anywheresoftware.b4a.objects.LabelWrapper _laberemovefriend = null;
public anywheresoftware.b4a.objects.LabelWrapper _labeldeletelistite2 = null;
public anywheresoftware.b4a.objects.LabelWrapper _labechatfriend = null;
public b4a.example3.customlistview _customlisallmessages = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imagevmessageprofil = null;
public anywheresoftware.b4a.objects.LabelWrapper _labelmessagename = null;
public anywheresoftware.b4a.objects.LabelWrapper _labrefresh = null;
public anywheresoftware.b4a.objects.LabelWrapper _labmessagenumbers = null;
public anywheresoftware.b4a.objects.PanelWrapper _panemessage = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imagevionlinestatue = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imavcoverr = null;
public anywheresoftware.b4a.objects.RuntimePermissions _rp = null;
public com.rootsoft.rspopupmenu.RSPopupMenu _rs = null;
public b4a.example.fbrtdb.b4xdialog _dialog = null;
public anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _chatback = null;
public anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _panlback = null;
public anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _profileback = null;
public anywheresoftware.b4a.objects.PanelWrapper _paneprofilperson = null;
public anywheresoftware.b4a.objects.B4XViewWrapper _imagevrofilepicture = null;
public anywheresoftware.b4a.objects.LabelWrapper _labuserstatue = null;
public b4a.example.dateutils _dateutils = null;
public b4a.example.fbrtdb.main _main = null;
public b4a.example.fbrtdb.romeservice _romeservice = null;
public b4a.example.fbrtdb.messageservice _messageservice = null;
public b4a.example.fbrtdb.useservice _useservice = null;
public b4a.example.fbrtdb.starter _starter = null;
public b4a.example.fbrtdb.httputils2service _httputils2service = null;
public b4a.example.fbrtdb.xuiviewsutils _xuiviewsutils = null;
public b4a.example.fbrtdb.b4xcollections _b4xcollections = null;

public static void initializeProcessGlobals() {
             try {
                Class.forName(BA.applicationContext.getPackageName() + ".main").getMethod("initializeProcessGlobals").invoke(null, null);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
}
public static void  _activity_create(boolean _firsttime) throws Exception{
ResumableSub_Activity_Create rsub = new ResumableSub_Activity_Create(null,_firsttime);
rsub.resume(processBA, null);
}
public static class ResumableSub_Activity_Create extends BA.ResumableSub {
public ResumableSub_Activity_Create(b4a.example.fbrtdb.lobby parent,boolean _firsttime) {
this.parent = parent;
this._firsttime = _firsttime;
}
b4a.example.fbrtdb.lobby parent;
boolean _firsttime;
anywheresoftware.b4a.objects.drawable.ColorDrawable _cd = null;
anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper _ph = null;

@Override
public void resume(BA ba, Object[] result) throws Exception{

    while (true) {
        switch (state) {
            case -1:
return;

case 0:
//C
this.state = 1;
 //BA.debugLineNum = 78;BA.debugLine="Activity.LoadLayout(\"Lobby\")";
parent.mostCurrent._activity.LoadLayout("Lobby",mostCurrent.activityBA);
 //BA.debugLineNum = 80;BA.debugLine="AllPersonPic.Initialize";
parent.mostCurrent._allpersonpic.Initialize();
 //BA.debugLineNum = 81;BA.debugLine="AllFreindspic.Initialize";
parent.mostCurrent._allfreindspic.Initialize();
 //BA.debugLineNum = 83;BA.debugLine="Storage.Initialize";
parent.mostCurrent._storage._initialize /*String*/ (processBA);
 //BA.debugLineNum = 84;BA.debugLine="DownloadImagesApp";
_downloadimagesapp();
 //BA.debugLineNum = 86;BA.debugLine="AllUsers.Initialize";
parent.mostCurrent._allusers.Initialize();
 //BA.debugLineNum = 87;BA.debugLine="listvieExraitam = -1";
parent._listvieexraitam = (int) (-1);
 //BA.debugLineNum = 88;BA.debugLine="StartService(UseService)";
anywheresoftware.b4a.keywords.Common.StartService(processBA,(Object)(parent.mostCurrent._useservice.getObject()));
 //BA.debugLineNum = 89;BA.debugLine="CallSubDelayed(UseService,\"Inti\")";
anywheresoftware.b4a.keywords.Common.CallSubDelayed(processBA,(Object)(parent.mostCurrent._useservice.getObject()),"Inti");
 //BA.debugLineNum = 92;BA.debugLine="Sleep(2000)";
anywheresoftware.b4a.keywords.Common.Sleep(mostCurrent.activityBA,this,(int) (2000));
this.state = 7;
return;
case 7:
//C
this.state = 1;
;
 //BA.debugLineNum = 93;BA.debugLine="StartService(RomeService)";
anywheresoftware.b4a.keywords.Common.StartService(processBA,(Object)(parent.mostCurrent._romeservice.getObject()));
 //BA.debugLineNum = 94;BA.debugLine="CallSubDelayed(RomeService,\"Inti\")";
anywheresoftware.b4a.keywords.Common.CallSubDelayed(processBA,(Object)(parent.mostCurrent._romeservice.getObject()),"Inti");
 //BA.debugLineNum = 96;BA.debugLine="StartService(MessageService)";
anywheresoftware.b4a.keywords.Common.StartService(processBA,(Object)(parent.mostCurrent._messageservice.getObject()));
 //BA.debugLineNum = 97;BA.debugLine="MessageService.AllMessages.Initialize";
parent.mostCurrent._messageservice._allmessages /*anywheresoftware.b4a.objects.collections.List*/ .Initialize();
 //BA.debugLineNum = 99;BA.debugLine="RefUser.Initialize(\"RefUser2\",Main.realtime.getRe";
parent._refuser.Initialize(processBA,"RefUser2",(com.google.firebase.database.DatabaseReference)(parent.mostCurrent._main._realtime /*de.donmanfred.FirebaseDatabaseWrapper*/ .getReferencefromUrl(("https://first-project-5317d.firebaseio.com/Users")).getObject()),(Object)("User"));
 //BA.debugLineNum = 100;BA.debugLine="RefUser.addChildEventListener";
parent._refuser.addChildEventListener();
 //BA.debugLineNum = 106;BA.debugLine="TabStrip1.LoadLayout(\"Page1\", \"All Users\")";
parent.mostCurrent._tabstrip1.LoadLayout("Page1",BA.ObjectToCharSequence("All Users"));
 //BA.debugLineNum = 107;BA.debugLine="TabStrip1.LoadLayout(\"Page2\", \"My Contact\")";
parent.mostCurrent._tabstrip1.LoadLayout("Page2",BA.ObjectToCharSequence("My Contact"));
 //BA.debugLineNum = 108;BA.debugLine="TabStrip1.LoadLayout(\"page3\", \"Chat Messages\")";
parent.mostCurrent._tabstrip1.LoadLayout("page3",BA.ObjectToCharSequence("Chat Messages"));
 //BA.debugLineNum = 111;BA.debugLine="Pnlprofile.Initialize(\"Pnlprofile\")";
parent.mostCurrent._pnlprofile.Initialize(mostCurrent.activityBA,"Pnlprofile");
 //BA.debugLineNum = 112;BA.debugLine="Dim cd As ColorDrawable";
_cd = new anywheresoftware.b4a.objects.drawable.ColorDrawable();
 //BA.debugLineNum = 113;BA.debugLine="cd.Initialize(Colors.White,0)";
_cd.Initialize(anywheresoftware.b4a.keywords.Common.Colors.White,(int) (0));
 //BA.debugLineNum = 114;BA.debugLine="Pnlprofile.Background = cd";
parent.mostCurrent._pnlprofile.setBackground((android.graphics.drawable.Drawable)(_cd.getObject()));
 //BA.debugLineNum = 115;BA.debugLine="Activity.AddView(Pnlprofile,50dip,Activity.Height";
parent.mostCurrent._activity.AddView((android.view.View)(parent.mostCurrent._pnlprofile.getObject()),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (50)),(int) (parent.mostCurrent._activity.getHeight()/(double)2),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (70),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (70),mostCurrent.activityBA));
 //BA.debugLineNum = 117;BA.debugLine="Pnlprofile.LoadLayout(\"UserProfile\")";
parent.mostCurrent._pnlprofile.LoadLayout("UserProfile",mostCurrent.activityBA);
 //BA.debugLineNum = 118;BA.debugLine="Pnlprofile.SetLayoutAnimated(0,Activity.Width/2,I";
parent.mostCurrent._pnlprofile.SetLayoutAnimated((int) (0),(int) (parent.mostCurrent._activity.getWidth()/(double)2),parent.mostCurrent._imavuprofile.getHeight(),(int) (0),(int) (0));
 //BA.debugLineNum = 119;BA.debugLine="Pnlprofile.SetElevationAnimated(0,4dip)";
parent.mostCurrent._pnlprofile.SetElevationAnimated((int) (0),(float) (anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (4))));
 //BA.debugLineNum = 123;BA.debugLine="PanChat.Initialize(\"PanChat\")";
parent.mostCurrent._panchat.Initialize(mostCurrent.activityBA,"PanChat");
 //BA.debugLineNum = 124;BA.debugLine="cd.Initialize(Colors.White,30dip)";
_cd.Initialize(anywheresoftware.b4a.keywords.Common.Colors.White,anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (30)));
 //BA.debugLineNum = 125;BA.debugLine="PanChat.Background = cd";
parent.mostCurrent._panchat.setBackground((android.graphics.drawable.Drawable)(_cd.getObject()));
 //BA.debugLineNum = 126;BA.debugLine="Activity.AddView(PanChat,0,Activity.Height,Activi";
parent.mostCurrent._activity.AddView((android.view.View)(parent.mostCurrent._panchat.getObject()),(int) (0),parent.mostCurrent._activity.getHeight(),parent.mostCurrent._activity.getWidth(),parent.mostCurrent._activity.getHeight());
 //BA.debugLineNum = 127;BA.debugLine="chat1.Initialize(PanChat)";
parent.mostCurrent._chat1._initialize /*String*/ (mostCurrent.activityBA,(anywheresoftware.b4a.objects.B4XViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.B4XViewWrapper(), (java.lang.Object)(parent.mostCurrent._panchat.getObject())));
 //BA.debugLineNum = 128;BA.debugLine="PanChat.SetElevationAnimated(0,4dip)";
parent.mostCurrent._panchat.SetElevationAnimated((int) (0),(float) (anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (4))));
 //BA.debugLineNum = 130;BA.debugLine="Activity.Title = \"Chat Example\"";
parent.mostCurrent._activity.setTitle(BA.ObjectToCharSequence("Chat Example"));
 //BA.debugLineNum = 131;BA.debugLine="ime.Initialize(\"ime\")";
parent.mostCurrent._ime.Initialize("ime");
 //BA.debugLineNum = 132;BA.debugLine="ime.AddHeightChangedEvent";
parent.mostCurrent._ime.AddHeightChangedEvent(mostCurrent.activityBA);
 //BA.debugLineNum = 138;BA.debugLine="RedCircle = LoadBitmap(File.DirAssets,\"Green1.png";
parent.mostCurrent._redcircle = anywheresoftware.b4a.keywords.Common.LoadBitmap(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"Green1.png");
 //BA.debugLineNum = 139;BA.debugLine="BlckCircle = LoadBitmap(File.DirAssets,\"black1.pn";
parent.mostCurrent._blckcircle = anywheresoftware.b4a.keywords.Common.LoadBitmap(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"black1.png");
 //BA.debugLineNum = 142;BA.debugLine="Chosser.Initialize(\"chooser\")";
parent._chosser.Initialize("chooser");
 //BA.debugLineNum = 145;BA.debugLine="LabUserName.Text = Main.UserId";
parent.mostCurrent._labusername.setText(BA.ObjectToCharSequence(parent.mostCurrent._main._userid /*String*/ ));
 //BA.debugLineNum = 146;BA.debugLine="LabUserStatue.Text = UseService.Status";
parent.mostCurrent._labuserstatue.setText(BA.ObjectToCharSequence(parent.mostCurrent._useservice._status /*String*/ ));
 //BA.debugLineNum = 147;BA.debugLine="If Not(UseService.ProfilPhotoUrl == \"\") Then";
if (true) break;

case 1:
//if
this.state = 6;
if (anywheresoftware.b4a.keywords.Common.Not((parent.mostCurrent._useservice._profilphotourl /*String*/ ).equals(""))) { 
this.state = 3;
}else {
this.state = 5;
}if (true) break;

case 3:
//C
this.state = 6;
 //BA.debugLineNum = 148;BA.debugLine="Storage.DownloadProfilImage(UseService.ProfilPho";
parent.mostCurrent._storage._downloadprofilimage /*void*/ (parent.mostCurrent._useservice._profilphotourl /*String*/ ,parent.mostCurrent._profilepic,parent.mostCurrent._useservice._profilphotourl /*String*/ .substring((int) (6)),anywheresoftware.b4a.keywords.Common.False,(int) (0));
 if (true) break;

case 5:
//C
this.state = 6;
 //BA.debugLineNum = 150;BA.debugLine="ProfilePic = LoadBitmap(File.DirAssets,\"unknowni";
parent.mostCurrent._profilepic = anywheresoftware.b4a.keywords.Common.LoadBitmap(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"unknownimage.png");
 //BA.debugLineNum = 151;BA.debugLine="Dim ph As B4XBitmap = ProfilePic";
_ph = new anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper();
_ph = (anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper(), (android.graphics.Bitmap)(parent.mostCurrent._profilepic.getObject()));
 //BA.debugLineNum = 152;BA.debugLine="ImavUProfile.SetBitmap(CreateRoundBitmap(ph,Imav";
parent.mostCurrent._imavuprofile.SetBitmap((android.graphics.Bitmap)(_createroundbitmap(_ph,parent.mostCurrent._imavuprofile.getWidth()).getObject()));
 if (true) break;

case 6:
//C
this.state = -1;
;
 //BA.debugLineNum = 156;BA.debugLine="Rs.Initialize(\"PopupMenu\",LabOptions )";
parent.mostCurrent._rs.Initialize(mostCurrent.activityBA,"PopupMenu",(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(parent.mostCurrent._laboptions.getObject())));
 //BA.debugLineNum = 157;BA.debugLine="Rs.AddMenuItem(0, 0, \"Show My Information\")";
parent.mostCurrent._rs.AddMenuItem((int) (0),(int) (0),"Show My Information");
 //BA.debugLineNum = 158;BA.debugLine="Rs.AddMenuItem(1, 1, \"Edit State\")";
parent.mostCurrent._rs.AddMenuItem((int) (1),(int) (1),"Edit State");
 //BA.debugLineNum = 159;BA.debugLine="Rs.AddMenuItem(2, 2, \"Details\")";
parent.mostCurrent._rs.AddMenuItem((int) (2),(int) (2),"Details");
 //BA.debugLineNum = 160;BA.debugLine="dialog.Initialize(Activity)";
parent.mostCurrent._dialog._initialize /*String*/ (mostCurrent.activityBA,(anywheresoftware.b4a.objects.B4XViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.B4XViewWrapper(), (java.lang.Object)(parent.mostCurrent._activity.getObject())));
 //BA.debugLineNum = 161;BA.debugLine="dialog.Title = \"Edit State\"";
parent.mostCurrent._dialog._title /*Object*/  = (Object)("Edit State");
 //BA.debugLineNum = 163;BA.debugLine="Sleep(6000)";
anywheresoftware.b4a.keywords.Common.Sleep(mostCurrent.activityBA,this,(int) (6000));
this.state = 8;
return;
case 8:
//C
this.state = -1;
;
 //BA.debugLineNum = 164;BA.debugLine="DowloadPersonsProfPic";
_dowloadpersonsprofpic();
 //BA.debugLineNum = 166;BA.debugLine="End Sub";
if (true) break;

            }
        }
    }
}
public static String  _activity_pause(boolean _userclosed) throws Exception{
int _i = 0;
de.donmanfred.DataSnapshotWrapper _sn = null;
 //BA.debugLineNum = 191;BA.debugLine="Sub Activity_Pause (UserClosed As Boolean)";
 //BA.debugLineNum = 192;BA.debugLine="If(UserClosed) Then";
if ((_userclosed)) { 
 //BA.debugLineNum = 193;BA.debugLine="RefUser.Child(UseService.childd).updateChildren(";
_refuser.Child(BA.ObjectToString(mostCurrent._useservice._childd /*Object*/ )).updateChildren(processBA,anywheresoftware.b4a.keywords.Common.createMap(new Object[] {(Object)("IsOnline"),(Object)(anywheresoftware.b4a.keywords.Common.False)}));
 //BA.debugLineNum = 194;BA.debugLine="UseService.IsOnline = False";
mostCurrent._useservice._isonline /*boolean*/  = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 196;BA.debugLine="For i =0 To MessageService.AllMessages.Size-1";
{
final int step4 = 1;
final int limit4 = (int) (mostCurrent._messageservice._allmessages /*anywheresoftware.b4a.objects.collections.List*/ .getSize()-1);
_i = (int) (0) ;
for (;_i <= limit4 ;_i = _i + step4 ) {
 //BA.debugLineNum = 197;BA.debugLine="Dim Sn As DataSnapshot";
_sn = new de.donmanfred.DataSnapshotWrapper();
 //BA.debugLineNum = 198;BA.debugLine="Sn.Initialize(MessageService.AllMessages.Get(i)";
_sn.Initialize(processBA,(com.google.firebase.database.DataSnapshot)(mostCurrent._messageservice._allmessages /*anywheresoftware.b4a.objects.collections.List*/ .Get(_i)));
 //BA.debugLineNum = 199;BA.debugLine="MessageService.refMessage.Child(Sn.Key).removeV";
mostCurrent._messageservice._refmessage /*de.donmanfred.DatabaseReferenceWrapper*/ .Child(_sn.getKey()).removeValue();
 }
};
 //BA.debugLineNum = 203;BA.debugLine="MessageService.AllMessages.Clear";
mostCurrent._messageservice._allmessages /*anywheresoftware.b4a.objects.collections.List*/ .Clear();
 };
 //BA.debugLineNum = 206;BA.debugLine="End Sub";
return "";
}
public static String  _activity_resume() throws Exception{
 //BA.debugLineNum = 187;BA.debugLine="Sub Activity_Resume";
 //BA.debugLineNum = 189;BA.debugLine="End Sub";
return "";
}
public static String  _addimagetochat(String _m,boolean _isr) throws Exception{
 //BA.debugLineNum = 607;BA.debugLine="Public Sub AddImageToChat(M As String,isr As Boole";
 //BA.debugLineNum = 608;BA.debugLine="chat1.AddImage(M,isr,chat1.chooserBitmap)";
mostCurrent._chat1._addimage /*String*/ (_m,_isr,mostCurrent._chat1._chooserbitmap /*anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper*/ );
 //BA.debugLineNum = 609;BA.debugLine="End Sub";
return "";
}
public static String  _addmessagetolable(String _recuser) throws Exception{
int _i = 0;
anywheresoftware.b4a.objects.B4XViewWrapper _pnluser = null;
anywheresoftware.b4a.objects.LabelWrapper _lb = null;
int _valuse = 0;
 //BA.debugLineNum = 674;BA.debugLine="Public Sub AddMessageToLable(RecUser As String)";
 //BA.debugLineNum = 676;BA.debugLine="For i =0 To CustomLisAllMessages.Size-1";
{
final int step1 = 1;
final int limit1 = (int) (mostCurrent._customlisallmessages._getsize()-1);
_i = (int) (0) ;
for (;_i <= limit1 ;_i = _i + step1 ) {
 //BA.debugLineNum = 677;BA.debugLine="If(CustomLisAllMessages.GetValue(i) == RecUser)";
if (((mostCurrent._customlisallmessages._getvalue(_i)).equals((Object)(_recuser)))) { 
 //BA.debugLineNum = 678;BA.debugLine="Dim pnluser As B4XView";
_pnluser = new anywheresoftware.b4a.objects.B4XViewWrapper();
 //BA.debugLineNum = 679;BA.debugLine="pnluser = CustomLisAllMessages.GetPanel(i)";
_pnluser = mostCurrent._customlisallmessages._getpanel(_i);
 //BA.debugLineNum = 680;BA.debugLine="Dim lb As Label";
_lb = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 681;BA.debugLine="lb = pnluser.GetView(0).GetView(3)";
_lb = (anywheresoftware.b4a.objects.LabelWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.LabelWrapper(), (android.widget.TextView)(_pnluser.GetView((int) (0)).GetView((int) (3)).getObject()));
 //BA.debugLineNum = 682;BA.debugLine="Dim valuse As Int = lb.Text";
_valuse = (int)(Double.parseDouble(_lb.getText()));
 //BA.debugLineNum = 683;BA.debugLine="valuse = valuse+1";
_valuse = (int) (_valuse+1);
 //BA.debugLineNum = 684;BA.debugLine="lb.Text = valuse";
_lb.setText(BA.ObjectToCharSequence(_valuse));
 };
 }
};
 //BA.debugLineNum = 688;BA.debugLine="End Sub";
return "";
}
public static String  _addscrollviewitem(String _value,String _name,String _status,String _profpic) throws Exception{
anywheresoftware.b4a.objects.B4XViewWrapper _pn = null;
 //BA.debugLineNum = 208;BA.debugLine="Sub AddScrollviewItem(Value As String,Name As Stri";
 //BA.debugLineNum = 209;BA.debugLine="Dim pn As B4XView = xui.CreatePanel(\"\")";
_pn = new anywheresoftware.b4a.objects.B4XViewWrapper();
_pn = mostCurrent._xui.CreatePanel(processBA,"");
 //BA.debugLineNum = 210;BA.debugLine="pn.SetLayoutAnimated(0,0,0,CustomLisAllUsers.AsVi";
_pn.SetLayoutAnimated((int) (0),(int) (0),(int) (0),mostCurrent._customlisallusers._asview().getWidth(),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (60)));
 //BA.debugLineNum = 211;BA.debugLine="pn.LoadLayout(\"cellitem\")";
_pn.LoadLayout("cellitem",mostCurrent.activityBA);
 //BA.debugLineNum = 212;BA.debugLine="LabePersonName.Text = Name";
mostCurrent._labepersonname.setText(BA.ObjectToCharSequence(_name));
 //BA.debugLineNum = 213;BA.debugLine="LabePersonstatus.Text = Status";
mostCurrent._labepersonstatus.setText(BA.ObjectToCharSequence(_status));
 //BA.debugLineNum = 215;BA.debugLine="AllPersonPic.Add(Profpic)";
mostCurrent._allpersonpic.Add((Object)(_profpic));
 //BA.debugLineNum = 216;BA.debugLine="ImageVRofilePicture.SetBitmap(CreateRoundBitma";
mostCurrent._imagevrofilepicture.SetBitmap((android.graphics.Bitmap)(_createroundbitmap((anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper(), (android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"unknownimage.png").getObject())),mostCurrent._imagevrofilepicture.getWidth()).getObject()));
 //BA.debugLineNum = 219;BA.debugLine="ImageVRofilePicture.Tag = Profpic";
mostCurrent._imagevrofilepicture.setTag((Object)(_profpic));
 //BA.debugLineNum = 220;BA.debugLine="CustomLisAllUsers.Add(pn,Value)";
mostCurrent._customlisallusers._add(_pn,(Object)(_value));
 //BA.debugLineNum = 221;BA.debugLine="End Sub";
return "";
}
public static String  _addscrollviewitem2(String _value,String _name,String _status,boolean _isonline,String _profpic) throws Exception{
anywheresoftware.b4a.objects.B4XViewWrapper _pn = null;
 //BA.debugLineNum = 223;BA.debugLine="Sub AddScrollviewItem2(Value As String,Name As Str";
 //BA.debugLineNum = 224;BA.debugLine="Dim pn As B4XView = xui.CreatePanel(\"\")";
_pn = new anywheresoftware.b4a.objects.B4XViewWrapper();
_pn = mostCurrent._xui.CreatePanel(processBA,"");
 //BA.debugLineNum = 225;BA.debugLine="pn.SetLayoutAnimated(0,0,0,CustomLisAllUsers.AsVi";
_pn.SetLayoutAnimated((int) (0),(int) (0),(int) (0),mostCurrent._customlisallusers._asview().getWidth(),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (60)));
 //BA.debugLineNum = 226;BA.debugLine="pn.LoadLayout(\"cellitem\")";
_pn.LoadLayout("cellitem",mostCurrent.activityBA);
 //BA.debugLineNum = 227;BA.debugLine="LabePersonName.Text = Name";
mostCurrent._labepersonname.setText(BA.ObjectToCharSequence(_name));
 //BA.debugLineNum = 228;BA.debugLine="LabePersonstatus.Text = Status";
mostCurrent._labepersonstatus.setText(BA.ObjectToCharSequence(_status));
 //BA.debugLineNum = 229;BA.debugLine="ImageVRofilePicture.SetBitmap(CreateRoundBitmap(L";
mostCurrent._imagevrofilepicture.SetBitmap((android.graphics.Bitmap)(_createroundbitmap((anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper(), (android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"unknownimage.png").getObject())),mostCurrent._imagevrofilepicture.getWidth()).getObject()));
 //BA.debugLineNum = 230;BA.debugLine="If(Isonline) Then";
if ((_isonline)) { 
 //BA.debugLineNum = 231;BA.debugLine="ImageViOnlineStatue.Bitmap = RedCircle";
mostCurrent._imagevionlinestatue.setBitmap((android.graphics.Bitmap)(mostCurrent._redcircle.getObject()));
 }else {
 //BA.debugLineNum = 233;BA.debugLine="ImageViOnlineStatue.Bitmap = BlckCircle";
mostCurrent._imagevionlinestatue.setBitmap((android.graphics.Bitmap)(mostCurrent._blckcircle.getObject()));
 };
 //BA.debugLineNum = 235;BA.debugLine="ImageVRofilePicture.Tag = profpic";
mostCurrent._imagevrofilepicture.setTag((Object)(_profpic));
 //BA.debugLineNum = 236;BA.debugLine="CustomLisAllContacts.Add(pn,Value)";
mostCurrent._customlisallcontacts._add(_pn,(Object)(_value));
 //BA.debugLineNum = 237;BA.debugLine="End Sub";
return "";
}
public static String  _addscrollviewitem3(String _value,String _name) throws Exception{
anywheresoftware.b4a.objects.B4XViewWrapper _pn = null;
 //BA.debugLineNum = 239;BA.debugLine="public Sub AddScrollviewItem3(Value As String,Name";
 //BA.debugLineNum = 240;BA.debugLine="Dim pn As B4XView = xui.CreatePanel(\"\")";
_pn = new anywheresoftware.b4a.objects.B4XViewWrapper();
_pn = mostCurrent._xui.CreatePanel(processBA,"");
 //BA.debugLineNum = 241;BA.debugLine="pn.SetLayoutAnimated(0,0,0,CustomLisAllUsers.AsVi";
_pn.SetLayoutAnimated((int) (0),(int) (0),(int) (0),mostCurrent._customlisallusers._asview().getWidth(),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (60)));
 //BA.debugLineNum = 242;BA.debugLine="pn.LoadLayout(\"CellItemMessages\")";
_pn.LoadLayout("CellItemMessages",mostCurrent.activityBA);
 //BA.debugLineNum = 243;BA.debugLine="LabelMessageName.Text = Name";
mostCurrent._labelmessagename.setText(BA.ObjectToCharSequence(_name));
 //BA.debugLineNum = 245;BA.debugLine="CustomLisAllMessages.Add(pn,Value)";
mostCurrent._customlisallmessages._add(_pn,(Object)(_value));
 //BA.debugLineNum = 246;BA.debugLine="End Sub";
return "";
}
public static String  _addvoicetochat(String _m,boolean _isr) throws Exception{
 //BA.debugLineNum = 611;BA.debugLine="Public Sub AddVoiceToChat(M As String,isr As Boole";
 //BA.debugLineNum = 612;BA.debugLine="chat1.AddIAudio(chat1.Dirvoice,chat1.FileNameVoic";
mostCurrent._chat1._addiaudio /*String*/ (mostCurrent._chat1._dirvoice /*String*/ ,mostCurrent._chat1._filenamevoice /*String*/ ,_isr);
 //BA.debugLineNum = 613;BA.debugLine="End Sub";
return "";
}
public static String  _butchangeprofpic_click() throws Exception{
 //BA.debugLineNum = 727;BA.debugLine="Sub ButChangeProfPic_Click";
 //BA.debugLineNum = 728;BA.debugLine="Chosser.Show(\"image/*\", \"Choose image\")";
_chosser.Show(processBA,"image/*","Choose image");
 //BA.debugLineNum = 729;BA.debugLine="End Sub";
return "";
}
public static void  _changestate() throws Exception{
ResumableSub_ChangeState rsub = new ResumableSub_ChangeState(null);
rsub.resume(processBA, null);
}
public static class ResumableSub_ChangeState extends BA.ResumableSub {
public ResumableSub_ChangeState(b4a.example.fbrtdb.lobby parent) {
this.parent = parent;
}
b4a.example.fbrtdb.lobby parent;
b4a.example.fbrtdb.b4xinputtemplate _input = null;
int _result = 0;
String _res = "";

@Override
public void resume(BA ba, Object[] result) throws Exception{

    while (true) {
        switch (state) {
            case -1:
return;

case 0:
//C
this.state = 1;
 //BA.debugLineNum = 841;BA.debugLine="Dim input As B4XInputTemplate";
_input = new b4a.example.fbrtdb.b4xinputtemplate();
 //BA.debugLineNum = 842;BA.debugLine="input.Initialize";
_input._initialize /*String*/ (mostCurrent.activityBA);
 //BA.debugLineNum = 843;BA.debugLine="input.lblTitle.Text = \"Enter Text:\"";
_input._lbltitle /*anywheresoftware.b4a.objects.B4XViewWrapper*/ .setText(BA.ObjectToCharSequence("Enter Text:"));
 //BA.debugLineNum = 845;BA.debugLine="Wait For (dialog.ShowTemplate(input, \"OK\", \"\", \"C";
anywheresoftware.b4a.keywords.Common.WaitFor("complete", processBA, this, parent.mostCurrent._dialog._showtemplate /*anywheresoftware.b4a.keywords.Common.ResumableSubWrapper*/ ((Object)(_input),(Object)("OK"),(Object)(""),(Object)("CANCEL")));
this.state = 5;
return;
case 5:
//C
this.state = 1;
_result = (Integer) result[0];
;
 //BA.debugLineNum = 846;BA.debugLine="If Result = xui.DialogResponse_Positive Then";
if (true) break;

case 1:
//if
this.state = 4;
if (_result==parent.mostCurrent._xui.DialogResponse_Positive) { 
this.state = 3;
}if (true) break;

case 3:
//C
this.state = 4;
 //BA.debugLineNum = 847;BA.debugLine="Dim res As String = input.TextField1.Text 'no ne";
_res = _input._textfield1 /*anywheresoftware.b4a.objects.B4XViewWrapper*/ .getText();
 //BA.debugLineNum = 848;BA.debugLine="UseService.Status = res";
parent.mostCurrent._useservice._status /*String*/  = _res;
 //BA.debugLineNum = 849;BA.debugLine="RefUser.Child(UseService.childd).updateChildren(";
parent._refuser.Child(BA.ObjectToString(parent.mostCurrent._useservice._childd /*Object*/ )).updateChildren(processBA,anywheresoftware.b4a.keywords.Common.createMap(new Object[] {(Object)("Status"),(Object)(_res)}));
 if (true) break;

case 4:
//C
this.state = -1;
;
 //BA.debugLineNum = 851;BA.debugLine="End Sub";
if (true) break;

            }
        }
    }
}
public static void  _complete(int _result) throws Exception{
}
public static String  _chooser_result(boolean _success,String _dir,String _filename) throws Exception{
anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _imag = null;
 //BA.debugLineNum = 731;BA.debugLine="Sub chooser_Result (Success As Boolean, Dir As Str";
 //BA.debugLineNum = 732;BA.debugLine="If Success Then";
if (_success) { 
 //BA.debugLineNum = 733;BA.debugLine="Storage.UploadProfilImage(\"/user/\"&FileNam";
mostCurrent._storage._uploadprofilimage /*void*/ ("/user/"+_filename.substring((int) (61))+".jpg",_dir,_filename);
 //BA.debugLineNum = 734;BA.debugLine="UseService.ProfilPhotoUrl = \"/user/\"&FileName.Su";
mostCurrent._useservice._profilphotourl /*String*/  = "/user/"+_filename.substring((int) (61))+".jpg";
 //BA.debugLineNum = 736;BA.debugLine="Dim imag As Bitmap";
_imag = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper();
 //BA.debugLineNum = 737;BA.debugLine="imag.Initialize(Dir,FileName)";
_imag.Initialize(_dir,_filename);
 //BA.debugLineNum = 738;BA.debugLine="ImageUserProfile2.Bitmap = imag";
mostCurrent._imageuserprofile2.setBitmap((android.graphics.Bitmap)(_imag.getObject()));
 }else {
 //BA.debugLineNum = 740;BA.debugLine="ToastMessageShow(\"No image selected\", True)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow(BA.ObjectToCharSequence("No image selected"),anywheresoftware.b4a.keywords.Common.True);
 };
 //BA.debugLineNum = 744;BA.debugLine="End Sub";
return "";
}
public static String  _closechat() throws Exception{
 //BA.debugLineNum = 628;BA.debugLine="Public Sub CloseChat";
 //BA.debugLineNum = 629;BA.debugLine="chat1.ClearMessage";
mostCurrent._chat1._clearmessage /*String*/ ();
 //BA.debugLineNum = 630;BA.debugLine="PanChat.SetLayoutAnimated(300,0,Activity.Height,A";
mostCurrent._panchat.SetLayoutAnimated((int) (300),(int) (0),mostCurrent._activity.getHeight(),mostCurrent._activity.getWidth(),mostCurrent._activity.getHeight());
 //BA.debugLineNum = 631;BA.debugLine="ChatState = False";
_chatstate = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 641;BA.debugLine="End Sub";
return "";
}
public static anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper  _createroundbitmap(anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper _input,int _size) throws Exception{
int _l = 0;
anywheresoftware.b4a.objects.B4XCanvas _c = null;
anywheresoftware.b4a.objects.B4XViewWrapper _xview = null;
anywheresoftware.b4a.objects.B4XCanvas.B4XPath _path = null;
anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper _res = null;
 //BA.debugLineNum = 329;BA.debugLine="Sub CreateRoundBitmap (Input As B4XBitmap, Size As";
 //BA.debugLineNum = 330;BA.debugLine="If Input.Width <> Input.Height Then";
if (_input.getWidth()!=_input.getHeight()) { 
 //BA.debugLineNum = 332;BA.debugLine="Dim l As Int = Min(Input.Width, Input.Height)";
_l = (int) (anywheresoftware.b4a.keywords.Common.Min(_input.getWidth(),_input.getHeight()));
 //BA.debugLineNum = 333;BA.debugLine="Input = Input.Crop(Input.Width / 2 - l / 2, Inpu";
_input = _input.Crop((int) (_input.getWidth()/(double)2-_l/(double)2),(int) (_input.getHeight()/(double)2-_l/(double)2),_l,_l);
 };
 //BA.debugLineNum = 335;BA.debugLine="Dim c As B4XCanvas";
_c = new anywheresoftware.b4a.objects.B4XCanvas();
 //BA.debugLineNum = 336;BA.debugLine="Dim xview As B4XView = xui.CreatePanel(\"\")";
_xview = new anywheresoftware.b4a.objects.B4XViewWrapper();
_xview = mostCurrent._xui.CreatePanel(processBA,"");
 //BA.debugLineNum = 337;BA.debugLine="xview.SetLayoutAnimated(0, 0, 0, Size, Size)";
_xview.SetLayoutAnimated((int) (0),(int) (0),(int) (0),_size,_size);
 //BA.debugLineNum = 338;BA.debugLine="c.Initialize(xview)";
_c.Initialize(_xview);
 //BA.debugLineNum = 339;BA.debugLine="Dim path As B4XPath";
_path = new anywheresoftware.b4a.objects.B4XCanvas.B4XPath();
 //BA.debugLineNum = 340;BA.debugLine="path.InitializeOval(c.TargetRect)";
_path.InitializeOval(_c.getTargetRect());
 //BA.debugLineNum = 341;BA.debugLine="c.ClipPath(path)";
_c.ClipPath(_path);
 //BA.debugLineNum = 342;BA.debugLine="c.DrawBitmap(Input.Resize(Size, Size, False), c.T";
_c.DrawBitmap((android.graphics.Bitmap)(_input.Resize(_size,_size,anywheresoftware.b4a.keywords.Common.False).getObject()),_c.getTargetRect());
 //BA.debugLineNum = 343;BA.debugLine="c.RemoveClip";
_c.RemoveClip();
 //BA.debugLineNum = 344;BA.debugLine="c.DrawCircle(c.TargetRect.CenterX, c.TargetRect.C";
_c.DrawCircle(_c.getTargetRect().getCenterX(),_c.getTargetRect().getCenterY(),(float) (_c.getTargetRect().getWidth()/(double)2-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (2))),mostCurrent._xui.Color_White,anywheresoftware.b4a.keywords.Common.False,(float) (anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (5))));
 //BA.debugLineNum = 345;BA.debugLine="c.Invalidate";
_c.Invalidate();
 //BA.debugLineNum = 346;BA.debugLine="Dim res As B4XBitmap = c.CreateBitmap";
_res = new anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper();
_res = _c.CreateBitmap();
 //BA.debugLineNum = 347;BA.debugLine="c.Release";
_c.Release();
 //BA.debugLineNum = 348;BA.debugLine="Return res";
if (true) return _res;
 //BA.debugLineNum = 349;BA.debugLine="End Sub";
return null;
}
public static String  _customlisallcontacts_itemclick(int _index,Object _value) throws Exception{
anywheresoftware.b4a.objects.B4XViewWrapper _pn = null;
 //BA.debugLineNum = 390;BA.debugLine="Sub CustomLisAllContacts_ItemClick (Index As Int,";
 //BA.debugLineNum = 391;BA.debugLine="If(Index <> listvieExraitam Or listvieExraitam ==";
if ((_index!=_listvieexraitam || _listvieexraitam==-1)) { 
 //BA.debugLineNum = 392;BA.debugLine="If(listvieExraitam == -1) Then";
if ((_listvieexraitam==-1)) { 
 //BA.debugLineNum = 393;BA.debugLine="listvieExraitam = 0";
_listvieexraitam = (int) (0);
 };
 //BA.debugLineNum = 395;BA.debugLine="Dim pn As B4XView = xui.CreatePanel(\"\")";
_pn = new anywheresoftware.b4a.objects.B4XViewWrapper();
_pn = mostCurrent._xui.CreatePanel(processBA,"");
 //BA.debugLineNum = 396;BA.debugLine="pn.SetLayoutAnimated(300,0,0,CustomLisAllContact";
_pn.SetLayoutAnimated((int) (300),(int) (0),(int) (0),mostCurrent._customlisallcontacts._asview().getWidth(),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (40)));
 //BA.debugLineNum = 397;BA.debugLine="pn.LoadLayout(\"CellItemClickPage2\")";
_pn.LoadLayout("CellItemClickPage2",mostCurrent.activityBA);
 //BA.debugLineNum = 398;BA.debugLine="PanelFriendPerson.Tag = Value";
mostCurrent._panelfriendperson.setTag(_value);
 //BA.debugLineNum = 399;BA.debugLine="If(listvieExraitam <> 0) Then";
if ((_listvieexraitam!=0)) { 
 //BA.debugLineNum = 400;BA.debugLine="CustomLisAllContacts.RemoveAt(listvieExraitam)";
mostCurrent._customlisallcontacts._removeat(_listvieexraitam);
 };
 //BA.debugLineNum = 404;BA.debugLine="If(Index == CustomLisAllContacts.Size) Then";
if ((_index==mostCurrent._customlisallcontacts._getsize())) { 
 //BA.debugLineNum = 405;BA.debugLine="CustomLisAllContacts.Add(pn,Value)";
mostCurrent._customlisallcontacts._add(_pn,_value);
 //BA.debugLineNum = 406;BA.debugLine="listvieExraitam = Index";
_listvieexraitam = _index;
 }else {
 //BA.debugLineNum = 408;BA.debugLine="CustomLisAllContacts.InsertAt(Index+1,pn,Value)";
mostCurrent._customlisallcontacts._insertat((int) (_index+1),_pn,_value);
 //BA.debugLineNum = 409;BA.debugLine="listvieExraitam = Index + 1";
_listvieexraitam = (int) (_index+1);
 };
 };
 //BA.debugLineNum = 412;BA.debugLine="End Sub";
return "";
}
public static String  _customlisallmessages_itemclick(int _index,Object _value) throws Exception{
int _i = 0;
de.donmanfred.DataSnapshotWrapper _ds = null;
String _m1 = "";
String _m2 = "";
 //BA.debugLineNum = 649;BA.debugLine="Sub CustomLisAllMessages_ItemClick (Index As Int,";
 //BA.debugLineNum = 650;BA.debugLine="MessageService.AllMessages.Clear";
mostCurrent._messageservice._allmessages /*anywheresoftware.b4a.objects.collections.List*/ .Clear();
 //BA.debugLineNum = 651;BA.debugLine="For i =0 To RomeService.AllRooms.Size-1";
{
final int step2 = 1;
final int limit2 = (int) (mostCurrent._romeservice._allrooms /*anywheresoftware.b4a.objects.collections.List*/ .getSize()-1);
_i = (int) (0) ;
for (;_i <= limit2 ;_i = _i + step2 ) {
 //BA.debugLineNum = 652;BA.debugLine="Dim Ds As DataSnapshot";
_ds = new de.donmanfred.DataSnapshotWrapper();
 //BA.debugLineNum = 653;BA.debugLine="Ds.Initialize(RomeService.AllRooms.Get(i))";
_ds.Initialize(processBA,(com.google.firebase.database.DataSnapshot)(mostCurrent._romeservice._allrooms /*anywheresoftware.b4a.objects.collections.List*/ .Get(_i)));
 //BA.debugLineNum = 654;BA.debugLine="Dim m1,m2 As String";
_m1 = "";
_m2 = "";
 //BA.debugLineNum = 655;BA.debugLine="m1 = Ds.getChild(\"Members\").getChild(\"Member1\").";
_m1 = BA.ObjectToString(_ds.getChild("Members").getChild("Member1").getValue());
 //BA.debugLineNum = 656;BA.debugLine="m2 = Ds.getChild(\"Members\").getChild(\"Member2\").";
_m2 = BA.ObjectToString(_ds.getChild("Members").getChild("Member2").getValue());
 //BA.debugLineNum = 657;BA.debugLine="If(UseService.UserName == m1 Or UseService.UserN";
if (((mostCurrent._useservice._username /*String*/ ).equals(_m1) || (mostCurrent._useservice._username /*String*/ ).equals(_m2))) { 
 //BA.debugLineNum = 658;BA.debugLine="If(Value == m1 Or Value== m2) Then";
if (((_value).equals((Object)(_m1)) || (_value).equals((Object)(_m2)))) { 
 //BA.debugLineNum = 659;BA.debugLine="If (Not(MessageService.ReceiverMember == Value";
if ((anywheresoftware.b4a.keywords.Common.Not((mostCurrent._messageservice._receivermember /*String*/ ).equals(BA.ObjectToString(_value))) || (anywheresoftware.b4a.keywords.Common.Not(mostCurrent._messageservice._refmessage /*de.donmanfred.DatabaseReferenceWrapper*/ .IsInitialized())))) { 
 //BA.debugLineNum = 660;BA.debugLine="MessageService.Objest = Ds.Key";
mostCurrent._messageservice._objest /*Object*/  = (Object)(_ds.getKey());
 //BA.debugLineNum = 661;BA.debugLine="CallSubDelayed(MessageService,\"Inti\")";
anywheresoftware.b4a.keywords.Common.CallSubDelayed(processBA,(Object)(mostCurrent._messageservice.getObject()),"Inti");
 //BA.debugLineNum = 662;BA.debugLine="Log(\"find\")";
anywheresoftware.b4a.keywords.Common.LogImpl("02818061","find",0);
 };
 //BA.debugLineNum = 665;BA.debugLine="MessageService.SenderMember = UseService.UserN";
mostCurrent._messageservice._sendermember /*String*/  = mostCurrent._useservice._username /*String*/ ;
 //BA.debugLineNum = 666;BA.debugLine="MessageService.ReceiverMember = Value";
mostCurrent._messageservice._receivermember /*String*/  = BA.ObjectToString(_value);
 };
 };
 }
};
 //BA.debugLineNum = 670;BA.debugLine="PanChat.SetLayoutAnimated(400,0,0,Activity.Width,";
mostCurrent._panchat.SetLayoutAnimated((int) (400),(int) (0),(int) (0),mostCurrent._activity.getWidth(),mostCurrent._activity.getHeight());
 //BA.debugLineNum = 671;BA.debugLine="ChatState = True";
_chatstate = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 672;BA.debugLine="End Sub";
return "";
}
public static String  _customlisallusers_itemclick(int _index,Object _value) throws Exception{
anywheresoftware.b4a.objects.B4XViewWrapper _pn = null;
 //BA.debugLineNum = 366;BA.debugLine="Sub CustomLisAllUsers_ItemClick (Index As Int, Val";
 //BA.debugLineNum = 367;BA.debugLine="If(Index <> listvieExraitam Or listvieExraitam ==";
if ((_index!=_listvieexraitam || _listvieexraitam==-1)) { 
 //BA.debugLineNum = 368;BA.debugLine="If(listvieExraitam == -1) Then";
if ((_listvieexraitam==-1)) { 
 //BA.debugLineNum = 369;BA.debugLine="listvieExraitam = 0";
_listvieexraitam = (int) (0);
 };
 //BA.debugLineNum = 371;BA.debugLine="Dim pn As B4XView = xui.CreatePanel(\"\")";
_pn = new anywheresoftware.b4a.objects.B4XViewWrapper();
_pn = mostCurrent._xui.CreatePanel(processBA,"");
 //BA.debugLineNum = 372;BA.debugLine="pn.SetLayoutAnimated(300,0,0,CustomLisAllUsers.As";
_pn.SetLayoutAnimated((int) (300),(int) (0),(int) (0),mostCurrent._customlisallusers._asview().getWidth(),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (40)));
 //BA.debugLineNum = 373;BA.debugLine="pn.LoadLayout(\"CellitemClicked\")";
_pn.LoadLayout("CellitemClicked",mostCurrent.activityBA);
 //BA.debugLineNum = 374;BA.debugLine="PanelAddPErson.Tag = Value";
mostCurrent._paneladdperson.setTag(_value);
 //BA.debugLineNum = 375;BA.debugLine="If(listvieExraitam <> 0) Then";
if ((_listvieexraitam!=0)) { 
 //BA.debugLineNum = 376;BA.debugLine="CustomLisAllUsers.RemoveAt(listvieExraitam)";
mostCurrent._customlisallusers._removeat(_listvieexraitam);
 };
 //BA.debugLineNum = 380;BA.debugLine="If(Index == CustomLisAllUsers.Size) Then";
if ((_index==mostCurrent._customlisallusers._getsize())) { 
 //BA.debugLineNum = 381;BA.debugLine="CustomLisAllUsers.Add(pn,Value)";
mostCurrent._customlisallusers._add(_pn,_value);
 //BA.debugLineNum = 382;BA.debugLine="listvieExraitam = Index";
_listvieexraitam = _index;
 }else {
 //BA.debugLineNum = 384;BA.debugLine="CustomLisAllUsers.InsertAt(Index+1,pn,Value)";
mostCurrent._customlisallusers._insertat((int) (_index+1),_pn,_value);
 //BA.debugLineNum = 385;BA.debugLine="listvieExraitam = Index + 1";
_listvieexraitam = (int) (_index+1);
 };
 };
 //BA.debugLineNum = 388;BA.debugLine="End Sub";
return "";
}
public static String  _dowloadfriendsprofpic() throws Exception{
 //BA.debugLineNum = 799;BA.debugLine="Sub DowloadFriendsProfPic";
 //BA.debugLineNum = 800;BA.debugLine="Storage.DownloadProfilFriendsImage(AllFreindspic)";
mostCurrent._storage._downloadprofilfriendsimage /*void*/ (mostCurrent._allfreindspic);
 //BA.debugLineNum = 801;BA.debugLine="End Sub";
return "";
}
public static String  _dowloadpersonsprofpic() throws Exception{
 //BA.debugLineNum = 795;BA.debugLine="Sub DowloadPersonsProfPic";
 //BA.debugLineNum = 796;BA.debugLine="Storage.DownloadProfilPersonImage(AllPersonPic";
mostCurrent._storage._downloadprofilpersonimage /*void*/ (mostCurrent._allpersonpic);
 //BA.debugLineNum = 797;BA.debugLine="End Sub";
return "";
}
public static void  _downloadimagesapp() throws Exception{
ResumableSub_DownloadImagesApp rsub = new ResumableSub_DownloadImagesApp(null);
rsub.resume(processBA, null);
}
public static class ResumableSub_DownloadImagesApp extends BA.ResumableSub {
public ResumableSub_DownloadImagesApp(b4a.example.fbrtdb.lobby parent) {
this.parent = parent;
}
b4a.example.fbrtdb.lobby parent;

@Override
public void resume(BA ba, Object[] result) throws Exception{

    while (true) {
        switch (state) {
            case -1:
return;

case 0:
//C
this.state = -1;
 //BA.debugLineNum = 716;BA.debugLine="Storage.DownloadImage(\"/public/2835be38b5274a4b20";
parent.mostCurrent._storage._downloadimage /*void*/ ("/public/2835be38b5274a4b20155999a7613542.jpg",parent.mostCurrent._panlback,"PanlBack.jpg");
 //BA.debugLineNum = 717;BA.debugLine="Sleep(3000)";
anywheresoftware.b4a.keywords.Common.Sleep(mostCurrent.activityBA,this,(int) (3000));
this.state = 1;
return;
case 1:
//C
this.state = -1;
;
 //BA.debugLineNum = 718;BA.debugLine="PanlBack.Resize(ImavCoverr.Width,ImavCoverr.Heigh";
parent.mostCurrent._panlback.Resize((float) (parent.mostCurrent._imavcoverr.getWidth()),(float) (parent.mostCurrent._imavcoverr.getHeight()),anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 719;BA.debugLine="ImavCoverr.Bitmap = PanlBack";
parent.mostCurrent._imavcoverr.setBitmap((android.graphics.Bitmap)(parent.mostCurrent._panlback.getObject()));
 //BA.debugLineNum = 720;BA.debugLine="Storage.DownloadImage(\"/public/unknownimage.png\",";
parent.mostCurrent._storage._downloadimage /*void*/ ("/public/unknownimage.png",parent.mostCurrent._profileback,"ProfilBack.jpg");
 //BA.debugLineNum = 721;BA.debugLine="Sleep(3000)";
anywheresoftware.b4a.keywords.Common.Sleep(mostCurrent.activityBA,this,(int) (3000));
this.state = 2;
return;
case 2:
//C
this.state = -1;
;
 //BA.debugLineNum = 722;BA.debugLine="ProfileBack.Resize(ImageUserProfile2.Width,ImageU";
parent.mostCurrent._profileback.Resize((float) (parent.mostCurrent._imageuserprofile2.getWidth()),(float) (parent.mostCurrent._imageuserprofile2.getHeight()),anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 723;BA.debugLine="ImageUserProfile2.Bitmap = ProfileBack";
parent.mostCurrent._imageuserprofile2.setBitmap((android.graphics.Bitmap)(parent.mostCurrent._profileback.getObject()));
 //BA.debugLineNum = 725;BA.debugLine="End Sub";
if (true) break;

            }
        }
    }
}
public static String  _friendprofilepicset(anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _picc,int _i) throws Exception{
anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper _pic = null;
anywheresoftware.b4a.objects.B4XViewWrapper _pnluser = null;
anywheresoftware.b4a.objects.B4XViewWrapper _imv = null;
 //BA.debugLineNum = 785;BA.debugLine="Public Sub FriendProfilePicSet(picc As Bitmap,i As";
 //BA.debugLineNum = 786;BA.debugLine="Dim Pic As B4XBitmap = picc";
_pic = new anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper();
_pic = (anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper(), (android.graphics.Bitmap)(_picc.getObject()));
 //BA.debugLineNum = 788;BA.debugLine="Dim pnluser As B4XView";
_pnluser = new anywheresoftware.b4a.objects.B4XViewWrapper();
 //BA.debugLineNum = 789;BA.debugLine="pnluser = CustomLisAllContacts.GetPanel(i)";
_pnluser = mostCurrent._customlisallcontacts._getpanel(_i);
 //BA.debugLineNum = 790;BA.debugLine="Dim imv As B4XView = pnluser.GetView(0).GetView(1";
_imv = new anywheresoftware.b4a.objects.B4XViewWrapper();
_imv = _pnluser.GetView((int) (0)).GetView((int) (1));
 //BA.debugLineNum = 792;BA.debugLine="imv.SetBitmap(CreateRoundBitmap(Pic,imv.Width))";
_imv.SetBitmap((android.graphics.Bitmap)(_createroundbitmap(_pic,_imv.getWidth()).getObject()));
 //BA.debugLineNum = 793;BA.debugLine="End Sub";
return "";
}
public static String  _globals() throws Exception{
 //BA.debugLineNum = 16;BA.debugLine="Sub Globals";
 //BA.debugLineNum = 19;BA.debugLine="Dim Pnlprofile As Panel";
mostCurrent._pnlprofile = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 20;BA.debugLine="Dim xui As XUI";
mostCurrent._xui = new anywheresoftware.b4a.objects.B4XViewWrapper.XUI();
 //BA.debugLineNum = 21;BA.debugLine="Dim AllUsers As List";
mostCurrent._allusers = new anywheresoftware.b4a.objects.collections.List();
 //BA.debugLineNum = 23;BA.debugLine="Dim listvieExraitam As Int";
_listvieexraitam = 0;
 //BA.debugLineNum = 24;BA.debugLine="Public chat1 As Chat";
mostCurrent._chat1 = new b4a.example.fbrtdb.chat();
 //BA.debugLineNum = 25;BA.debugLine="Private ime As IME";
mostCurrent._ime = new anywheresoftware.b4a.objects.IME();
 //BA.debugLineNum = 26;BA.debugLine="Public Storage As FirebasStorage";
mostCurrent._storage = new b4a.example.fbrtdb.firebasstorage();
 //BA.debugLineNum = 27;BA.debugLine="Dim PanChat As Panel";
mostCurrent._panchat = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 28;BA.debugLine="Dim RedCircle As Bitmap";
mostCurrent._redcircle = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper();
 //BA.debugLineNum = 29;BA.debugLine="Dim BlckCircle As Bitmap";
mostCurrent._blckcircle = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper();
 //BA.debugLineNum = 30;BA.debugLine="Dim ProfilePic As Bitmap";
mostCurrent._profilepic = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper();
 //BA.debugLineNum = 31;BA.debugLine="Dim PeronProfilePic As Bitmap";
mostCurrent._peronprofilepic = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper();
 //BA.debugLineNum = 32;BA.debugLine="Dim AllPersonPic As List";
mostCurrent._allpersonpic = new anywheresoftware.b4a.objects.collections.List();
 //BA.debugLineNum = 33;BA.debugLine="Dim AllFreindspic As List";
mostCurrent._allfreindspic = new anywheresoftware.b4a.objects.collections.List();
 //BA.debugLineNum = 35;BA.debugLine="Private TabStrip1 As TabStrip";
mostCurrent._tabstrip1 = new anywheresoftware.b4a.objects.TabStripViewPager();
 //BA.debugLineNum = 36;BA.debugLine="Private LabUserName As Label";
mostCurrent._labusername = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 37;BA.debugLine="Private LabOptions As Label";
mostCurrent._laboptions = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 38;BA.debugLine="Private ImavUProfile As B4XView";
mostCurrent._imavuprofile = new anywheresoftware.b4a.objects.B4XViewWrapper();
 //BA.debugLineNum = 39;BA.debugLine="Private LabProfClose As Label";
mostCurrent._labprofclose = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 40;BA.debugLine="Private CustomLisAllUsers As CustomListView";
mostCurrent._customlisallusers = new b4a.example3.customlistview();
 //BA.debugLineNum = 41;BA.debugLine="Private ImageUserProfile2 As ImageView";
mostCurrent._imageuserprofile2 = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 43;BA.debugLine="Private Labeldeletelistite As Label";
mostCurrent._labeldeletelistite = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 44;BA.debugLine="Private LabePersonstatus As Label";
mostCurrent._labepersonstatus = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 45;BA.debugLine="Private LabePersonName As Label";
mostCurrent._labepersonname = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 46;BA.debugLine="Private LabelAddPerson As Label";
mostCurrent._labeladdperson = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 47;BA.debugLine="Private PanelAddPErson As Panel";
mostCurrent._paneladdperson = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 48;BA.debugLine="Private CustomLisAllContacts As CustomListView";
mostCurrent._customlisallcontacts = new b4a.example3.customlistview();
 //BA.debugLineNum = 49;BA.debugLine="Private PanelFriendPerson As Panel";
mostCurrent._panelfriendperson = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 51;BA.debugLine="Private LabeRemoveFriend As Label";
mostCurrent._laberemovefriend = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 52;BA.debugLine="Private Labeldeletelistite2 As Label";
mostCurrent._labeldeletelistite2 = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 53;BA.debugLine="Private LabeChatFriend As Label";
mostCurrent._labechatfriend = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 54;BA.debugLine="Private CustomLisAllMessages As CustomListView";
mostCurrent._customlisallmessages = new b4a.example3.customlistview();
 //BA.debugLineNum = 55;BA.debugLine="Private ImageVMessageProfil As ImageView";
mostCurrent._imagevmessageprofil = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 56;BA.debugLine="Private LabelMessageName As Label";
mostCurrent._labelmessagename = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 57;BA.debugLine="Private LabRefresh As Label";
mostCurrent._labrefresh = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 58;BA.debugLine="Private LabMessageNumbers As Label";
mostCurrent._labmessagenumbers = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 59;BA.debugLine="Private PaneMessage As Panel";
mostCurrent._panemessage = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 60;BA.debugLine="Private ImageViOnlineStatue As ImageView";
mostCurrent._imagevionlinestatue = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 61;BA.debugLine="Private ImavCoverr As ImageView";
mostCurrent._imavcoverr = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 63;BA.debugLine="Dim rp As RuntimePermissions";
mostCurrent._rp = new anywheresoftware.b4a.objects.RuntimePermissions();
 //BA.debugLineNum = 64;BA.debugLine="Dim Rs As RSPopupMenu";
mostCurrent._rs = new com.rootsoft.rspopupmenu.RSPopupMenu();
 //BA.debugLineNum = 65;BA.debugLine="Private dialog As B4XDialog";
mostCurrent._dialog = new b4a.example.fbrtdb.b4xdialog();
 //BA.debugLineNum = 67;BA.debugLine="Dim ChatBack As Bitmap";
mostCurrent._chatback = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper();
 //BA.debugLineNum = 68;BA.debugLine="Dim PanlBack As Bitmap";
mostCurrent._panlback = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper();
 //BA.debugLineNum = 69;BA.debugLine="Dim ProfileBack As Bitmap";
mostCurrent._profileback = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper();
 //BA.debugLineNum = 71;BA.debugLine="Private PaneProfilPerson As Panel";
mostCurrent._paneprofilperson = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 73;BA.debugLine="Private ImageVRofilePicture As B4XView";
mostCurrent._imagevrofilepicture = new anywheresoftware.b4a.objects.B4XViewWrapper();
 //BA.debugLineNum = 74;BA.debugLine="Private LabUserStatue As Label";
mostCurrent._labuserstatue = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 75;BA.debugLine="End Sub";
return "";
}
public static String  _imagevrofilepicture_click() throws Exception{
anywheresoftware.b4a.objects.ImageViewWrapper _imageprofil = null;
anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _pic = null;
String _picurl = "";
anywheresoftware.b4a.objects.PanelWrapper _pn = null;
anywheresoftware.b4a.objects.LabelWrapper _labname = null;
anywheresoftware.b4a.objects.LabelWrapper _labstate = null;
 //BA.debugLineNum = 803;BA.debugLine="Sub ImageVRofilePicture_Click";
 //BA.debugLineNum = 804;BA.debugLine="Dim imageProfil As ImageView = Sender";
_imageprofil = new anywheresoftware.b4a.objects.ImageViewWrapper();
_imageprofil = (anywheresoftware.b4a.objects.ImageViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ImageViewWrapper(), (android.widget.ImageView)(anywheresoftware.b4a.keywords.Common.Sender(mostCurrent.activityBA)));
 //BA.debugLineNum = 805;BA.debugLine="Dim pic As Bitmap";
_pic = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper();
 //BA.debugLineNum = 806;BA.debugLine="Dim picurl As String = imageProfil.Tag";
_picurl = BA.ObjectToString(_imageprofil.getTag());
 //BA.debugLineNum = 807;BA.debugLine="If(picurl <> \"\") Then";
if (((_picurl).equals("") == false)) { 
 //BA.debugLineNum = 808;BA.debugLine="Pnlprofile.SetLayoutAnimated(0,imageProfil.Left,i";
mostCurrent._pnlprofile.SetLayoutAnimated((int) (0),_imageprofil.getLeft(),_imageprofil.getHeight(),(int) (0),(int) (0));
 //BA.debugLineNum = 809;BA.debugLine="Pnlprofile.SetLayoutAnimated(300,50dip,50dip,70%x";
mostCurrent._pnlprofile.SetLayoutAnimated((int) (300),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (50)),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (50)),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (70),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (70),mostCurrent.activityBA));
 //BA.debugLineNum = 810;BA.debugLine="Storage.DownloadImage(imageProfil.Tag,pic,picurl.";
mostCurrent._storage._downloadimage /*void*/ (BA.ObjectToString(_imageprofil.getTag()),_pic,_picurl.substring((int) (6)));
 //BA.debugLineNum = 811;BA.debugLine="ImageUserProfile2.Bitmap = pic";
mostCurrent._imageuserprofile2.setBitmap((android.graphics.Bitmap)(_pic.getObject()));
 };
 //BA.debugLineNum = 814;BA.debugLine="Dim pn As Panel = imageProfil.Parent";
_pn = new anywheresoftware.b4a.objects.PanelWrapper();
_pn = (anywheresoftware.b4a.objects.PanelWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.PanelWrapper(), (android.view.ViewGroup)(_imageprofil.getParent()));
 //BA.debugLineNum = 815;BA.debugLine="Dim labname As Label = pn.GetView(0)";
_labname = new anywheresoftware.b4a.objects.LabelWrapper();
_labname = (anywheresoftware.b4a.objects.LabelWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.LabelWrapper(), (android.widget.TextView)(_pn.GetView((int) (0)).getObject()));
 //BA.debugLineNum = 816;BA.debugLine="Dim labState As Label = pn.GetView(2)";
_labstate = new anywheresoftware.b4a.objects.LabelWrapper();
_labstate = (anywheresoftware.b4a.objects.LabelWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.LabelWrapper(), (android.widget.TextView)(_pn.GetView((int) (2)).getObject()));
 //BA.debugLineNum = 817;BA.debugLine="LabUserName.Text = labname.Text";
mostCurrent._labusername.setText(BA.ObjectToCharSequence(_labname.getText()));
 //BA.debugLineNum = 818;BA.debugLine="LabUserStatue.Text = labState.Text";
mostCurrent._labuserstatue.setText(BA.ObjectToCharSequence(_labstate.getText()));
 //BA.debugLineNum = 819;BA.debugLine="End Sub";
return "";
}
public static String  _imavuprofile_click() throws Exception{
 //BA.debugLineNum = 351;BA.debugLine="Sub ImavUProfile_Click";
 //BA.debugLineNum = 352;BA.debugLine="Pnlprofile.SetLayoutAnimated(300,50dip,50dip,70%x";
mostCurrent._pnlprofile.SetLayoutAnimated((int) (300),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (50)),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (50)),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (70),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (70),mostCurrent.activityBA));
 //BA.debugLineNum = 353;BA.debugLine="ProfilePic.Resize(ImageUserProfile2.Width,ImageUs";
mostCurrent._profilepic.Resize((float) (mostCurrent._imageuserprofile2.getWidth()),(float) (mostCurrent._imageuserprofile2.getHeight()),anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 355;BA.debugLine="ImageUserProfile2.Bitmap = ProfilePic";
mostCurrent._imageuserprofile2.setBitmap((android.graphics.Bitmap)(mostCurrent._profilepic.getObject()));
 //BA.debugLineNum = 357;BA.debugLine="LabUserName.Text = Main.UserId";
mostCurrent._labusername.setText(BA.ObjectToCharSequence(mostCurrent._main._userid /*String*/ ));
 //BA.debugLineNum = 358;BA.debugLine="LabUserStatue.Text = UseService.Status";
mostCurrent._labuserstatue.setText(BA.ObjectToCharSequence(mostCurrent._useservice._status /*String*/ ));
 //BA.debugLineNum = 359;BA.debugLine="End Sub";
return "";
}
public static String  _insializchatvoice(String _dir,String _filname) throws Exception{
 //BA.debugLineNum = 615;BA.debugLine="Public Sub insializChatvoice(Dir As String,FilName";
 //BA.debugLineNum = 616;BA.debugLine="chat1.Dirvoice = Dir";
mostCurrent._chat1._dirvoice /*String*/  = _dir;
 //BA.debugLineNum = 617;BA.debugLine="chat1.FileNameVoice = FilName";
mostCurrent._chat1._filenamevoice /*String*/  = _filname;
 //BA.debugLineNum = 618;BA.debugLine="End Sub";
return "";
}
public static void  _labechatfriend_click() throws Exception{
ResumableSub_LabeChatFriend_Click rsub = new ResumableSub_LabeChatFriend_Click(null);
rsub.resume(processBA, null);
}
public static class ResumableSub_LabeChatFriend_Click extends BA.ResumableSub {
public ResumableSub_LabeChatFriend_Click(b4a.example.fbrtdb.lobby parent) {
this.parent = parent;
}
b4a.example.fbrtdb.lobby parent;
int _i = 0;
de.donmanfred.DataSnapshotWrapper _ds = null;
String _m1 = "";
String _m2 = "";
int step2;
int limit2;

@Override
public void resume(BA ba, Object[] result) throws Exception{

    while (true) {
        switch (state) {
            case -1:
return;

case 0:
//C
this.state = 1;
 //BA.debugLineNum = 520;BA.debugLine="MessageService.AllMessages.Clear";
parent.mostCurrent._messageservice._allmessages /*anywheresoftware.b4a.objects.collections.List*/ .Clear();
 //BA.debugLineNum = 521;BA.debugLine="For i =0 To RomeService.AllRooms.Size-1";
if (true) break;

case 1:
//for
this.state = 16;
step2 = 1;
limit2 = (int) (parent.mostCurrent._romeservice._allrooms /*anywheresoftware.b4a.objects.collections.List*/ .getSize()-1);
_i = (int) (0) ;
this.state = 17;
if (true) break;

case 17:
//C
this.state = 16;
if ((step2 > 0 && _i <= limit2) || (step2 < 0 && _i >= limit2)) this.state = 3;
if (true) break;

case 18:
//C
this.state = 17;
_i = ((int)(0 + _i + step2)) ;
if (true) break;

case 3:
//C
this.state = 4;
 //BA.debugLineNum = 522;BA.debugLine="Dim Ds As DataSnapshot";
_ds = new de.donmanfred.DataSnapshotWrapper();
 //BA.debugLineNum = 523;BA.debugLine="Ds.Initialize(RomeService.AllRooms.Get(i))";
_ds.Initialize(processBA,(com.google.firebase.database.DataSnapshot)(parent.mostCurrent._romeservice._allrooms /*anywheresoftware.b4a.objects.collections.List*/ .Get(_i)));
 //BA.debugLineNum = 524;BA.debugLine="Dim m1,m2 As String";
_m1 = "";
_m2 = "";
 //BA.debugLineNum = 525;BA.debugLine="m1 = Ds.getChild(\"Members\").getChild(\"Member1\").";
_m1 = BA.ObjectToString(_ds.getChild("Members").getChild("Member1").getValue());
 //BA.debugLineNum = 526;BA.debugLine="m2 = Ds.getChild(\"Members\").getChild(\"Member2\").";
_m2 = BA.ObjectToString(_ds.getChild("Members").getChild("Member2").getValue());
 //BA.debugLineNum = 527;BA.debugLine="If(UseService.UserName == m1 Or UseService.UserN";
if (true) break;

case 4:
//if
this.state = 15;
if (((parent.mostCurrent._useservice._username /*String*/ ).equals(_m1) || (parent.mostCurrent._useservice._username /*String*/ ).equals(_m2))) { 
this.state = 6;
}if (true) break;

case 6:
//C
this.state = 7;
 //BA.debugLineNum = 528;BA.debugLine="If(PanelFriendPerson.Tag == m1 Or PanelFriendPe";
if (true) break;

case 7:
//if
this.state = 14;
if (((parent.mostCurrent._panelfriendperson.getTag()).equals((Object)(_m1)) || (parent.mostCurrent._panelfriendperson.getTag()).equals((Object)(_m2)))) { 
this.state = 9;
}if (true) break;

case 9:
//C
this.state = 10;
 //BA.debugLineNum = 529;BA.debugLine="MessageService.Objest = Ds.Key";
parent.mostCurrent._messageservice._objest /*Object*/  = (Object)(_ds.getKey());
 //BA.debugLineNum = 530;BA.debugLine="If (Not(MessageService.ReceiverMember == Panel";
if (true) break;

case 10:
//if
this.state = 13;
if ((anywheresoftware.b4a.keywords.Common.Not((parent.mostCurrent._messageservice._receivermember /*String*/ ).equals(BA.ObjectToString(parent.mostCurrent._panelfriendperson.getTag()))) || (anywheresoftware.b4a.keywords.Common.Not(parent.mostCurrent._messageservice._refmessage /*de.donmanfred.DatabaseReferenceWrapper*/ .IsInitialized())))) { 
this.state = 12;
}if (true) break;

case 12:
//C
this.state = 13;
 //BA.debugLineNum = 531;BA.debugLine="CallSubDelayed(MessageService,\"Inti\")";
anywheresoftware.b4a.keywords.Common.CallSubDelayed(processBA,(Object)(parent.mostCurrent._messageservice.getObject()),"Inti");
 //BA.debugLineNum = 532;BA.debugLine="Log(\"find\")";
anywheresoftware.b4a.keywords.Common.LogImpl("02162701","find",0);
 if (true) break;

case 13:
//C
this.state = 14;
;
 //BA.debugLineNum = 535;BA.debugLine="MessageService.SenderMember = UseService.UserN";
parent.mostCurrent._messageservice._sendermember /*String*/  = parent.mostCurrent._useservice._username /*String*/ ;
 //BA.debugLineNum = 536;BA.debugLine="MessageService.ReceiverMember = PanelFriendPer";
parent.mostCurrent._messageservice._receivermember /*String*/  = BA.ObjectToString(parent.mostCurrent._panelfriendperson.getTag());
 if (true) break;

case 14:
//C
this.state = 15;
;
 if (true) break;

case 15:
//C
this.state = 18;
;
 if (true) break;
if (true) break;

case 16:
//C
this.state = -1;
;
 //BA.debugLineNum = 541;BA.debugLine="PanChat.SetLayoutAnimated(300,0,0,Activity.Width,";
parent.mostCurrent._panchat.SetLayoutAnimated((int) (300),(int) (0),(int) (0),parent.mostCurrent._activity.getWidth(),parent.mostCurrent._activity.getHeight());
 //BA.debugLineNum = 543;BA.debugLine="Sleep(3000)";
anywheresoftware.b4a.keywords.Common.Sleep(mostCurrent.activityBA,this,(int) (3000));
this.state = 19;
return;
case 19:
//C
this.state = -1;
;
 //BA.debugLineNum = 544;BA.debugLine="CallSub(MessageService,\"prossesMessage\")";
anywheresoftware.b4a.keywords.Common.CallSubNew(processBA,(Object)(parent.mostCurrent._messageservice.getObject()),"prossesMessage");
 //BA.debugLineNum = 546;BA.debugLine="ChatState = True";
parent._chatstate = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 547;BA.debugLine="End Sub";
if (true) break;

            }
        }
    }
}
public static String  _labeladdperson_click() throws Exception{
String _personname = "";
anywheresoftware.b4a.objects.collections.Map _map = null;
boolean _ishave = false;
int _i = 0;
de.donmanfred.DataSnapshotWrapper _snlist = null;
de.donmanfred.DatabaseReferenceWrapper _refadd = null;
 //BA.debugLineNum = 424;BA.debugLine="Sub LabelAddPerson_Click";
 //BA.debugLineNum = 425;BA.debugLine="Dim PersonName As String = PanelAddPErson.Tag";
_personname = BA.ObjectToString(mostCurrent._paneladdperson.getTag());
 //BA.debugLineNum = 426;BA.debugLine="Dim map As Map";
_map = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 427;BA.debugLine="map.Initialize";
_map.Initialize();
 //BA.debugLineNum = 428;BA.debugLine="map.Put(\"UserName\",PersonName)";
_map.Put((Object)("UserName"),(Object)(_personname));
 //BA.debugLineNum = 430;BA.debugLine="Dim Ishave As Boolean = False";
_ishave = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 431;BA.debugLine="For i=0 To UseService.Contact.Size-1";
{
final int step6 = 1;
final int limit6 = (int) (mostCurrent._useservice._contact /*anywheresoftware.b4a.objects.collections.List*/ .getSize()-1);
_i = (int) (0) ;
for (;_i <= limit6 ;_i = _i + step6 ) {
 //BA.debugLineNum = 432;BA.debugLine="Dim snlist As DataSnapshot";
_snlist = new de.donmanfred.DataSnapshotWrapper();
 //BA.debugLineNum = 433;BA.debugLine="snlist.Initialize(UseService.Contact.Get(i))";
_snlist.Initialize(processBA,(com.google.firebase.database.DataSnapshot)(mostCurrent._useservice._contact /*anywheresoftware.b4a.objects.collections.List*/ .Get(_i)));
 //BA.debugLineNum = 434;BA.debugLine="If(snlist.getChild(\"UserName\").Value == PersonNam";
if (((_snlist.getChild("UserName").getValue()).equals((Object)(_personname)))) { 
 //BA.debugLineNum = 435;BA.debugLine="Ishave = True";
_ishave = anywheresoftware.b4a.keywords.Common.True;
 };
 }
};
 //BA.debugLineNum = 439;BA.debugLine="If(Ishave) Then";
if ((_ishave)) { 
 //BA.debugLineNum = 440;BA.debugLine="ToastMessageShow(\"You have \"&PersonName&\" in frie";
anywheresoftware.b4a.keywords.Common.ToastMessageShow(BA.ObjectToCharSequence("You have "+_personname+" in friend list"),anywheresoftware.b4a.keywords.Common.True);
 }else {
 //BA.debugLineNum = 442;BA.debugLine="UseService.refContact.push.setValue(map,\"Contact\"";
mostCurrent._useservice._refcontact /*de.donmanfred.DatabaseReferenceWrapper*/ .push().setValue(processBA,_map,"Contact","");
 //BA.debugLineNum = 444;BA.debugLine="For i = 0 To AllUsers.Size-1 ' Add me to person c";
{
final int step17 = 1;
final int limit17 = (int) (mostCurrent._allusers.getSize()-1);
_i = (int) (0) ;
for (;_i <= limit17 ;_i = _i + step17 ) {
 //BA.debugLineNum = 445;BA.debugLine="Dim snlist As DataSnapshot";
_snlist = new de.donmanfred.DataSnapshotWrapper();
 //BA.debugLineNum = 446;BA.debugLine="snlist.Initialize(AllUsers.Get(i))";
_snlist.Initialize(processBA,(com.google.firebase.database.DataSnapshot)(mostCurrent._allusers.Get(_i)));
 //BA.debugLineNum = 447;BA.debugLine="If(snlist.getChild(\"UserName\").Value == PersonN";
if (((_snlist.getChild("UserName").getValue()).equals((Object)(_personname)))) { 
 //BA.debugLineNum = 448;BA.debugLine="Dim refAdd As DatabaseReference";
_refadd = new de.donmanfred.DatabaseReferenceWrapper();
 //BA.debugLineNum = 449;BA.debugLine="refAdd.Initialize(\"RefAdd\",Main.realtime.getRe";
_refadd.Initialize(processBA,"RefAdd",(com.google.firebase.database.DatabaseReference)(mostCurrent._main._realtime /*de.donmanfred.FirebaseDatabaseWrapper*/ .getReferencefromUrl("https://first-project-5317d.firebaseio.com/Users/"+_snlist.getKey()+"/Contact").getObject()),(Object)("User"));
 //BA.debugLineNum = 450;BA.debugLine="refAdd.push.setValue(CreateMap(\"UserName\":UseS";
_refadd.push().setValue(processBA,anywheresoftware.b4a.keywords.Common.createMap(new Object[] {(Object)("UserName"),(Object)(mostCurrent._useservice._username /*String*/ )}),"Contact","");
 };
 }
};
 //BA.debugLineNum = 455;BA.debugLine="Log(RomeService.AllRooms.Size)";
anywheresoftware.b4a.keywords.Common.LogImpl("02031647",BA.NumberToString(mostCurrent._romeservice._allrooms /*anywheresoftware.b4a.objects.collections.List*/ .getSize()),0);
 //BA.debugLineNum = 456;BA.debugLine="RomeService.refRoom.push.setValue(CreateMap(\"Name";
mostCurrent._romeservice._refroom /*de.donmanfred.DatabaseReferenceWrapper*/ .push().setValue(processBA,anywheresoftware.b4a.keywords.Common.createMap(new Object[] {(Object)("Name"),(Object)("."),(Object)("Messages"),(Object)(""),(Object)("Members"),(Object)(anywheresoftware.b4a.keywords.Common.createMap(new Object[] {(Object)("Member1"),(Object)(mostCurrent._useservice._username /*String*/ ),(Object)("Member2"),(Object)(_personname)}).getObject())}),"Rooms","Roo");
 };
 //BA.debugLineNum = 458;BA.debugLine="End Sub";
return "";
}
public static String  _labeldeletelistite_click() throws Exception{
 //BA.debugLineNum = 414;BA.debugLine="Sub Labeldeletelistite_Click";
 //BA.debugLineNum = 415;BA.debugLine="CustomLisAllUsers.RemoveAt(listvieExraitam)";
mostCurrent._customlisallusers._removeat(_listvieexraitam);
 //BA.debugLineNum = 416;BA.debugLine="listvieExraitam = -1";
_listvieexraitam = (int) (-1);
 //BA.debugLineNum = 417;BA.debugLine="End Sub";
return "";
}
public static String  _labeldeletelistite2_click() throws Exception{
 //BA.debugLineNum = 419;BA.debugLine="Sub Labeldeletelistite2_Click";
 //BA.debugLineNum = 420;BA.debugLine="CustomLisAllContacts.RemoveAt(listvieExraitam)";
mostCurrent._customlisallcontacts._removeat(_listvieexraitam);
 //BA.debugLineNum = 421;BA.debugLine="listvieExraitam = -1";
_listvieexraitam = (int) (-1);
 //BA.debugLineNum = 422;BA.debugLine="End Sub";
return "";
}
public static String  _laberemovefriend_click() throws Exception{
String _personname = "";
int _itemindex = 0;
int _i = 0;
de.donmanfred.DataSnapshotWrapper _snlist = null;
int _k = 0;
de.donmanfred.DataSnapshotWrapper _sn = null;
de.donmanfred.DatabaseReferenceWrapper _refremove = null;
anywheresoftware.b4a.objects.collections.List _lischild = null;
int _j = 0;
de.donmanfred.DataSnapshotWrapper _schild = null;
String _person1 = "";
String _person2 = "";
 //BA.debugLineNum = 460;BA.debugLine="Sub LabeRemoveFriend_Click";
 //BA.debugLineNum = 462;BA.debugLine="Dim PersonName As String = PanelFriendPerson.Tag";
_personname = BA.ObjectToString(mostCurrent._panelfriendperson.getTag());
 //BA.debugLineNum = 463;BA.debugLine="Dim ItemIndex As Int";
_itemindex = 0;
 //BA.debugLineNum = 464;BA.debugLine="For i=0 To UseService.Contact.Size-1";
{
final int step3 = 1;
final int limit3 = (int) (mostCurrent._useservice._contact /*anywheresoftware.b4a.objects.collections.List*/ .getSize()-1);
_i = (int) (0) ;
for (;_i <= limit3 ;_i = _i + step3 ) {
 //BA.debugLineNum = 465;BA.debugLine="Dim snlist As DataSnapshot";
_snlist = new de.donmanfred.DataSnapshotWrapper();
 //BA.debugLineNum = 466;BA.debugLine="snlist.Initialize(UseService.Contact.Get(i))";
_snlist.Initialize(processBA,(com.google.firebase.database.DataSnapshot)(mostCurrent._useservice._contact /*anywheresoftware.b4a.objects.collections.List*/ .Get(_i)));
 //BA.debugLineNum = 467;BA.debugLine="If(snlist.getChild(\"UserName\").Value == PersonNa";
if (((_snlist.getChild("UserName").getValue()).equals((Object)(_personname)))) { 
 //BA.debugLineNum = 468;BA.debugLine="ItemIndex = i";
_itemindex = _i;
 //BA.debugLineNum = 469;BA.debugLine="UseService.refContact.Child(snlist.Key).removeV";
mostCurrent._useservice._refcontact /*de.donmanfred.DatabaseReferenceWrapper*/ .Child(_snlist.getKey()).removeValue();
 //BA.debugLineNum = 471;BA.debugLine="For k = 0 To AllUsers.Size-1 ' Remove me to per";
{
final int step9 = 1;
final int limit9 = (int) (mostCurrent._allusers.getSize()-1);
_k = (int) (0) ;
for (;_k <= limit9 ;_k = _k + step9 ) {
 //BA.debugLineNum = 472;BA.debugLine="Dim sn As DataSnapshot";
_sn = new de.donmanfred.DataSnapshotWrapper();
 //BA.debugLineNum = 473;BA.debugLine="sn.Initialize(AllUsers.Get(k))";
_sn.Initialize(processBA,(com.google.firebase.database.DataSnapshot)(mostCurrent._allusers.Get(_k)));
 //BA.debugLineNum = 474;BA.debugLine="If(sn.getChild(\"UserName\").Value == PersonName";
if (((_sn.getChild("UserName").getValue()).equals((Object)(_personname)))) { 
 //BA.debugLineNum = 475;BA.debugLine="Dim refRemove As DatabaseReference";
_refremove = new de.donmanfred.DatabaseReferenceWrapper();
 //BA.debugLineNum = 476;BA.debugLine="refRemove.Initialize(\"RefRemove\",Main.realtim";
_refremove.Initialize(processBA,"RefRemove",(com.google.firebase.database.DatabaseReference)(mostCurrent._main._realtime /*de.donmanfred.FirebaseDatabaseWrapper*/ .getReferencefromUrl("https://first-project-5317d.firebaseio.com/Users/"+_sn.getKey()+"/Contact").getObject()),(Object)("personCont"));
 //BA.debugLineNum = 477;BA.debugLine="Dim lisChild As List";
_lischild = new anywheresoftware.b4a.objects.collections.List();
 //BA.debugLineNum = 478;BA.debugLine="lisChild = sn.getChild(\"Contact\").Children";
_lischild = _sn.getChild("Contact").getChildren();
 //BA.debugLineNum = 479;BA.debugLine="Log(lisChild.Size)";
anywheresoftware.b4a.keywords.Common.LogImpl("02097171",BA.NumberToString(_lischild.getSize()),0);
 //BA.debugLineNum = 480;BA.debugLine="For j =0 To lisChild.Size-1";
{
final int step18 = 1;
final int limit18 = (int) (_lischild.getSize()-1);
_j = (int) (0) ;
for (;_j <= limit18 ;_j = _j + step18 ) {
 //BA.debugLineNum = 481;BA.debugLine="Dim schild As DataSnapshot";
_schild = new de.donmanfred.DataSnapshotWrapper();
 //BA.debugLineNum = 482;BA.debugLine="schild= lisChild.Get(j)";
_schild = (de.donmanfred.DataSnapshotWrapper)(_lischild.Get(_j));
 //BA.debugLineNum = 483;BA.debugLine="If(schild.getChild(\"UserName\").Value == UseS";
if (((_schild.getChild("UserName").getValue()).equals((Object)(mostCurrent._useservice._username /*String*/ )))) { 
 //BA.debugLineNum = 484;BA.debugLine="refRemove.Child(schild.Key).removeValue";
_refremove.Child(_schild.getKey()).removeValue();
 };
 }
};
 };
 }
};
 };
 }
};
 //BA.debugLineNum = 497;BA.debugLine="Dim person1 As String = PersonName";
_person1 = _personname;
 //BA.debugLineNum = 498;BA.debugLine="Dim person2 As String = UseService.UserName";
_person2 = mostCurrent._useservice._username /*String*/ ;
 //BA.debugLineNum = 499;BA.debugLine="For k = 0 To RomeService.AllRooms.Size-1";
{
final int step31 = 1;
final int limit31 = (int) (mostCurrent._romeservice._allrooms /*anywheresoftware.b4a.objects.collections.List*/ .getSize()-1);
_k = (int) (0) ;
for (;_k <= limit31 ;_k = _k + step31 ) {
 //BA.debugLineNum = 500;BA.debugLine="Dim sn As DataSnapshot";
_sn = new de.donmanfred.DataSnapshotWrapper();
 //BA.debugLineNum = 501;BA.debugLine="sn.Initialize(RomeService.AllRooms.Get(k))";
_sn.Initialize(processBA,(com.google.firebase.database.DataSnapshot)(mostCurrent._romeservice._allrooms /*anywheresoftware.b4a.objects.collections.List*/ .Get(_k)));
 //BA.debugLineNum = 502;BA.debugLine="If(sn.getChild(\"Members\").getChild(\"Member1\").Va";
if (((_sn.getChild("Members").getChild("Member1").getValue()).equals((Object)(_person1)) || (_sn.getChild("Members").getChild("Member2").getValue()).equals((Object)(_person1)))) { 
 //BA.debugLineNum = 503;BA.debugLine="If(sn.getChild(\"Members\").getChild(\"Member1\").V";
if (((_sn.getChild("Members").getChild("Member1").getValue()).equals((Object)(_person2)) || (_sn.getChild("Members").getChild("Member2").getValue()).equals((Object)(_person2)))) { 
 //BA.debugLineNum = 504;BA.debugLine="RomeService.refRoom.Child(sn.Key).removeValue";
mostCurrent._romeservice._refroom /*de.donmanfred.DatabaseReferenceWrapper*/ .Child(_sn.getKey()).removeValue();
 //BA.debugLineNum = 505;BA.debugLine="RomeService.AllRooms.RemoveAt(k)";
mostCurrent._romeservice._allrooms /*anywheresoftware.b4a.objects.collections.List*/ .RemoveAt(_k);
 };
 };
 }
};
 //BA.debugLineNum = 510;BA.debugLine="UseService.Contact.RemoveAt(ItemIndex)";
mostCurrent._useservice._contact /*anywheresoftware.b4a.objects.collections.List*/ .RemoveAt(_itemindex);
 //BA.debugLineNum = 511;BA.debugLine="CustomLisAllContacts.RemoveAt(ItemIndex+1)";
mostCurrent._customlisallcontacts._removeat((int) (_itemindex+1));
 //BA.debugLineNum = 512;BA.debugLine="CustomLisAllContacts.RemoveAt(ItemIndex)";
mostCurrent._customlisallcontacts._removeat(_itemindex);
 //BA.debugLineNum = 514;BA.debugLine="CustomLisAllMessages.RemoveAt(ItemIndex)";
mostCurrent._customlisallmessages._removeat(_itemindex);
 //BA.debugLineNum = 515;BA.debugLine="AllPersonPic.RemoveAt(ItemIndex)";
mostCurrent._allpersonpic.RemoveAt(_itemindex);
 //BA.debugLineNum = 516;BA.debugLine="End Sub";
return "";
}
public static String  _laboptions_click() throws Exception{
 //BA.debugLineNum = 853;BA.debugLine="Sub LabOptions_Click";
 //BA.debugLineNum = 854;BA.debugLine="Rs.Show";
mostCurrent._rs.Show();
 //BA.debugLineNum = 855;BA.debugLine="End Sub";
return "";
}
public static String  _labprofclose_click() throws Exception{
 //BA.debugLineNum = 361;BA.debugLine="Sub LabProfClose_Click";
 //BA.debugLineNum = 362;BA.debugLine="Pnlprofile.SetLayoutAnimated(300,ImavUProfile.Lef";
mostCurrent._pnlprofile.SetLayoutAnimated((int) (300),mostCurrent._imavuprofile.getLeft(),mostCurrent._imavuprofile.getHeight(),(int) (0),(int) (0));
 //BA.debugLineNum = 364;BA.debugLine="End Sub";
return "";
}
public static String  _labrefresh_click() throws Exception{
 //BA.debugLineNum = 644;BA.debugLine="Sub LabRefresh_Click";
 //BA.debugLineNum = 646;BA.debugLine="End Sub";
return "";
}
public static String  _personprofilepicset(anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _picc,int _i) throws Exception{
anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper _pic = null;
anywheresoftware.b4a.objects.B4XViewWrapper _pnluser = null;
anywheresoftware.b4a.objects.B4XViewWrapper _imv = null;
 //BA.debugLineNum = 766;BA.debugLine="Public Sub PersonProfilePicSet(picc As Bitmap,i As";
 //BA.debugLineNum = 767;BA.debugLine="Dim Pic As B4XBitmap = picc";
_pic = new anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper();
_pic = (anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper(), (android.graphics.Bitmap)(_picc.getObject()));
 //BA.debugLineNum = 769;BA.debugLine="Dim pnluser As B4XView";
_pnluser = new anywheresoftware.b4a.objects.B4XViewWrapper();
 //BA.debugLineNum = 770;BA.debugLine="pnluser = CustomLisAllUsers.GetPanel(i)";
_pnluser = mostCurrent._customlisallusers._getpanel(_i);
 //BA.debugLineNum = 771;BA.debugLine="Dim imv As B4XView = pnluser.GetView(0).GetView(1";
_imv = new anywheresoftware.b4a.objects.B4XViewWrapper();
_imv = _pnluser.GetView((int) (0)).GetView((int) (1));
 //BA.debugLineNum = 773;BA.debugLine="imv.SetBitmap(CreateRoundBitmap(Pic,imv.Width))";
_imv.SetBitmap((android.graphics.Bitmap)(_createroundbitmap(_pic,_imv.getWidth()).getObject()));
 //BA.debugLineNum = 783;BA.debugLine="End Sub";
return "";
}
public static String  _popupmenu_dismiss() throws Exception{
 //BA.debugLineNum = 823;BA.debugLine="Sub PopupMenu_Dismiss";
 //BA.debugLineNum = 825;BA.debugLine="End Sub";
return "";
}
public static boolean  _popupmenu_menuitemclick(int _itemid) throws Exception{
 //BA.debugLineNum = 827;BA.debugLine="Sub PopupMenu_MenuItemClick (ItemId As Int) As Boo";
 //BA.debugLineNum = 829;BA.debugLine="If(ItemId == 0) Then";
if ((_itemid==0)) { 
 //BA.debugLineNum = 830;BA.debugLine="Pnlprofile.SetLayoutAnimated(300,50dip,50dip,70%";
mostCurrent._pnlprofile.SetLayoutAnimated((int) (300),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (50)),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (50)),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (70),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (70),mostCurrent.activityBA));
 //BA.debugLineNum = 831;BA.debugLine="ProfilePic.Resize(ImageUserProfile2.Width,ImageU";
mostCurrent._profilepic.Resize((float) (mostCurrent._imageuserprofile2.getWidth()),(float) (mostCurrent._imageuserprofile2.getHeight()),anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 833;BA.debugLine="ImageUserProfile2.Bitmap = ProfilePic";
mostCurrent._imageuserprofile2.setBitmap((android.graphics.Bitmap)(mostCurrent._profilepic.getObject()));
 }else if((_itemid==1)) { 
 //BA.debugLineNum = 835;BA.debugLine="ChangeState";
_changestate();
 };
 //BA.debugLineNum = 837;BA.debugLine="Return False";
if (true) return anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 838;BA.debugLine="End Sub";
return false;
}
public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 6;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 9;BA.debugLine="Public RefUser As DatabaseReference";
_refuser = new de.donmanfred.DatabaseReferenceWrapper();
 //BA.debugLineNum = 11;BA.debugLine="Dim Chosser As ContentChooser";
_chosser = new anywheresoftware.b4a.phone.Phone.ContentChooser();
 //BA.debugLineNum = 13;BA.debugLine="Public ChatState As Boolean = False";
_chatstate = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 14;BA.debugLine="End Sub";
return "";
}
public static String  _recivemessagecustoview(String _m,boolean _isr) throws Exception{
boolean _ismessage = false;
boolean _isphoto = false;
 //BA.debugLineNum = 557;BA.debugLine="Public Sub ReciveMessageCustoview(M As String,isr";
 //BA.debugLineNum = 558;BA.debugLine="Dim isMessage As Boolean";
_ismessage = false;
 //BA.debugLineNum = 559;BA.debugLine="If(M.CharAt(0) == \"/\") Then";
if ((_m.charAt((int) (0))==BA.ObjectToChar("/"))) { 
 //BA.debugLineNum = 560;BA.debugLine="isMessage = False";
_ismessage = anywheresoftware.b4a.keywords.Common.False;
 }else {
 //BA.debugLineNum = 562;BA.debugLine="isMessage = True";
_ismessage = anywheresoftware.b4a.keywords.Common.True;
 };
 //BA.debugLineNum = 564;BA.debugLine="If(isMessage) Then";
if ((_ismessage)) { 
 //BA.debugLineNum = 565;BA.debugLine="chat1.AddItem(M,isr,MessageService.ReceiverMe";
mostCurrent._chat1._additem /*String*/ (_m,_isr,mostCurrent._messageservice._receivermember /*String*/ );
 }else {
 //BA.debugLineNum = 567;BA.debugLine="Dim IsPhoto As Boolean = True";
_isphoto = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 568;BA.debugLine="If(M.CharAt(M.Length-1) == \"v\") Then";
if ((_m.charAt((int) (_m.length()-1))==BA.ObjectToChar("v"))) { 
 //BA.debugLineNum = 569;BA.debugLine="IsPhoto = False";
_isphoto = anywheresoftware.b4a.keywords.Common.False;
 };
 //BA.debugLineNum = 572;BA.debugLine="If(IsPhoto) Then";
if ((_isphoto)) { 
 //BA.debugLineNum = 573;BA.debugLine="Storage.DownloadImageCover(M,M,isr,chat1.chooser";
mostCurrent._storage._downloadimagecover /*void*/ (_m,_m,_isr,mostCurrent._chat1._chooserbitmap /*anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper*/ );
 }else {
 //BA.debugLineNum = 575;BA.debugLine="Storage.DownloadVoiceCover(M,M,isr)";
mostCurrent._storage._downloadvoicecover /*void*/ (_m,_m,_isr);
 };
 };
 //BA.debugLineNum = 580;BA.debugLine="End Sub";
return "";
}
public static String  _refuser2_onchildadded(Object _snapshot,String _child,Object _tag) throws Exception{
de.donmanfred.DataSnapshotWrapper _sn = null;
String _personname = "";
String _status = "";
String _profpic = "";
 //BA.debugLineNum = 248;BA.debugLine="Sub RefUser2_onChildAdded(Snapshot As Object, chil";
 //BA.debugLineNum = 249;BA.debugLine="Dim sn As DataSnapshot";
_sn = new de.donmanfred.DataSnapshotWrapper();
 //BA.debugLineNum = 250;BA.debugLine="sn.Initialize(Snapshot)";
_sn.Initialize(processBA,(com.google.firebase.database.DataSnapshot)(_snapshot));
 //BA.debugLineNum = 251;BA.debugLine="If(UseService.UserName <> sn.getChild(\"UserName\")";
if (((mostCurrent._useservice._username /*String*/ ).equals(BA.ObjectToString(_sn.getChild("UserName").getValue())) == false)) { 
 //BA.debugLineNum = 252;BA.debugLine="AllUsers.Add(Snapshot)'Add one User to the list";
mostCurrent._allusers.Add(_snapshot);
 //BA.debugLineNum = 254;BA.debugLine="Dim PersonName As String = sn.getChild(\"UserName\"";
_personname = BA.ObjectToString(_sn.getChild("UserName").getValue());
 //BA.debugLineNum = 255;BA.debugLine="Dim Status As String = sn.getChild(\"Status\").Valu";
_status = BA.ObjectToString(_sn.getChild("Status").getValue());
 //BA.debugLineNum = 256;BA.debugLine="Dim ProfPic As String = sn.getChild(\"ProfilPhotoU";
_profpic = BA.ObjectToString(_sn.getChild("ProfilPhotoURL").getValue());
 //BA.debugLineNum = 257;BA.debugLine="AddScrollviewItem(PersonName,PersonName,Status,Pr";
_addscrollviewitem(_personname,_personname,_status,_profpic);
 };
 //BA.debugLineNum = 259;BA.debugLine="End Sub";
return "";
}
public static String  _refuser2_onchildchanged(Object _snapshot,String _child,Object _tag) throws Exception{
de.donmanfred.DataSnapshotWrapper _sn = null;
int _i = 0;
de.donmanfred.DataSnapshotWrapper _snuser = null;
boolean _ison = false;
int _k = 0;
anywheresoftware.b4a.objects.ImageViewWrapper _img = null;
 //BA.debugLineNum = 278;BA.debugLine="Sub RefUser2_onChildChanged(Snapshot As Object, ch";
 //BA.debugLineNum = 279;BA.debugLine="Dim Sn As DataSnapshot";
_sn = new de.donmanfred.DataSnapshotWrapper();
 //BA.debugLineNum = 280;BA.debugLine="Sn.Initialize(Snapshot)";
_sn.Initialize(processBA,(com.google.firebase.database.DataSnapshot)(_snapshot));
 //BA.debugLineNum = 295;BA.debugLine="For i =0 To AllUsers.Size-1";
{
final int step3 = 1;
final int limit3 = (int) (mostCurrent._allusers.getSize()-1);
_i = (int) (0) ;
for (;_i <= limit3 ;_i = _i + step3 ) {
 //BA.debugLineNum = 296;BA.debugLine="Dim Snuser As DataSnapshot";
_snuser = new de.donmanfred.DataSnapshotWrapper();
 //BA.debugLineNum = 297;BA.debugLine="Snuser.Initialize(AllUsers.Get(i))";
_snuser.Initialize(processBA,(com.google.firebase.database.DataSnapshot)(mostCurrent._allusers.Get(_i)));
 //BA.debugLineNum = 298;BA.debugLine="If(Sn.Key == Snuser.Key) Then";
if (((_sn.getKey()).equals(_snuser.getKey()))) { 
 //BA.debugLineNum = 299;BA.debugLine="AllUsers.Set(i,Snapshot)";
mostCurrent._allusers.Set(_i,_snapshot);
 //BA.debugLineNum = 301;BA.debugLine="If(Snuser.getChild(\"UserName\").Value <> UseServ";
if (((_snuser.getChild("UserName").getValue()).equals((Object)(mostCurrent._useservice._username /*String*/ )) == false)) { 
 //BA.debugLineNum = 302;BA.debugLine="Dim ison As Boolean = Sn.getChild(\"IsOnline\").";
_ison = BA.ObjectToBoolean(_sn.getChild("IsOnline").getValue());
 //BA.debugLineNum = 303;BA.debugLine="For k =0 To CustomLisAllContacts.Size-1";
{
final int step10 = 1;
final int limit10 = (int) (mostCurrent._customlisallcontacts._getsize()-1);
_k = (int) (0) ;
for (;_k <= limit10 ;_k = _k + step10 ) {
 //BA.debugLineNum = 304;BA.debugLine="If(CustomLisAllContacts.GetValue(k) == Snuser";
if (((mostCurrent._customlisallcontacts._getvalue(_k)).equals(_snuser.getChild("UserName").getValue()))) { 
 //BA.debugLineNum = 305;BA.debugLine="Dim img As ImageView = CustomLisAllContacts.";
_img = new anywheresoftware.b4a.objects.ImageViewWrapper();
_img = (anywheresoftware.b4a.objects.ImageViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ImageViewWrapper(), (android.widget.ImageView)(mostCurrent._customlisallcontacts._getpanel(_k).GetView((int) (0)).GetView((int) (3)).getObject()));
 //BA.debugLineNum = 306;BA.debugLine="If(ison) Then";
if ((_ison)) { 
 //BA.debugLineNum = 307;BA.debugLine="img.Bitmap = RedCircle";
_img.setBitmap((android.graphics.Bitmap)(mostCurrent._redcircle.getObject()));
 }else {
 //BA.debugLineNum = 309;BA.debugLine="img.Bitmap = BlckCircle";
_img.setBitmap((android.graphics.Bitmap)(mostCurrent._blckcircle.getObject()));
 };
 };
 }
};
 };
 //BA.debugLineNum = 322;BA.debugLine="UpdateTabeFriend";
_updatetabefriend();
 };
 }
};
 //BA.debugLineNum = 327;BA.debugLine="End Sub";
return "";
}
public static String  _refuser2_onchildremoved(Object _snapshot,Object _tag) throws Exception{
de.donmanfred.DataSnapshotWrapper _sn = null;
int _itemindex = 0;
int _i = 0;
de.donmanfred.DataSnapshotWrapper _snlist = null;
 //BA.debugLineNum = 261;BA.debugLine="Sub RefUser2_onChildRemoved(Snapshot As Object, ta";
 //BA.debugLineNum = 262;BA.debugLine="Dim sn As DataSnapshot";
_sn = new de.donmanfred.DataSnapshotWrapper();
 //BA.debugLineNum = 263;BA.debugLine="sn.Initialize(Snapshot)";
_sn.Initialize(processBA,(com.google.firebase.database.DataSnapshot)(_snapshot));
 //BA.debugLineNum = 265;BA.debugLine="Dim ItemIndex As Int";
_itemindex = 0;
 //BA.debugLineNum = 266;BA.debugLine="For i=0 To AllUsers.Size-1";
{
final int step4 = 1;
final int limit4 = (int) (mostCurrent._allusers.getSize()-1);
_i = (int) (0) ;
for (;_i <= limit4 ;_i = _i + step4 ) {
 //BA.debugLineNum = 267;BA.debugLine="Dim snlist As DataSnapshot";
_snlist = new de.donmanfred.DataSnapshotWrapper();
 //BA.debugLineNum = 268;BA.debugLine="snlist.Initialize(AllUsers.Get(i))";
_snlist.Initialize(processBA,(com.google.firebase.database.DataSnapshot)(mostCurrent._allusers.Get(_i)));
 //BA.debugLineNum = 269;BA.debugLine="If(snlist.getChild(\"UserName\").Value == sn.getCh";
if (((_snlist.getChild("UserName").getValue()).equals(_sn.getChild("UserName").getValue()))) { 
 //BA.debugLineNum = 270;BA.debugLine="ItemIndex = i";
_itemindex = _i;
 };
 }
};
 //BA.debugLineNum = 274;BA.debugLine="AllUsers.RemoveAt(ItemIndex)";
mostCurrent._allusers.RemoveAt(_itemindex);
 //BA.debugLineNum = 275;BA.debugLine="CustomLisAllUsers.RemoveAt(ItemIndex)";
mostCurrent._customlisallusers._removeat(_itemindex);
 //BA.debugLineNum = 276;BA.debugLine="End Sub";
return "";
}
public static String  _sentimage(String _url) throws Exception{
 //BA.debugLineNum = 746;BA.debugLine="Public Sub sentimage(Url As String)";
 //BA.debugLineNum = 753;BA.debugLine="RefUser.Child(UseService.childd).updateChildren";
_refuser.Child(BA.ObjectToString(mostCurrent._useservice._childd /*Object*/ )).updateChildren(processBA,anywheresoftware.b4a.keywords.Common.createMap(new Object[] {(Object)("ProfilPhotoURL"),(Object)(_url)}));
 //BA.debugLineNum = 754;BA.debugLine="UseService.ProfilPhotoUrl = Url";
mostCurrent._useservice._profilphotourl /*String*/  = _url;
 //BA.debugLineNum = 755;BA.debugLine="Storage.DownloadProfilImage(UseService.Pr";
mostCurrent._storage._downloadprofilimage /*void*/ (mostCurrent._useservice._profilphotourl /*String*/ ,mostCurrent._profilepic,mostCurrent._useservice._profilphotourl /*String*/ .substring((int) (6)),anywheresoftware.b4a.keywords.Common.False,(int) (0));
 //BA.debugLineNum = 758;BA.debugLine="End Sub";
return "";
}
public static String  _sentimagetodatabase(String _filname) throws Exception{
 //BA.debugLineNum = 620;BA.debugLine="Public Sub SentImageToDataBase(FilName As String)";
 //BA.debugLineNum = 621;BA.debugLine="chat1.Sent(\"/message/\"&FilName)";
mostCurrent._chat1._sent /*String*/ ("/message/"+_filname);
 //BA.debugLineNum = 622;BA.debugLine="End Sub";
return "";
}
public static String  _sentmessagecustomview(String _m,boolean _isr) throws Exception{
boolean _ismessage = false;
boolean _isphoto = false;
 //BA.debugLineNum = 582;BA.debugLine="Public Sub SentMessageCustomview(M As String,isr A";
 //BA.debugLineNum = 583;BA.debugLine="Dim isMessage As Boolean";
_ismessage = false;
 //BA.debugLineNum = 584;BA.debugLine="If(M.CharAt(0) == \"/\") Then";
if ((_m.charAt((int) (0))==BA.ObjectToChar("/"))) { 
 //BA.debugLineNum = 585;BA.debugLine="isMessage = False";
_ismessage = anywheresoftware.b4a.keywords.Common.False;
 }else {
 //BA.debugLineNum = 587;BA.debugLine="isMessage = True";
_ismessage = anywheresoftware.b4a.keywords.Common.True;
 };
 //BA.debugLineNum = 590;BA.debugLine="If (isMessage) Then";
if ((_ismessage)) { 
 //BA.debugLineNum = 591;BA.debugLine="chat1.AddItem(M,isr,MessageService.SenderMemb";
mostCurrent._chat1._additem /*String*/ (_m,_isr,mostCurrent._messageservice._sendermember /*String*/ );
 }else {
 //BA.debugLineNum = 593;BA.debugLine="Dim IsPhoto As Boolean = True";
_isphoto = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 594;BA.debugLine="If(M.CharAt(M.Length-1) == \"v\") Then";
if ((_m.charAt((int) (_m.length()-1))==BA.ObjectToChar("v"))) { 
 //BA.debugLineNum = 595;BA.debugLine="IsPhoto = False";
_isphoto = anywheresoftware.b4a.keywords.Common.False;
 };
 //BA.debugLineNum = 598;BA.debugLine="If(IsPhoto) Then";
if ((_isphoto)) { 
 //BA.debugLineNum = 599;BA.debugLine="Storage.DownloadImageCover(M,M,isr,chat1.choose";
mostCurrent._storage._downloadimagecover /*void*/ (_m,_m,_isr,mostCurrent._chat1._chooserbitmap /*anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper*/ );
 }else {
 //BA.debugLineNum = 601;BA.debugLine="Storage.DownloadVoiceCover(M,M,isr)";
mostCurrent._storage._downloadvoicecover /*void*/ (_m,_m,_isr);
 };
 };
 //BA.debugLineNum = 605;BA.debugLine="End Sub";
return "";
}
public static String  _sentvoicetodatabase() throws Exception{
 //BA.debugLineNum = 624;BA.debugLine="Public Sub SentvoiceToDataBase";
 //BA.debugLineNum = 625;BA.debugLine="chat1.Sent(\"/message/\"&\"myRecording\"&chat1.AudioN";
mostCurrent._chat1._sent /*String*/ ("/message/"+"myRecording"+BA.NumberToString(mostCurrent._chat1._audionumber /*int*/ )+".wav");
 //BA.debugLineNum = 626;BA.debugLine="End Sub";
return "";
}
public static String  _tabstrip1_pageselected(int _position) throws Exception{
 //BA.debugLineNum = 180;BA.debugLine="Sub TabStrip1_PageSelected (Position As Int)";
 //BA.debugLineNum = 181;BA.debugLine="listvieExraitam = -1";
_listvieexraitam = (int) (-1);
 //BA.debugLineNum = 182;BA.debugLine="If(Position == 1) Then ' Upload all contact list";
if ((_position==1)) { 
 //BA.debugLineNum = 183;BA.debugLine="UpdateTabeFriend";
_updatetabefriend();
 };
 //BA.debugLineNum = 185;BA.debugLine="End Sub";
return "";
}
public static String  _updatetabefriend() throws Exception{
int _j = 0;
de.donmanfred.DataSnapshotWrapper _sn = null;
int _i = 0;
de.donmanfred.DataSnapshotWrapper _snn = null;
boolean _ison = false;
String _photourl = "";
 //BA.debugLineNum = 690;BA.debugLine="Sub UpdateTabeFriend";
 //BA.debugLineNum = 691;BA.debugLine="CustomLisAllContacts.Clear";
mostCurrent._customlisallcontacts._clear();
 //BA.debugLineNum = 692;BA.debugLine="AllFreindspic.Clear";
mostCurrent._allfreindspic.Clear();
 //BA.debugLineNum = 693;BA.debugLine="For j = 0 To UseService.Contact.Size -1";
{
final int step3 = 1;
final int limit3 = (int) (mostCurrent._useservice._contact /*anywheresoftware.b4a.objects.collections.List*/ .getSize()-1);
_j = (int) (0) ;
for (;_j <= limit3 ;_j = _j + step3 ) {
 //BA.debugLineNum = 694;BA.debugLine="Dim sn As DataSnapshot";
_sn = new de.donmanfred.DataSnapshotWrapper();
 //BA.debugLineNum = 695;BA.debugLine="sn.Initialize(UseService.Contact.Get(j))";
_sn.Initialize(processBA,(com.google.firebase.database.DataSnapshot)(mostCurrent._useservice._contact /*anywheresoftware.b4a.objects.collections.List*/ .Get(_j)));
 //BA.debugLineNum = 696;BA.debugLine="For i = 0 To AllUsers.Size -1";
{
final int step6 = 1;
final int limit6 = (int) (mostCurrent._allusers.getSize()-1);
_i = (int) (0) ;
for (;_i <= limit6 ;_i = _i + step6 ) {
 //BA.debugLineNum = 697;BA.debugLine="Dim snn As DataSnapshot";
_snn = new de.donmanfred.DataSnapshotWrapper();
 //BA.debugLineNum = 698;BA.debugLine="snn.Initialize(AllUsers.Get(i))";
_snn.Initialize(processBA,(com.google.firebase.database.DataSnapshot)(mostCurrent._allusers.Get(_i)));
 //BA.debugLineNum = 699;BA.debugLine="If(sn.getChild(\"UserName\").Value == snn.getChil";
if (((_sn.getChild("UserName").getValue()).equals(_snn.getChild("UserName").getValue()))) { 
 //BA.debugLineNum = 700;BA.debugLine="Dim ison As Boolean = snn.getChild(\"IsOnline\")";
_ison = BA.ObjectToBoolean(_snn.getChild("IsOnline").getValue());
 //BA.debugLineNum = 701;BA.debugLine="Dim photoUrl As String = snn.getChild(\"ProfilP";
_photourl = BA.ObjectToString(_snn.getChild("ProfilPhotoURL").getValue());
 //BA.debugLineNum = 702;BA.debugLine="AllFreindspic.Add(photoUrl)";
mostCurrent._allfreindspic.Add((Object)(_photourl));
 //BA.debugLineNum = 703;BA.debugLine="AddScrollviewItem2(sn.getChild(\"UserName\").Val";
_addscrollviewitem2(BA.ObjectToString(_sn.getChild("UserName").getValue()),BA.ObjectToString(_sn.getChild("UserName").getValue()),BA.ObjectToString(_snn.getChild("Status").getValue()),_ison,_photourl);
 };
 }
};
 }
};
 //BA.debugLineNum = 707;BA.debugLine="DowloadFriendsProfPic";
_dowloadfriendsprofpic();
 //BA.debugLineNum = 708;BA.debugLine="End Sub";
return "";
}
public static String  _userprofilepicset() throws Exception{
anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper _pic = null;
 //BA.debugLineNum = 760;BA.debugLine="Public Sub UserProfilePicSet";
 //BA.debugLineNum = 761;BA.debugLine="Dim Pic As B4XBitmap = ProfilePic";
_pic = new anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper();
_pic = (anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.B4XViewWrapper.B4XBitmapWrapper(), (android.graphics.Bitmap)(mostCurrent._profilepic.getObject()));
 //BA.debugLineNum = 762;BA.debugLine="Pic = CreateRoundBitmap(Pic,ImavUProfile.Width)";
_pic = _createroundbitmap(_pic,mostCurrent._imavuprofile.getWidth());
 //BA.debugLineNum = 763;BA.debugLine="ImavUProfile.SetBitmap(Pic)";
mostCurrent._imavuprofile.SetBitmap((android.graphics.Bitmap)(_pic.getObject()));
 //BA.debugLineNum = 764;BA.debugLine="End Sub";
return "";
}
}
