package b4a.example.fbrtdb;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.objects.ServiceHelper;
import anywheresoftware.b4a.debug.*;

public class useservice extends  android.app.Service{
	public static class useservice_BR extends android.content.BroadcastReceiver {

		@Override
		public void onReceive(android.content.Context context, android.content.Intent intent) {
            BA.LogInfo("** Receiver (useservice) OnReceive **");
			android.content.Intent in = new android.content.Intent(context, useservice.class);
			if (intent != null)
				in.putExtra("b4a_internal_intent", intent);
            ServiceHelper.StarterHelper.startServiceFromReceiver (context, in, false, BA.class);
		}

	}
    static useservice mostCurrent;
	public static BA processBA;
    private ServiceHelper _service;
    public static Class<?> getObject() {
		return useservice.class;
	}
	@Override
	public void onCreate() {
        super.onCreate();
        mostCurrent = this;
        if (processBA == null) {
		    processBA = new BA(this, null, null, "b4a.example.fbrtdb", "b4a.example.fbrtdb.useservice");
            if (BA.isShellModeRuntimeCheck(processBA)) {
                processBA.raiseEvent2(null, true, "SHELL", false);
		    }
            try {
                Class.forName(BA.applicationContext.getPackageName() + ".main").getMethod("initializeProcessGlobals").invoke(null, null);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            processBA.loadHtSubs(this.getClass());
            ServiceHelper.init();
        }
        _service = new ServiceHelper(this);
        processBA.service = this;
        
        if (BA.isShellModeRuntimeCheck(processBA)) {
			processBA.raiseEvent2(null, true, "CREATE", true, "b4a.example.fbrtdb.useservice", processBA, _service, anywheresoftware.b4a.keywords.Common.Density);
		}
        if (!false && ServiceHelper.StarterHelper.startFromServiceCreate(processBA, false) == false) {
				
		}
		else {
            processBA.setActivityPaused(false);
            BA.LogInfo("*** Service (useservice) Create ***");
            processBA.raiseEvent(null, "service_create");
        }
        processBA.runHook("oncreate", this, null);
        if (false) {
			ServiceHelper.StarterHelper.runWaitForLayouts();
		}
    }
		@Override
	public void onStart(android.content.Intent intent, int startId) {
		onStartCommand(intent, 0, 0);
    }
    @Override
    public int onStartCommand(final android.content.Intent intent, int flags, int startId) {
    	if (ServiceHelper.StarterHelper.onStartCommand(processBA, new Runnable() {
            public void run() {
                handleStart(intent);
            }}))
			;
		else {
			ServiceHelper.StarterHelper.addWaitForLayout (new Runnable() {
				public void run() {
                    processBA.setActivityPaused(false);
                    BA.LogInfo("** Service (useservice) Create **");
                    processBA.raiseEvent(null, "service_create");
					handleStart(intent);
                    ServiceHelper.StarterHelper.removeWaitForLayout();
				}
			});
		}
        processBA.runHook("onstartcommand", this, new Object[] {intent, flags, startId});
		return android.app.Service.START_NOT_STICKY;
    }
    public void onTaskRemoved(android.content.Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        if (false)
            processBA.raiseEvent(null, "service_taskremoved");
            
    }
    private void handleStart(android.content.Intent intent) {
    	BA.LogInfo("** Service (useservice) Start **");
    	java.lang.reflect.Method startEvent = processBA.htSubs.get("service_start");
    	if (startEvent != null) {
    		if (startEvent.getParameterTypes().length > 0) {
    			anywheresoftware.b4a.objects.IntentWrapper iw = ServiceHelper.StarterHelper.handleStartIntent(intent, _service, processBA);
    			processBA.raiseEvent(null, "service_start", iw);
    		}
    		else {
    			processBA.raiseEvent(null, "service_start");
    		}
    	}
    }
	
	@Override
	public void onDestroy() {
        super.onDestroy();
        if (false) {
            BA.LogInfo("** Service (useservice) Destroy (ignored)**");
        }
        else {
            BA.LogInfo("** Service (useservice) Destroy **");
		    processBA.raiseEvent(null, "service_destroy");
            processBA.service = null;
		    mostCurrent = null;
		    processBA.setActivityPaused(true);
            processBA.runHook("ondestroy", this, null);
        }
	}

@Override
	public android.os.IBinder onBind(android.content.Intent intent) {
		return null;
	}public anywheresoftware.b4a.keywords.Common __c = null;
public static String _username = "";
public static String _age = "";
public static anywheresoftware.b4a.objects.collections.List _contact = null;
public static boolean _isonline = false;
public static String _status = "";
public static String _profilphotourl = "";
public static de.donmanfred.DatabaseReferenceWrapper _refcontact = null;
public static Object _childd = null;
public b4a.example.dateutils _dateutils = null;
public b4a.example.fbrtdb.main _main = null;
public b4a.example.fbrtdb.lobby _lobby = null;
public b4a.example.fbrtdb.romeservice _romeservice = null;
public b4a.example.fbrtdb.messageservice _messageservice = null;
public b4a.example.fbrtdb.starter _starter = null;
public b4a.example.fbrtdb.httputils2service _httputils2service = null;
public b4a.example.fbrtdb.xuiviewsutils _xuiviewsutils = null;
public b4a.example.fbrtdb.b4xcollections _b4xcollections = null;
public static String  _inti() throws Exception{
 //BA.debugLineNum = 36;BA.debugLine="Public Sub Inti()";
 //BA.debugLineNum = 37;BA.debugLine="Service.StopAutomaticForeground 'Call this when t";
mostCurrent._service.StopAutomaticForeground();
 //BA.debugLineNum = 39;BA.debugLine="Contact.Initialize";
_contact.Initialize();
 //BA.debugLineNum = 41;BA.debugLine="refContact.Initialize(\"RefUserContact\",Main.realt";
_refcontact.Initialize(processBA,"RefUserContact",(com.google.firebase.database.DatabaseReference)(mostCurrent._main._realtime /*de.donmanfred.FirebaseDatabaseWrapper*/ .getReferencefromUrl("https://first-project-5317d.firebaseio.com/Users/"+BA.ObjectToString(_childd)+"/Contact").getObject()),(Object)("Contact"));
 //BA.debugLineNum = 42;BA.debugLine="refContact.addChildEventListener";
_refcontact.addChildEventListener();
 //BA.debugLineNum = 46;BA.debugLine="End Sub";
return "";
}
public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 6;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 9;BA.debugLine="Public UserName As String";
_username = "";
 //BA.debugLineNum = 10;BA.debugLine="Public Age As String";
_age = "";
 //BA.debugLineNum = 11;BA.debugLine="Public Contact As List";
_contact = new anywheresoftware.b4a.objects.collections.List();
 //BA.debugLineNum = 12;BA.debugLine="Public IsOnline As Boolean";
_isonline = false;
 //BA.debugLineNum = 13;BA.debugLine="Public Status As String";
_status = "";
 //BA.debugLineNum = 14;BA.debugLine="Public ProfilPhotoUrl As String";
_profilphotourl = "";
 //BA.debugLineNum = 16;BA.debugLine="Public refContact As DatabaseReference";
_refcontact = new de.donmanfred.DatabaseReferenceWrapper();
 //BA.debugLineNum = 18;BA.debugLine="Public childd As Object";
_childd = new Object();
 //BA.debugLineNum = 20;BA.debugLine="End Sub";
return "";
}
public static String  _refusercontact_onchildadded(Object _snapshot,String _child,Object _tag) throws Exception{
 //BA.debugLineNum = 49;BA.debugLine="Sub RefUserContact_onChildAdded(Snapshot As Object";
 //BA.debugLineNum = 50;BA.debugLine="Contact.Add(Snapshot)";
_contact.Add(_snapshot);
 //BA.debugLineNum = 51;BA.debugLine="Log(\"contact child added\")";
anywheresoftware.b4a.keywords.Common.LogImpl("06750210","contact child added",0);
 //BA.debugLineNum = 52;BA.debugLine="End Sub";
return "";
}
public static String  _refusercontact_onchildchanged(Object _snapshot,String _child,Object _tag) throws Exception{
 //BA.debugLineNum = 53;BA.debugLine="Sub RefUserContact_onChildChanged(Snapshot As Obje";
 //BA.debugLineNum = 55;BA.debugLine="End Sub";
return "";
}
public static String  _service_create() throws Exception{
 //BA.debugLineNum = 22;BA.debugLine="Sub Service_Create";
 //BA.debugLineNum = 24;BA.debugLine="End Sub";
return "";
}
public static String  _service_destroy() throws Exception{
 //BA.debugLineNum = 32;BA.debugLine="Sub Service_Destroy";
 //BA.debugLineNum = 34;BA.debugLine="End Sub";
return "";
}
public static String  _service_start(anywheresoftware.b4a.objects.IntentWrapper _startingintent) throws Exception{
 //BA.debugLineNum = 26;BA.debugLine="Sub Service_Start (StartingIntent As Intent)";
 //BA.debugLineNum = 27;BA.debugLine="Service.StopAutomaticForeground 'Call this when t";
mostCurrent._service.StopAutomaticForeground();
 //BA.debugLineNum = 30;BA.debugLine="End Sub";
return "";
}
}
