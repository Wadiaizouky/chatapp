package b4a.example.fbrtdb;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.objects.ServiceHelper;
import anywheresoftware.b4a.debug.*;

public class messageservice extends  android.app.Service{
	public static class messageservice_BR extends android.content.BroadcastReceiver {

		@Override
		public void onReceive(android.content.Context context, android.content.Intent intent) {
            BA.LogInfo("** Receiver (messageservice) OnReceive **");
			android.content.Intent in = new android.content.Intent(context, messageservice.class);
			if (intent != null)
				in.putExtra("b4a_internal_intent", intent);
            ServiceHelper.StarterHelper.startServiceFromReceiver (context, in, false, BA.class);
		}

	}
    static messageservice mostCurrent;
	public static BA processBA;
    private ServiceHelper _service;
    public static Class<?> getObject() {
		return messageservice.class;
	}
	@Override
	public void onCreate() {
        super.onCreate();
        mostCurrent = this;
        if (processBA == null) {
		    processBA = new BA(this, null, null, "b4a.example.fbrtdb", "b4a.example.fbrtdb.messageservice");
            if (BA.isShellModeRuntimeCheck(processBA)) {
                processBA.raiseEvent2(null, true, "SHELL", false);
		    }
            try {
                Class.forName(BA.applicationContext.getPackageName() + ".main").getMethod("initializeProcessGlobals").invoke(null, null);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            processBA.loadHtSubs(this.getClass());
            ServiceHelper.init();
        }
        _service = new ServiceHelper(this);
        processBA.service = this;
        
        if (BA.isShellModeRuntimeCheck(processBA)) {
			processBA.raiseEvent2(null, true, "CREATE", true, "b4a.example.fbrtdb.messageservice", processBA, _service, anywheresoftware.b4a.keywords.Common.Density);
		}
        if (!false && ServiceHelper.StarterHelper.startFromServiceCreate(processBA, false) == false) {
				
		}
		else {
            processBA.setActivityPaused(false);
            BA.LogInfo("*** Service (messageservice) Create ***");
            processBA.raiseEvent(null, "service_create");
        }
        processBA.runHook("oncreate", this, null);
        if (false) {
			ServiceHelper.StarterHelper.runWaitForLayouts();
		}
    }
		@Override
	public void onStart(android.content.Intent intent, int startId) {
		onStartCommand(intent, 0, 0);
    }
    @Override
    public int onStartCommand(final android.content.Intent intent, int flags, int startId) {
    	if (ServiceHelper.StarterHelper.onStartCommand(processBA, new Runnable() {
            public void run() {
                handleStart(intent);
            }}))
			;
		else {
			ServiceHelper.StarterHelper.addWaitForLayout (new Runnable() {
				public void run() {
                    processBA.setActivityPaused(false);
                    BA.LogInfo("** Service (messageservice) Create **");
                    processBA.raiseEvent(null, "service_create");
					handleStart(intent);
                    ServiceHelper.StarterHelper.removeWaitForLayout();
				}
			});
		}
        processBA.runHook("onstartcommand", this, new Object[] {intent, flags, startId});
		return android.app.Service.START_NOT_STICKY;
    }
    public void onTaskRemoved(android.content.Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        if (false)
            processBA.raiseEvent(null, "service_taskremoved");
            
    }
    private void handleStart(android.content.Intent intent) {
    	BA.LogInfo("** Service (messageservice) Start **");
    	java.lang.reflect.Method startEvent = processBA.htSubs.get("service_start");
    	if (startEvent != null) {
    		if (startEvent.getParameterTypes().length > 0) {
    			anywheresoftware.b4a.objects.IntentWrapper iw = ServiceHelper.StarterHelper.handleStartIntent(intent, _service, processBA);
    			processBA.raiseEvent(null, "service_start", iw);
    		}
    		else {
    			processBA.raiseEvent(null, "service_start");
    		}
    	}
    }
	
	@Override
	public void onDestroy() {
        super.onDestroy();
        if (false) {
            BA.LogInfo("** Service (messageservice) Destroy (ignored)**");
        }
        else {
            BA.LogInfo("** Service (messageservice) Destroy **");
		    processBA.raiseEvent(null, "service_destroy");
            processBA.service = null;
		    mostCurrent = null;
		    processBA.setActivityPaused(true);
            processBA.runHook("ondestroy", this, null);
        }
	}

@Override
	public android.os.IBinder onBind(android.content.Intent intent) {
		return null;
	}public anywheresoftware.b4a.keywords.Common __c = null;
public static de.donmanfred.DatabaseReferenceWrapper _refmessage = null;
public static anywheresoftware.b4a.objects.collections.List _allmessages = null;
public static Object _objest = null;
public static String _sendermember = "";
public static String _receivermember = "";
public static String _lastsender = "";
public static boolean _firsttime = false;
public b4a.example.dateutils _dateutils = null;
public b4a.example.fbrtdb.main _main = null;
public b4a.example.fbrtdb.lobby _lobby = null;
public b4a.example.fbrtdb.romeservice _romeservice = null;
public b4a.example.fbrtdb.useservice _useservice = null;
public b4a.example.fbrtdb.starter _starter = null;
public b4a.example.fbrtdb.httputils2service _httputils2service = null;
public b4a.example.fbrtdb.xuiviewsutils _xuiviewsutils = null;
public b4a.example.fbrtdb.b4xcollections _b4xcollections = null;
public static String  _inti() throws Exception{
 //BA.debugLineNum = 29;BA.debugLine="Public Sub Inti()";
 //BA.debugLineNum = 33;BA.debugLine="refMessage.Initialize(\"RefMessages\",Main.realtime";
_refmessage.Initialize(processBA,"RefMessages",(com.google.firebase.database.DatabaseReference)(mostCurrent._main._realtime /*de.donmanfred.FirebaseDatabaseWrapper*/ .getReferencefromUrl("https://first-project-5317d.firebaseio.com/Rooms/"+BA.ObjectToString(_objest)+"/Messages").getObject()),(Object)("Message"));
 //BA.debugLineNum = 34;BA.debugLine="refMessage.addChildEventListener";
_refmessage.addChildEventListener();
 //BA.debugLineNum = 38;BA.debugLine="End Sub";
return "";
}
public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 6;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 9;BA.debugLine="Public refMessage As DatabaseReference";
_refmessage = new de.donmanfred.DatabaseReferenceWrapper();
 //BA.debugLineNum = 10;BA.debugLine="Public AllMessages As List";
_allmessages = new anywheresoftware.b4a.objects.collections.List();
 //BA.debugLineNum = 12;BA.debugLine="Public Objest As Object";
_objest = new Object();
 //BA.debugLineNum = 14;BA.debugLine="Public SenderMember As String";
_sendermember = "";
 //BA.debugLineNum = 15;BA.debugLine="Public ReceiverMember As String";
_receivermember = "";
 //BA.debugLineNum = 16;BA.debugLine="Public LastSender As String";
_lastsender = "";
 //BA.debugLineNum = 18;BA.debugLine="Public Firsttime As Boolean = True";
_firsttime = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 19;BA.debugLine="End Sub";
return "";
}
public static void  _prossesmessage() throws Exception{
ResumableSub_prossesMessage rsub = new ResumableSub_prossesMessage(null);
rsub.resume(processBA, null);
}
public static class ResumableSub_prossesMessage extends BA.ResumableSub {
public ResumableSub_prossesMessage(b4a.example.fbrtdb.messageservice parent) {
this.parent = parent;
}
b4a.example.fbrtdb.messageservice parent;
int _i = 0;
de.donmanfred.DataSnapshotWrapper _sn = null;
String _ms = "";
int step1;
int limit1;

@Override
public void resume(BA ba, Object[] result) throws Exception{

    while (true) {
        switch (state) {
            case -1:
return;

case 0:
//C
this.state = 1;
 //BA.debugLineNum = 66;BA.debugLine="For i = 0 To AllMessages.Size-1";
if (true) break;

case 1:
//for
this.state = 13;
step1 = 1;
limit1 = (int) (parent._allmessages.getSize()-1);
_i = (int) (0) ;
this.state = 14;
if (true) break;

case 14:
//C
this.state = 13;
if ((step1 > 0 && _i <= limit1) || (step1 < 0 && _i >= limit1)) this.state = 3;
if (true) break;

case 15:
//C
this.state = 14;
_i = ((int)(0 + _i + step1)) ;
if (true) break;

case 3:
//C
this.state = 4;
 //BA.debugLineNum = 67;BA.debugLine="Dim Sn As DataSnapshot";
_sn = new de.donmanfred.DataSnapshotWrapper();
 //BA.debugLineNum = 68;BA.debugLine="Sn.Initialize(AllMessages.Get(i))";
_sn.Initialize(processBA,(com.google.firebase.database.DataSnapshot)(parent._allmessages.Get(_i)));
 //BA.debugLineNum = 70;BA.debugLine="Dim ms As String = Sn.getChild(\"Message\").Value";
_ms = BA.ObjectToString(_sn.getChild("Message").getValue());
 //BA.debugLineNum = 71;BA.debugLine="If(ms.CharAt(0) == \"/\" ) Then";
if (true) break;

case 4:
//if
this.state = 7;
if ((_ms.charAt((int) (0))==BA.ObjectToChar("/"))) { 
this.state = 6;
}if (true) break;

case 6:
//C
this.state = 7;
 //BA.debugLineNum = 72;BA.debugLine="Sleep(4000)";
anywheresoftware.b4a.keywords.Common.Sleep(processBA,this,(int) (4000));
this.state = 16;
return;
case 16:
//C
this.state = 7;
;
 if (true) break;
;
 //BA.debugLineNum = 75;BA.debugLine="If(Sn.getChild(\"Sender\").Value <> UseService.Use";

case 7:
//if
this.state = 12;
if (((_sn.getChild("Sender").getValue()).equals((Object)(parent.mostCurrent._useservice._username /*String*/ )) == false)) { 
this.state = 9;
}else {
this.state = 11;
}if (true) break;

case 9:
//C
this.state = 12;
 //BA.debugLineNum = 76;BA.debugLine="CallSubDelayed3(Lobby,\"ReciveMessageCustoview\",";
anywheresoftware.b4a.keywords.Common.CallSubDelayed3(processBA,(Object)(parent.mostCurrent._lobby.getObject()),"ReciveMessageCustoview",_sn.getChild("Message").getValue(),(Object)(anywheresoftware.b4a.keywords.Common.True));
 if (true) break;

case 11:
//C
this.state = 12;
 //BA.debugLineNum = 78;BA.debugLine="CallSubDelayed3(Lobby,\"SentMessageCustomview\",S";
anywheresoftware.b4a.keywords.Common.CallSubDelayed3(processBA,(Object)(parent.mostCurrent._lobby.getObject()),"SentMessageCustomview",_sn.getChild("Message").getValue(),(Object)(anywheresoftware.b4a.keywords.Common.False));
 if (true) break;

case 12:
//C
this.state = 15;
;
 if (true) break;
if (true) break;

case 13:
//C
this.state = -1;
;
 //BA.debugLineNum = 82;BA.debugLine="AllMessages.Clear";
parent._allmessages.Clear();
 //BA.debugLineNum = 83;BA.debugLine="Firsttime = False";
parent._firsttime = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 84;BA.debugLine="End Sub";
if (true) break;

            }
        }
    }
}
public static String  _refmessages_onchildadded(Object _snapshot,String _child,Object _tag) throws Exception{
de.donmanfred.DataSnapshotWrapper _sn = null;
 //BA.debugLineNum = 40;BA.debugLine="Sub RefMessages_onChildAdded(Snapshot As Object, c";
 //BA.debugLineNum = 41;BA.debugLine="AllMessages.Add(Snapshot)";
_allmessages.Add(_snapshot);
 //BA.debugLineNum = 42;BA.debugLine="Log(\"Message added ..\")";
anywheresoftware.b4a.keywords.Common.LogImpl("06160386","Message added ..",0);
 //BA.debugLineNum = 44;BA.debugLine="If Not(Firsttime) Then";
if (anywheresoftware.b4a.keywords.Common.Not(_firsttime)) { 
 //BA.debugLineNum = 45;BA.debugLine="Dim Sn As DataSnapshot";
_sn = new de.donmanfred.DataSnapshotWrapper();
 //BA.debugLineNum = 46;BA.debugLine="Sn.Initialize(Snapshot)";
_sn.Initialize(processBA,(com.google.firebase.database.DataSnapshot)(_snapshot));
 //BA.debugLineNum = 48;BA.debugLine="LastSender = Sn.getChild(\"Sender\").Value";
_lastsender = BA.ObjectToString(_sn.getChild("Sender").getValue());
 //BA.debugLineNum = 49;BA.debugLine="If(Sn.getChild(\"Sender\").Value <> UseService.User";
if (((_sn.getChild("Sender").getValue()).equals((Object)(mostCurrent._useservice._username /*String*/ )) == false)) { 
 //BA.debugLineNum = 50;BA.debugLine="CallSubDelayed3(Lobby,\"ReciveMessageCustoview\",S";
anywheresoftware.b4a.keywords.Common.CallSubDelayed3(processBA,(Object)(mostCurrent._lobby.getObject()),"ReciveMessageCustoview",_sn.getChild("Message").getValue(),(Object)(anywheresoftware.b4a.keywords.Common.True));
 }else {
 //BA.debugLineNum = 52;BA.debugLine="CallSubDelayed3(Lobby,\"SentMessageCustomview\",Sn";
anywheresoftware.b4a.keywords.Common.CallSubDelayed3(processBA,(Object)(mostCurrent._lobby.getObject()),"SentMessageCustomview",_sn.getChild("Message").getValue(),(Object)(anywheresoftware.b4a.keywords.Common.False));
 };
 };
 //BA.debugLineNum = 55;BA.debugLine="End Sub";
return "";
}
public static String  _refmessages_onchildchanged(Object _snapshot,String _child,Object _tag) throws Exception{
 //BA.debugLineNum = 57;BA.debugLine="Sub RefMessages_onChildChanged(Snapshot As Object,";
 //BA.debugLineNum = 59;BA.debugLine="End Sub";
return "";
}
public static String  _service_create() throws Exception{
 //BA.debugLineNum = 21;BA.debugLine="Sub Service_Create";
 //BA.debugLineNum = 23;BA.debugLine="End Sub";
return "";
}
public static String  _service_destroy() throws Exception{
 //BA.debugLineNum = 61;BA.debugLine="Sub Service_Destroy";
 //BA.debugLineNum = 63;BA.debugLine="End Sub";
return "";
}
public static String  _service_start(anywheresoftware.b4a.objects.IntentWrapper _startingintent) throws Exception{
 //BA.debugLineNum = 25;BA.debugLine="Sub Service_Start (StartingIntent As Intent)";
 //BA.debugLineNum = 26;BA.debugLine="Service.StopAutomaticForeground 'Call this when t";
mostCurrent._service.StopAutomaticForeground();
 //BA.debugLineNum = 27;BA.debugLine="End Sub";
return "";
}
}
