﻿B4A=true
Group=Default Group
ModulesStructureVersion=1
Type=Service
Version=10.2
@EndOfDesignText@
#Region  Service Attributes 
	#StartAtBoot: False
	
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
	
	Public refRoom As DatabaseReference
	Public AllRooms As List
End Sub

Sub Service_Create

End Sub

Sub Service_Start (StartingIntent As Intent)
	Service.StopAutomaticForeground 'Call this when the background task completes (if there is one)
End Sub

Public Sub Inti()
	'Service.StopAutomaticForeground 'Call this when the background task completes (if there is one)
	
	AllRooms.Initialize
	
	refRoom.Initialize("RefUserRoom",Main.realtime.getReferencefromUrl("https://first-project-5317d.firebaseio.com/Rooms"),"Room")
	refRoom.addChildEventListener
	'refRoom.addListenerForSingleValueEvent
	'refRoom.addValueEventListener

End Sub

Sub RefUserRoom_onChildAdded(Snapshot As Object, child As String, tag As Object)
	AllRooms.Add(Snapshot)
	Log("Room added ..")
	
	Dim Sn As DataSnapshot
	Sn.Initialize(Snapshot)
	If(Sn.getChild("Members").getChild("Member1").Value == UseService.UserName) Then
        CallSubDelayed3(Lobby,"AddScrollviewItem3",Sn.getChild("Members").getChild("Member2").Value,Sn.getChild("Members").getChild("Member2").Value)
	Else if((Sn.getChild("Members").getChild("Member2").Value == UseService.UserName)) Then
		CallSubDelayed3(Lobby,"AddScrollviewItem3",Sn.getChild("Members").getChild("Member1").Value,Sn.getChild("Members").getChild("Member1").Value)
    End If
End Sub

Sub RefUserRoom_onChildChanged(Snapshot As Object, child As String, tag As Object)
	Dim Sn,sm As DataSnapshot
	Sn.Initialize(Snapshot)
	Log(Snapshot)
	'Log(Sn.getChild("").Value)
	If(MessageService.refMessage.IsInitialized) And Not(Lobby.ChatState) Then
	For i =0 To AllRooms.Size-1 
	sm.Initialize(AllRooms.Get(i))
	If(sm.Key == Sn.Key) Then	
		
			If(sm.getChild("Members").getChild("Member1").Value == UseService.UserName) Or (sm.getChild("Members").getChild("Member2").Value == UseService.UserName) Then
				'If(Sn.ChildrenCount > 3)  Then
					If(sm.getChild("Members").getChild("Member1").Value == UseService.UserName) Then
						If(MessageService.SenderMember == sm.getChild("Members").getChild("Member2").Value Or MessageService.ReceiverMember == sm.getChild("Members").getChild("Member2").Value) Then
			'CallSubDelayed3(Lobby,"AddScrollviewItem3",Sn.getChild("Members").getChild("Member1").Value,Sn.getChild("Members").getChild("Member1").Value)
						CallSubDelayed2(Lobby,"AddMessageToLable",sm.getChild("Members").getChild("Member2").Value)
						End If
					Else if(sm.getChild("Members").getChild("Member2").Value == UseService.UserName) Then
						If(MessageService.SenderMember == sm.getChild("Members").getChild("Member1").Value Or MessageService.ReceiverMember == sm.getChild("Members").getChild("Member1").Value) Then
						CallSubDelayed2(Lobby,"AddMessageToLable",sm.getChild("Members").getChild("Member1").Value)
						End If
			'CallSubDelayed3(Lobby,"AddScrollviewItem3",Sn.getChild("Members").getChild("Member2").Value,Sn.getChild("Members").getChild("Member2").Value)
		            End If
	End If
	End If
	'End If
	Next
	
	End If

End Sub

Sub Service_Destroy

End Sub
