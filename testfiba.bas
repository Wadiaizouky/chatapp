﻿B4A=true
Group=Default Group
ModulesStructureVersion=1
Type=Activity
Version=7.01
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: False
	#IncludeTitle: True
#End Region

Sub Process_Globals
	Dim strTarget As String = "users/alminick"
	Dim strText1 As String = "08"
End Sub

Sub Globals
	Private EditText1 As EditText
	Private EditText2 As EditText
	Private Label1 As Label
	Private Label2 As Label
	Private Label4 As Label
	Private Label5 As Label
	Private Label6 As Label
	Private Label7 As Label
	Private Label8 As Label
End Sub

Sub Activity_Create(FirstTime As Boolean)
	'Activity.LoadLayout("testfiba")

End Sub

Sub Activity_Resume

	'Label7.Text = Starter.strFirebaseSignedInUser 
	
	'EditText2.Text = strTarget 'Starter.kvs.GetDefault("fbdbPath", strTarget)
	'EditText1.Text = strText1  'Starter.kvs.GetDefault("fbdbValue", strText1)
	'
	'Label2.text = "(ready to send)"
	'Label8.text = "(ready to send)"
	'Label1.text = "(ready to receive)"
	'Label6.text = "(ready to delete)"

	If Starter.strFirebaseProjectId.Contains(" ") Or Starter.strFirebaseProjectId.Contains(" ") Then 
		Msgbox("*** You have to set up your Firebase parameters in the Starter-module", "Not ready for testing!")
		Return
	End If

   	Starter.fbdb.Initialize(Me, $"https://${Starter.strFireBaseProjectId}.firebaseio.com/"$, "FiBaTest_Return")

End Sub

Sub Activity_Pause (UserClosed As Boolean)

End Sub




Sub Button4_Click ' SIGN IN
	Starter.auth.SignInWithGoogle
	
Log("#-Button4_Click")	
	If Starter.auth.CurrentUser.IsInitialized Then 
		Auth_SignedIn(Starter.auth.CurrentUser )
	End If
'	
End Sub

Sub Button2_Click  ' POST
Log("#-Button2_Click")	
	 'Starter.kvs.put("fbdbPath", EditText2.Text)
	 'Starter.kvs.put("fbdbValue", EditText1.Text)

	' --- --- --- --- --- 
	' POST to FiBaDb
	' --- --- --- --- --- 
	Label2.text = "(running...)"

	Dim mapToSend As Map : mapToSend.Initialize
	mapToSend.Put("myvalue1", EditText1.Text)
	
	Starter.fbdb.POST(EditText2.Text, mapToSend, "xyz POST-Test")
	
End Sub

Sub Button5_Click ' PUT 
Log("#-Button5_Click")	
	 'Starter.kvs.put("fbdbPath", EditText2.Text)
	 'Starter.kvs.put("fbdbValue", EditText1.Text)

	' --- --- --- --- --- 
	' PUT to FiBaDb
	' --- --- --- --- --- 
	Label8.text = "(running...)"

	Dim mapToSend As Map : mapToSend.Initialize
	mapToSend.Put("myvalue12", EditText1.Text)
	
	Starter.fbdb.PUT(EditText2.Text, mapToSend, "vvv PUT-Test")
	
End Sub



Sub Button1_Click ' GET 
Log("#-Button1_Click")		
	 'Starter.kvs.put("fbdbPath", EditText2.Text)
	 'Starter.kvs.put("fbdbValue", EditText1.Text)

	' --- --- --- --- --- 
	' Read from FiBaDb
	' --- --- --- --- --- 
	Label1.text = "(running...)"


' -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
' uncomment ONE of the lines starting with "Starter.fbdb.GET..."
	
	' GET standard
Starter.fbdb.GET(EditText2.Text, "zzz GET-Test")
	
	' GET with callback
'Starter.fbdb.GET_cb(EditText2.Text, "zzz GET-Test", "subtotest")
	
	' GET with download
	'Dim strFileDest As String = File.Combine(File.DirRootExternal, "aaa_fibadbtest/" & EditText2.Text & ".json" )
	'Starter.fbdb.GET_dl(EditText2.Text, "zzz GET-Test", strFileDest )
' -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 


End Sub	
Sub Label1_Click
	Msgbox(Label1.Text, "Content of Label1")
End Sub

Sub subtotest(mapRet As Map) 
Log("#-subtotest, mapRet=" & mapRet)
	For Each n As String In mapRet.Keys
Log("#-  n=" & n & " --> " & mapRet.GetDefault(n, ""))			
	Next
	
End Sub



 
Sub Button3_Click ' DELETE
Log("#-Button3_Click")		
	 'Starter.kvs.put("fbdbPath", EditText2.Text)
	 'Starter.kvs.put("fbdbValue", EditText1.Text)

	' --- --- --- --- --- 
	' DELETE in FiBaDb
	' --- --- --- --- --- 
	Label6.text = "(running...)"

    If Msgbox2("Be careful! You may loose all your data if the path is not correct" & CRLF & CRLF & "DELETE '" & EditText2.Text & "' and all data below?", "Read me, ... no really, READ THE TEXT", "DELETE", "cancel","", Null) = DialogResponse.POSITIVE Then 
		Starter.fbdb.DELETE(EditText2.Text, "ppp DELETE-Test")
	End If

End Sub	
 

Sub FiBaTest_Return(mapRet As Map)
Log("#-FiBaTest_Return, mapRet=" & mapRet)
	
	Select Case True
		Case mapRet.Get("Job.Tag") = "xyz POST-Test"
			Msgbox(mapRet.Get("Job.info"),"")
			
		Case mapRet.Get("Job.Tag") = "vvv PUT-Test"
			Msgbox(mapRet.Get("Job.info"),"")
			
		Case mapRet.Get("Job.Tag") ="zzz GET-Test"
			Msgbox("(finished)","")
			Msgbox(mapRet.Get("Job.info"),"")
			
			' If a callback was defined for GET
			' --> https://firebase.google.com/docs/reference/rest/database/#section-param-callback
			Dim strGS As String = mapRet.GetDefault("Job.Getstring", "")
			If strGS.StartsWith("/**/") Then
				strGS = strGS.Replace("/**/", "").Trim 
				Dim intP As Int = strGS.IndexOf("(") 
				If intP > -1 Then 	
					Dim strSub As String = strGS.SubString2(0, intP)
					Dim strM As String   = strGS.SubString2(intP +1, strGS.Length -2)
					Dim mapDat As Map : mapDat.Initialize
					If strM <> "null" Then 
	    				Dim JSON As JSONParser : JSON.Initialize(strM)
	    				mapDat = JSON.NextObject
					End If
					CallSubDelayed2(Me, strSub, mapDat) 
				End If
			End If
			
		Case mapRet.Get("Job.Tag") = "ppp DELETE-Test"
			Msgbox("DELETE success=" & mapRet.Get("Job.Success"),"")
			
	End Select
End Sub



Sub Auth_SignedIn (User As FirebaseUser)
Log("#-Auth_SignedIn=" & User.DisplayName & ", User.PhotoUrl=" & User.PhotoUrl & ", User.Uid=" & User.Uid)
	Starter.strFirebaseSignedInUser = User.DisplayName
	Starter.strFirebaseSignedInUserEMail = User.Email
	Starter.strFirebaseSignedInUserPictureUrl = User.PhotoUrl
	'
	Starter.auth.GetUserTokenId(Starter.auth.CurrentUser, False )			
	
End Sub

Sub auth_TokenAvailable (User As FirebaseUser, Success As Boolean, TokenId As String)
Log("#-auth_TokenAvailable, Success=" & Success ) '& ", TokenId=" & TokenId)
	If Success Then 
		Starter.strFirebaseSignedInUserToken = TokenId
	Else
		Starter.strFirebaseSignedInUserToken = ""
	End If
End Sub

Sub Auth_SignOut(bolFollowedByNewSignIn As Boolean)
Log("#-Auth_SignOut")	
	Starter.auth.SignOutFromGoogle
	Starter.strFirebaseSignedInUser = ""
	Starter.strFirebaseSignedInUserEMail = ""
	Starter.strFirebaseSignedInUserPictureUrl = ""
	'
	If bolFollowedByNewSignIn Then 
		Starter.auth.SignInWithGoogle
	End If
	'
End Sub	